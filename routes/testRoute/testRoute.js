const express = require('express');
const testingController = require('../../controller/testingController');
const router = express.Router();
var auth = require('../../middleWare/auth');


const validation = require('../../middleWare/validation');


router.post("/addBill", testingController.addBill)
router.get("/getBills", testingController.getBills)
router.post("/getBill", testingController.getBill)
router.post("/payBillFromWallet",testingController.payBillFromWallet);
router.post("/getBillById", testingController.getBillById);
router.post("/updateBill", testingController.updateBill);

module.exports = router