const express = require('express');
const router = express.Router();
const admin = require("./adminRoute/adminRoutes")
const user = require("./userRoute/userRoutes")
const agent = require("./agentRoute/agentRoutes")
const mobileMoney = require("./MobileMoneyRoute/MobileMoneyRoute")
const test = require("./testRoute/testRoute");
const bill = require("./billAccountRoute/billAccountRoute")
const money = require("./moneyRoute/moneyRoutes") 

const staticContent = require("./staticContentRoute/staticContentRoutes")
const chat=require("./chatRoute/chatRoutes")
const merchant = require("./merchantRoutes/merchantRoutes");


router.use("/admin",admin)
router.use("/agent",agent)
router.use("/user",user)
router.use("/static",staticContent)
router.use("/chat",chat)
router.use("/mobileMoney",mobileMoney)
router.use("/merchant", merchant)
router.use("/test", test)
router.use("/bill", bill)
router.use('/money', money)

module.exports = router;    