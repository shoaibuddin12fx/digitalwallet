const express = require('express');
const router = express.Router();
const agentController = require("../../controller/agentController");
const MobileMoneyController = require('../../controller/MobileMoneyController');
var auth = require('../../middleWare/auth');
const validation = require('../../middleWare/validation');

router.post('/authenticateMM', MobileMoneyController.authenticate)
router.post('/pay', MobileMoneyController.payeeInitiate)
router.post('/getbillcompanies', MobileMoneyController.getbillcompanies);
router.post('/getBillAgainstUser', MobileMoneyController.getBillAgainstUser);
router.post('/payUserBill', MobileMoneyController.payUserBill);

module.exports = router