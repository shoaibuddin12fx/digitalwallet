const express = require('express');
const router = express.Router();
const agentController = require("../../controller/agentController");
const merchantController = require('../../controller/merchantController');
var auth = require('../../middleWare/auth');
const validation = require('../../middleWare/validation');

router.post('/addMerchant', merchantController.addMerchant)
router.post('/getMerchants', merchantController.getMerchants)
router.post('/getMerchant', merchantController.getMerchant)
router.post('/updateMerchant', merchantController.updateMerchant)

module.exports = router