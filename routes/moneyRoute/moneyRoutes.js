const express = require('express');
//const money=require("../../model/moneyModel")
const router = express.Router();
const moneyController=require("../../controller/moneyController")
var auth = require('../../middleWare/auth');


const {
   
  } = require('../../middleWare/validation');
  
 router.post("/setMoney",auth.verifyToken,moneyController.setMoney)
 router.post("/addBankCard", moneyController.addBankCard)
 router.post("/getBankCards", moneyController.getBankCards)
 router.post("/rechargeFromBankCard", moneyController.rechargeFromBankCard);
module.exports=router