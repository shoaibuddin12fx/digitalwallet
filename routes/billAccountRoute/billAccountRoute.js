const express = require('express');
const router = express.Router();
const billController = require("../../controller/billController")
var auth = require('../../middleWare/auth');
const validation = require('../../middleWare/validation');

router.post('/addBillAccount', billController.addBillAccount)
router.post('/updateBillAccount', billController.updateBillAccount)
router.get('/getBillAccounts', billController.getBillAccounts)
router.post('/getBillAccount', billController.getBillAccount)
router.post('/rechargeElectricityFromWallet', billController.rechargeElectricityFromWallet)
router.post('/topupMobileFromWallet', billController.topupMobileFromWallet)
router.post('/addEducationPayment', billController.addEducationPayment);

module.exports = router