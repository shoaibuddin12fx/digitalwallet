const chatSchema = require('../model/chatingModel');
const user = require('../model/userModel');
const commonFunction = require('../helper/commonFunction')


module.exports = {

     chattingAPI: (req) => {

          var query = { clearStatus: "false" }, response;
          if (req.senderId && req.receiverId) {
               query.$and = [{ $or: [{ senderId: req.senderId }, { senderId: req.receiverId }] }, { $or: [{ receiverId: req.receiverId }, { receiverId: req.senderId }] }]
          }

          return new Promise((resolve, reject) => {
               chatSchema.findOne(query).exec((err, result) => {
                    if (err) {
                         response = ({ responseCode: 500, responseMessage: 'Internal server error', err })
                         resolve(response)
                    }
                    else if (!result) {
                         new chatSchema(req).save((err1, succ) => {
                              if (err1) {
                                   response = ({ responseCode: 500, responseMessage: "Internal server error", err1 })
                                   resolve(response)
                              }
                              else {
                                   response = ({ responseCode: 200, responseMessage: 'Message send successfully', result: succ })
                                   resolve(response)
                              }
                         })
                    }
                    else {
                         if (result.status == "ACTIVE") {
                              chatSchema.findByIdAndUpdate({ "_id": result._id }, { $push: { messages: req.messages[0] } }, { new: true }, (err2, succ1) => {
                                   if (err2) {
                                        response = ({
                                             responseCode: 500,
                                             responseMessage: "Internal server error", err2
                                        })
                                        resolve(response)

                                   }
                                   else if (!succ1) {
                                        response = ({
                                             responseCode: 404,
                                             responceMessage: "Data not found"
                                        })
                                        resolve(response)

                                   }
                                   else {
                                        response = ({ responseCode: 200, responseMessage: 'Message send successfully', result: succ1 })
                                        resolve(response)
                                   }
                              })
                         }
                         else {
                              response = ({ responseCode: 404, responseMessage: 'You cant chat', result: result })
                              resolve(response)
                         }
                    }
               })
          })
     },

     chattingHistory: (req) => {
          let query = {};
          let response = {}

          return new Promise((resolve, reject) => {
               if (req.senderId && !req.receiverId) {
                    query.senderId = req.senderId
               }
               if (req.receiverId && !req.senderId) {
                    query.receiverId = req.receiverId
               }
               if (req.receiverId && req.senderId) {
                    query.$and = [{ $or: [{ receiverId: req.receiverId }, { receiverId: req.senderId }] }, { $or: [{ senderId: req.senderId }, { senderId: req.receiverId }] }]
               }
               if (req.chatId) {
                    query._id = req.chatId
               }
               console.log("query hit>>>>>>>>75", JSON.stringify(query))

               chatSchema.find(query).sort({ "messages.createdAt": -1 }).populate("senderId receiverId", "firstName profilePic").exec((err, result) => {
                    if (err) {
                         response = { responseCode: 500, responseMessage: "Internal server error", err }
                         resolve(response)
                    }
                    else if (result.length == false) {
                         response = { response_code: 200, response_message: "Data found successfully", result: [] }
                         resolve(response)
                    }
                    else {
                         response = { responseCode: 200, responseMessage: "Chatting history", result: result }
                         resolve(response)
                    }
               })
          })
     },

     chatList: (req) => {
          let query = {};
          let response = {}

          return new Promise((resolve, reject) => {
               query = { $or: [{ senderId: req.senderId }, { receiverId: req.senderId }] }
               console.log("query hit>>>>>>>>75", JSON.stringify(query))

               chatSchema.find(query).sort({ "messages.createdAt": -1 }).populate("senderId receiverId", "firstName profilePic").exec((err, result) => {
                    if (err) {
                         response = { responseCode: 500, responseMessage: "Internal server error", err }
                         resolve(response)
                    }
                    else if (result.length == false) {
                         response = { response_code: 200, response_message: "Data found successfully", result: [] }
                         resolve(response)
                    }
                    else {
                         response = { responseCode: 200, responseMessage: "Chat list found successfully", result: result }
                         resolve(response)
                    }
               })
          })
     },

     upoloadAttachment: (req) => {
          return new Promise( (resolve, reject) => {
               if(req.data === null || req.data === undefined)
               resolve({ error: "Data is missing", url : null});
               else {
                    console.log("Uploading Image");
                    if(req.type === 'video')
                    {
                         commonFunction.uploadVideo(req.data, (error, result) => {
                              if (error) {
                                   console.log("Error while Uploading video", error);
                                   resolve({ error: error, url: null });
                              } else {
                                   console.log("Video uploaded :)");
                                   resolve({error: null, url : result});
                              }
                         });
                    } else {
                         commonFunction.uploadImage(req.data, (error, result) => {
                              if (error) {
                                   console.log("Error while Uploading Image", error);
                                   resolve({ error: error, url: null });
                              } else {
                                   console.log("Image uploaded :)");
                                   resolve({error: null, url : result});
                              }
                         });
                    }
                    
               }
          });
     },

     uploadDocument: (req, res) => {
          try {
               let img = req.image;
               if(img == null || img === undefined)
               img = req.body.image;
               if (!img) {
                    console.log("Image param is missing");
                    res.send({ responseCode: 401, responseMessege: "Parameter missing" })

               } else {

                    return new Promise((resolve, reject) => {
                    console.log("Uploading Image");
                    commonFunction.uploadImage(img, (error, result) => {
                         if (error) {
                              console.log("Error while Uploading Image", error);
                              resolve(error);
                             // return res.send({ responseCode: 500, responseMessage: "Internal server error" })
                         } else {
                              console.log("Image uploaded");
                              resolve(result);
                           //   return res.send({ responseCode: 200, responseMessage: "Image upload successfully", result })
                         }
                    }
                    );
               });        
          }
          } catch (error) {
               res.send({ responseCode: 500, responseMessege: "Something went wrong" })
          }
     },
}