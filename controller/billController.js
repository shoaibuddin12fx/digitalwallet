const userModel = require('../model/userModel')
const qrCodeModel = require('../model/qrCodeModel')
const staticPage = require("../model/staticContentModel");
const notificationModel = require("../model/notificationModel")
const transactionModel = require("../model/transactionModel")
const messageModel = require("../model/messageModel")
const commissionModel = require("../model/commissionModel")
const moneyModel = require("../model/moneyModel")
const newcommissionModel = require("../model/newCommissionModel")
const commonFunction = require('../helper/commonFunction')
const stripe = require('stripe')('sk_test_L8oA9O5IOgtmflzWMndmmEhR');
const { commonResponse: response } = require('../helper/responseHandler')
const mongoose = require('mongoose')
const billAccountModel = require('../model/userBillModel');
const merchantModel = require("../model/merchantModel");
const educationPaymentModel = require('../model/educationPaymentModel');

const jwt = require('jsonwebtoken');
//  const { ErrorMessage } = require('../helper/responseMessege')
//  const { SuccessMessage } = require('../helper/responseMessege')
const { SuccessCode } = require('../helper/responseCode')
const { ErrorCode } = require('../helper/responseCode')
const bcrypt = require("bcrypt-nodejs");
const { ErrorMessage, SuccessMessage } = require('../helper/responseMessege');

module.exports = {
    addBillAccount: (req, res) => {
        billAccountModel.findOne({accountNumber: req.body.accountNumber}, (error,data) => {
            if(error)
                response(res, ErrorCode.INTERNAL_ERROR, '', ErrorMessage.INTERNAL_ERROR);
                else if(data)
                response(res, ErrorCode.INTERNAL_ERROR, '', "Account number already exist!");
                else {
                    new billAccountModel(req.body).save((error,data) => {
                        if(error)
                        response(res, ErrorCode.INTERNAL_ERROR, '', ErrorMessage.INTERNAL_ERROR);
                        else response(res, SuccessCode.SUCCESS, '','Bill account added successfully!');
            
                    });
                }
        });
    },
    updateBillAccount: (req,res) => {
        let body = req.body;
        billAccountModel.findOne({accountNumber: req.body.accountNumber}, (error, data) => {
            
            if(error)
                response(res, ErrorCode.INTERNAL_ERROR, '', ErrorMessage.INTERNAL_ERROR);
            
            else if(!data)
                response(res, ErrorCode.NOT_FOUND, '', ErrorMessage.NOT_FOUND);

                else {
                    let billAccount = {};

                    if(body.fullName)
                        billAccount.fullName = body.fullName

                    if(body.address)
                        billAccount.address = body.address

                    if(body.gender)
                        billAccount.gender = body.gender

                    if(body.status)
                        billAccount.status = body.status

                    billAccountModel.findOneAndUpdate({accountNumber: req.body.accountNumber}, {$set:billAccount }, (error, success) => {
                       
                    if(error)
                        response(res, ErrorCode.INTERNAL_ERROR, '', ErrorMessage.INTERNAL_ERROR);
            
                    else if(!success)
                        response(res, ErrorCode.INTERNAL_ERROR, '', ErrorMessage.INTERNAL_ERROR);

                    else response(res, SuccessCode.SUCCESS, '','Bill account updated successfully!');

                    });

                }
        }); 
    },

    getBillAccounts: (req,res) => {
        billAccountModel.find({}, (error, data) => {
            if(error)
                response(res, ErrorCode.INTERNAL_ERROR, '', ErrorMessage.INTERNAL_ERROR);

            else response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);

        });
    },

    getBillAccount: (req,res) => {
        billAccountModel.findOne({_id: req.body.id}, (error, data) => {
            if(error)
                response(res, ErrorCode.INTERNAL_ERROR, '', ErrorMessage.INTERNAL_ERROR);

            else if(!data)
                response(res, ErrorCode.NOT_FOUND, {}, ErrorMessage.NOT_FOUND);

            else response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);

        });
    },

    rechargeElectricityFromWallet: (req,res) => {
        let data = req.body;
        let tag  = "[rechargeElectricityFromWallet]: ";
        
        if(!data.meterNumber)
            response(res, ErrorCode.SOMETHING_WRONG, [], "Meter number is required (meterNumber)");
        
            else {
                    userModel.findOne({ _id: data.userId, status: "ACTIVE"}, (error, userDetail) => {
                        console.log(tag + "getting user", error, userDetail)
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                        }

                        else if(!userDetail)
                            response(res, ErrorCode.NOT_FOUND, [error], ErrorMessage.USER_NOT_FOUND);

                        else if (userDetail.kycStatus == "unverified") {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.PLEASE_VERIFY_KYC_DETAILS);
                        }
                        else {
                            commissionModel.findOne({ status: "ACTIVE" }, async (errComm, commissionDetails) => {
                                if (errComm) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                  //  var commission = parseFloat(data.amount) * parseFloat(commissionDetails.billPayment_commision / 100)// 10
                                   // let totalAmount = parseFloat(data.amount) + commission;
                                   let commission = 0;
                                   let totalAmount = parseFloat(data.amount);
                                        if(userDetail.amountCDF < totalAmount)
                                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                        else {
                                            let passwordCheck = bcrypt.compareSync(data.password, userDetail.password);
                                            if(passwordCheck) {
                                                let newUserBalance = parseFloat(userDetail.amountCDF) - parseFloat(totalAmount);
                                               let userBalanceUpdate = await userModel.findOneAndUpdate({ _id: data.userId },
                                                { $set: { amountCDF: newUserBalance } }, {new : true})
                                                
                                                if(userBalanceUpdate) {
                                                   merchantModel.findOne({ _id: data.merchantId, status: "ACTIVE"}, async (error, merchantDetail) => {
                                                       if(merchantDetail) {
                                                        var merchantBalanceUpdate = await merchantModel.findOneAndUpdate({ _id: data.merchantId },
                                                            { $set: { balanceSDG: parseFloat(merchantDetail.balanceSDG) + parseFloat(data.amount) } }, { new: true })
                                                           // End of find marchant

                                                           if(merchantBalanceUpdate) {
                                                               let obj_details = {
                                                                "merchantId": merchantDetail._id,
                                                                "merchant_id": merchantDetail._id,
                                                                "send_amount": totalAmount,
                                                                "receive_amount": totalAmount,
                                                                "commission": commission,
                                                                "commission_amount": commission,
                                                                "amountType": data.amountType,
                                                                "sendMoneyBy": userDetail.firstName + " " + userDetail.lastName,
                                                                "receiveMoneyBy": merchantDetail.companyName,
                                                                "sender_id": userDetail._id,
                                                                "sender_mobileNumber": userDetail.mobileNumber,
                                                                "receiver_mobileNumber": merchantDetail.contactNumber,
                                                                "sender_UserType": userDetail.userType,
                                                                "receiver_UserType": "MERCHANT",
                                                                "type_transaction": "BILL_PAY",
                                                                "transactionStatus": "Debited",
                                                                "transectionType": "paid"
                                                            }
                                                            new transactionModel(obj_details).save((errSbmit, transDetails) => {
                                                                if (errSbmit) {
                                                                    response(res, ErrorCode.SOMETHING_WRONG, [errSbmit], ErrorMessage.INTERNAL_ERROR);
                                                                } else {
                                                                    response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                }
                                                                
                                                            });

                                                           } else response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);

                                                       } else response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                   });
                                                } else response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                            }  
                                            else {
                                                response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                            }
                                        }
                                }
                            });
                            
                        }
                   })                  
        }
    },


    topupMobileFromWallet: (req,res) => {
        let data = req.body;
        let tag  = "[topupMobileFromWallet]: ";
        
        if(!data.mobileNumber)
            response(res, ErrorCode.SOMETHING_WRONG, [], "Mobile number is required (mobileNumber)");
        
            else {
                    userModel.findOne({ _id: data.userId, status: "ACTIVE"}, (error, userDetail) => {
                        console.log(tag + "getting user", error, userDetail)
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                        }

                        else if(!userDetail)
                            response(res, ErrorCode.NOT_FOUND, [error], ErrorMessage.USER_NOT_FOUND);

                        else if (userDetail.kycStatus == "unverified") {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.PLEASE_VERIFY_KYC_DETAILS);
                        }
                        else {
                            commissionModel.findOne({ status: "ACTIVE" }, async (errComm, commissionDetails) => {
                                if (errComm) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                  //  var commission = parseFloat(data.amount) * parseFloat(commissionDetails.billPayment_commision / 100)// 10
                                   // let totalAmount = parseFloat(data.amount) + commission;
                                   let commission = 0;
                                   let totalAmount = parseFloat(data.amount);
                                        if(userDetail.amountCDF < totalAmount)
                                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                        else {
                                            let passwordCheck = bcrypt.compareSync(data.password, userDetail.password);
                                            if(passwordCheck) {
                                                let newUserBalance = parseFloat(userDetail.amountCDF) - parseFloat(totalAmount);
                                               let userBalanceUpdate = await userModel.findOneAndUpdate({ _id: data.userId },
                                                { $set: { amountCDF: newUserBalance } }, {new : true})
                                                
                                                if(userBalanceUpdate) {
                                                   merchantModel.findOne({ _id: data.merchantId, status: "ACTIVE"}, async (error, merchantDetail) => {
                                                       if(merchantDetail) {
                                                        var merchantBalanceUpdate = await merchantModel.findOneAndUpdate({ _id: data.merchantId },
                                                            { $set: { balanceSDG: parseFloat(merchantDetail.balanceSDG) + parseFloat(data.amount) } }, { new: true })
                                                           // End of find marchant

                                                           if(merchantBalanceUpdate) {
                                                               let obj_details = {
                                                                "merchantId": merchantDetail._id,
                                                                "merchant_id": merchantDetail._id,
                                                                "send_amount": totalAmount,
                                                                "receive_amount": totalAmount,
                                                                "commission": commission,
                                                                "commission_amount": commission,
                                                                "amountType": data.amountType,
                                                                "sendMoneyBy": userDetail.firstName + " " + userDetail.lastName,
                                                                "receiveMoneyBy": merchantDetail.companyName,
                                                                "sender_id": userDetail._id,
                                                                "sender_mobileNumber": userDetail.mobileNumber,
                                                                "receiver_mobileNumber": merchantDetail.contactNumber,
                                                                "sender_UserType": userDetail.userType,
                                                                "receiver_UserType": "MERCHANT",
                                                                "type_transaction": "MOBILE_TOPUP",
                                                                "transactionStatus": "Debited",
                                                                "transectionType": "paid"
                                                            }
                                                            new transactionModel(obj_details).save((errSbmit, transDetails) => {
                                                                if (errSbmit) {
                                                                    response(res, ErrorCode.SOMETHING_WRONG, [errSbmit], ErrorMessage.INTERNAL_ERROR);
                                                                } else 
                                                                    response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                            });

                                                           } else response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);

                                                       } else response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                   });
                                                } else response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                            }  
                                            else {
                                                response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                            }
                                        }
                                }
                            });
                            
                        }
                   })                  
        }
    },

    addEducationPayment: (req, res) => {
        let tag  = "[rechargeElectricityFromWallet]: ";
        let body = req.body;
                    userModel.findOne({ _id: body.userId, status: "ACTIVE"}, (error, userDetail) => {
                        console.log(tag + "getting user", error, userDetail)
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                        }

                        else if(!userDetail)
                            response(res, ErrorCode.NOT_FOUND, [error], ErrorMessage.USER_NOT_FOUND);

                        else if (userDetail.kycStatus == "unverified") {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.PLEASE_VERIFY_KYC_DETAILS);
                        }
                        else {
                            commissionModel.findOne({ status: "ACTIVE" }, async (errComm, commissionDetails) => {
                                if (errComm) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                  //  var commission = parseFloat(data.amount) * parseFloat(commissionDetails.billPayment_commision / 100)// 10
                                   // let totalAmount = parseFloat(data.amount) + commission;
                                   let commission = 0;
                                   let totalAmount = parseFloat(body.amount);
                                        if(userDetail.amountCDF < totalAmount)
                                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                        else {
                                            let passwordCheck = bcrypt.compareSync(body.password, userDetail.password);
                                            if(passwordCheck) {
                                                let newUserBalance = parseFloat(userDetail.amountCDF) - parseFloat(totalAmount);
                                               let userBalanceUpdate = await userModel.findOneAndUpdate({ _id: body.userId },
                                                { $set: { amountCDF: newUserBalance } }, {new : true})
                                                
                                                if(userBalanceUpdate) {
                                                   merchantModel.findOne({ _id: body.merchantId, status: "ACTIVE"}, async (error, merchantDetail) => {
                                                       if(merchantDetail) {
                                                        var merchantBalanceUpdate = await merchantModel.findOneAndUpdate({ _id: body.merchantId },
                                                            { $set: { balanceSDG: parseFloat(merchantDetail.balanceSDG) + parseFloat(body.amount) } }, { new: true })
                                                           // End of find marchant

                                                           if(merchantBalanceUpdate) {
                                                               let obj_details = {
                                                                "merchantId": merchantDetail._id,
                                                                "merchant_id": merchantDetail._id,
                                                                "send_amount": totalAmount,
                                                                "receive_amount": totalAmount,
                                                                "commission": commission,
                                                                "commission_amount": commission,
                                                                "amountType": "SDG",
                                                                "sendMoneyBy": userDetail.firstName + " " + userDetail.lastName,
                                                                "receiveMoneyBy": merchantDetail.companyName,
                                                                "sender_id": userDetail._id,
                                                                "sender_mobileNumber": userDetail.mobileNumber,
                                                                "receiver_mobileNumber": merchantDetail.contactNumber,
                                                                "sender_UserType": userDetail.userType,
                                                                "receiver_UserType": "MERCHANT",
                                                                "type_transaction": "EDUCATION",
                                                                "transactionStatus": "Debited",
                                                                "transectionType": "paid"
                                                            }
                                                            new transactionModel(obj_details).save((errSbmit, transDetails) => {
                                                                if (errSbmit) {
                                                                    response(res, ErrorCode.SOMETHING_WRONG, [errSbmit], ErrorMessage.INTERNAL_ERROR);
                                                                } else {
                                                                    body.transactionRefId = transDetails._id;
                                                                    new educationPaymentModel(body).save((educationPaymenterror, data)=> {
                                                                        if (educationPaymenterror)
                                                                            response(res, ErrorCode.SOMETHING_WRONG, [errSbmit], ErrorMessage.INTERNAL_ERROR);
                                                                        else  response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                    });
                                                                }
                                                                
                                                            });

                                                           } else response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);

                                                       } else response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                   });
                                                } else response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                            }  
                                            else {
                                                response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                            }
                                        }
                                }
                            });
                            
                        }
                   })                  
        }
    }