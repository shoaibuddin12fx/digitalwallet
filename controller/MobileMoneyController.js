const userModel = require('../model/userModel')
const qrCodeModel = require('../model/qrCodeModel')
const staticPage = require("../model/staticContentModel");
const notificationModel = require("../model/notificationModel")
const transactionModel = require("../model/transactionModel")
const messageModel = require("../model/messageModel")
const commissionModel = require("../model/commissionModel")
const moneyModel = require("../model/moneyModel")
const newcommissionModel = require("../model/newCommissionModel")
const commonFunction = require('../helper/commonFunction')
const stripe = require('stripe')('sk_test_L8oA9O5IOgtmflzWMndmmEhR');
const { commonResponse: response } = require('../helper/responseHandler')
const mongoose = require('mongoose')
var https = require('follow-redirects').https;
var fs = require('fs');
const { v4: uuidv4 } = require('uuid');
const { SuccessMessage } = require('../helper/responseMessege')
var axios = require('axios');
var qs = require('qs');

let BASE_URL = "https://sandbox.mobilemoneyapi.io/simulator/v1.1/";
var data = qs.stringify({
  'grant_type': 'client_credentials',
  '': '' 
});

var paymentData = {
    "amount": "200.00",
    "debitParty": [
      {
        "key": "accountid",
        "value": "2999"
      }
    ],
    "creditParty": [
      {
        "key": "accountid",
        "value": "2999"
      }
    ],
    "currency": "RWF"
  };
  
  var paymentconfig = {
    method: 'post',
    url: 'https://sandbox.mobilemoneyapi.io/simulator/v1.2/passthrough/mm/transactions/type/merchantpay',
    headers: { 
      'X-CorrelationID': uuidv4(), 
     // 'X-Callback-URL': 'Please enter your callback URL here', 
      'Content-Type': 'application/json'
    },
    data : JSON.stringify(paymentData)
  };


//  const { ErrorMessage } = require('../helper/responseMessege')
//  const { SuccessMessage } = require('../helper/responseMessege')
const { SuccessCode } = require('../helper/responseCode')
const { ErrorCode } = require('../helper/responseCode')
const bcrypt = require("bcrypt-nodejs");


var config = {
    method: 'post',
    url: 'https://sandbox.mobilemoneyapi.io/v1/oauth/accesstoken',
    headers: { 
      'Content-Type': 'application/x-www-form-urlencoded', 
      'Authorization': 'Basic ZDN2N2RmNHFoYTY3bmJjbWYxcDZwZTg3ZTpidXZuYTRvaDR1YWdoMjQ2OXNhcjVudDM0Mms5bzl1OTlkaDloNWF2aXFidDdkaGI2ZjA='
    },
    data : data
  };
  
  

module.exports = {
    /**
      * Function Name : authenticate
      * Description   : authorization for Mobile Money
      *
      * @return response  
      */
    authenticate: (req, res) => {
        axios(config)
        .then(function (_response) {
            console.log(JSON.stringify(response.data));
            response(res, SuccessCode.SUCCESS, _response.data , "Success");
        })
        .catch(function (error) {
            console.log(JSON.stringify(error.response.data));
            response(res, SuccessCode.Error,  error.response.data, error.message);
        });
    },

    getbillcompanies: (req, res) => {

      return new Promise((resolve, reject) => {
        var config = {
          method: 'get',
          url: BASE_URL +'passthrough/mm/billcompanies',
          headers: { }
        };
        axios(config)
        .then(function (response) {
          console.log(JSON.stringify(response.data));
          resolve(response.data);
        })
        .catch(function (error) {
          console.log(error);
        });
    })
  },

  getBillAgainstUser :(req, res) => {
    if(!req.body || req.body.length == 0)
    response(res, ErrorCode.BAD_REQUEST, [], "params are reauired");
    var data = req.body[0];

    var config = {
      method: 'get',
      url: BASE_URL + 'passthrough/mm/accounts/'+ data.type+'@'+data.id+'/bills',
      headers: { }
    };
    
    axios(config)
    .then(function (resultMM) {
      console.log(JSON.stringify(res.data));
      response(res, SuccessCode.SUCCESS,resultMM.data, SuccessMessage.DETAIL_GET);
    })
    .catch(function (error) {
      console.log(error);
    });
  },

  payUserBill: (req,res) => {

    var data = req.body;
    var config = {
      method: 'post',
      url: BASE_URL + 'passthrough/mm/accounts/'+ data.type+'@'+data.id+'/bills/'+ data.ref +'/payments',
      headers: { 
        'accept': 'application/json', 
        'Content-Type': 'application/json'
      },
      data : data
    };
    
    axios(config)
    .then(function (resultMM) {
      console.log(JSON.stringify(res.data));
      response(res, SuccessCode.SUCCESS,resultMM.data, SuccessMessage.DETAIL_GET);
    })
    .catch(function (error) {
      console.log(error);
    });

  },
     
    payeeInitiate:(req,res) => {
        axios(paymentconfig)
        .then(function (response) {
            console.log(JSON.stringify(response.data));
        })
        .catch(function (error) {
            console.log(error);
        });
    }
};