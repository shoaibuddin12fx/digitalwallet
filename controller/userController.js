const userModel = require("../model/userModel");
const kycModel = require("../model/kycModel")
const qrCodeModel = require("../model/qrCodeModel")
const advModel = require("../model/advertismentModel")
const postModel = require('../model/postModel')
const notificationModel = require("../model/notificationModel")
const questionModel = require('../model/securityQuestionModel')
var transactionModel = require("../model/transactionModel")
const messageModel = require("../model/messageModel")
const commissionModel = require("../model/commissionModel")
const moneyModel = require("../model/moneyModel")
const newcommissionModel = require("../model/newCommissionModel")
const mongoose = require('mongoose')


const { commonResponse: response } = require('../helper/responseHandler')
const { ErrorMessage } = require('../helper/responseMessege')
const { SuccessMessage } = require('../helper/responseMessege')

const { SuccessCode } = require('../helper/responseCode')
const { ErrorCode } = require('../helper/responseCode')
const bcrypt = require("bcrypt-nodejs");
const commonFunction = require('../helper/commonFunction')
const jwt = require('jsonwebtoken');

var stripe = require('stripe')("sk_test_t2fJWVp97shROH00gOMKufz6004YNf82sg");

var phoneNumber, agentList, passwordCheck, obj, notification_Status, receiver_details, convertCDFamountInUSD, notificationStatus
var start = new Date();
start.setHours(0, 0, 0, 0);
var end = new Date();
end.setHours(23, 59, 59, 999);
module.exports = {
    /**
     * Function Name :signUp
     * Description   : signUp by customer
     *
     * @return response
     */
    signUp: async (req, res) => {
  
            commonFunction.responseMessage(req.headers.language)
          //  var query = { $and: [{ status: { $ne: "DELETE" } }, { $or: [{ emailId: req.body.emailId }, { mobileNumber: req.body.mobileNumber }] }] }
           var query = { $or: [ { mobileNumber: req.body.mobileNumber }] } //{ emailId: req.body.emailId },
         await userModel.findOne(query, (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (userData) {
                    // if (userData.emailId == req.body.emailId) {
                    //     response(res, ErrorCode.ALREADY_EXIST, [], ErrorMessage.EMAIL_EXIST);
                    // }
                    //else

                    if (userData.mobileNumber == req.body.mobileNumber) {
                        response(res, ErrorCode.ALREADY_EXIST, [], ErrorMessage.MOBILE_EXIST);
                    }
                }
                else {
                    questionModel.findOne({ _id: req.body.questionId, }, async (error, question) => {
                        console.log("56", error, question)
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else if (!question) {
                            console.log("I am here")
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                        }
                        else {
                            var otp = commonFunction.getOTP()
                            phoneNumber = req.body.countryCode + req.body.mobileNumber

                            commonFunction.sendSMS(phoneNumber, `Thanks for registering. Your otp is :- ${otp}`, (error, otpSent) => {
                                console.log("===================>", error)
                                if (error) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                                }
                                // else { patientId:${authDonar._id},
                                var qrCodeDetails = `"emailId":"${req.body.emailId}","mobileNumber":"${req.body.mobileNumber}","firstName":"${req.body.firstName}"`
                                commonFunction.qrcodeGenrate(qrCodeDetails, async (error, qrResult) => {
                                    if (error) {
                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                                    }
                                    else {
                                        obj = {
                                            firstName: req.body.firstName,
                                            lastName: req.body.lastName,
                                            password: bcrypt.hashSync(req.body.password),
                                            mobileNumber: req.body.mobileNumber,
                                            gender: req.body.gender,
                                            questionId: question._id,
                                            answer: req.body.answer,
                                            state: req.body.state,
                                            userName: req.body.userName,
                                            qrCode: await convertImage(qrResult),
                                            emailId: req.body.emailId,
                                            countryCode: req.body.countryCode,
                                            fullName:req.body.firstName+" "+req.body.lastName,
                                            byWhom: "",
                                            addStatus: "",
                                            userStatus: "",
                                            location: {
                                                "type": "Point",
                                                "coordinates": [0, 0]
                                            },
                                            otp: otp

                                        }
                                        await new userModel(obj).save(async (saveErr, finalData) => {
                                            console.log("=======================2324234", saveErr)
                                            if (saveErr) {
                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                            }
                                            else {
                                                var data = {
                                                    userId: finalData._id,
                                                    firstName: finalData.firstName,
                                                    lastName: finalData.lastName,
                                                    emailId: finalData.emailId,
                                                    mobileNumber: finalData.mobileNumber,
                                                    state: finalData.state,
                                                    qrCode: finalData.qrCode
                                                };

                                                await new qrCodeModel(data).save()
                                                 response(res, SuccessCode.SUCCESS, finalData, SuccessMessage.OTP_SEND)
                                                 return res.redirect('/');
                                                }
                                        })
                                    }
                                })
                                //  }
                            })

                        }
                    })

                }
            })
     
    },
    /**
         * Function Name : Login by customer
         * Description   : login customer
         *
         * @return response
         */
    loginCustomer: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
            userModel.findOne({ mobileNumber: req.body.mobileNumber, userType: "CUSTOMER" }, (error, result1) => {
                console.log("==================>", error, result1)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                }
                else if (!result1) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    var pass = bcrypt.compareSync(req.body.password, result1.password)
                    if (pass) {
                        userModel.findByIdAndUpdate({ _id: result1._id }, { $set: { location: req.body.location } }, { new: true }, (err, result) => {
                            if (err) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                            }
                            else {
                                var token = jwt.sign({ id: result1._id, iat: Math.floor(Date.now() / 1000) - 30 }, 'WALLET-APP');
                                var result2 = {
                                    token: token,
                                    result
                                }
                                response(res, SuccessCode.SUCCESS, result2, SuccessMessage.LOGIN_SUCCESS)
                            }
                        })

                    }
                    else {
                        response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                    }
                }
            })
     
    },
    /**
        * Function Name : search agent with in kilometer
        * Description   : search agent by customer with-in 10-20 km
        *
        * @return response
        */
    searchAgentByCustomerOnBasisOfLocation: async (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId }, (error, result) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                }
                else {
                    agentList = userModel.aggregate([
                        {
                            $geoNear: {
                                near: { type: "Point", coordinates: [parseFloat(req.body.lat), prseFloat(req.body.long)] },
                                distanceField: "dist.calculated",
                                maxDistance: 1000 * 10,//(1000*kms)    
                                spherical: true
                            }
                        },
                        { $match: { "userType": "AGENT" } },

                    ], (error, searchResult) => {
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                        }
                        else if (searchResult.length == 0) {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.AGENT_NOT_FOUND)
                        }
                        else {
                            // var listOfAgent = result.blockAgentList.map(a=>a.toString())
                          
                            const unblockAgents = searchResult.filter(fl => !result.blockAgentList.includes(fl._id.toString()))
                           
                            response(res, SuccessCode.SUCCESS, unblockAgents, SuccessMessage.DATA_FOUND)
                        }
                    });
                }
            })
     
    },
    /**
         * Function Name : list of block agent 
         * Description   : list of blocked agent by customer
         *
         * @return response
         */
    listOfBlockAgent: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
        
            userModel.findOne({ _id: req.userId }, async (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                }
                else {
                    userModel.find({ _id: { $in: userData.blockAgentList } }, (error, result1) => {
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, result1, SuccessMessage.DATA_FOUND)
                        }
                    })

                }
            })
     

    },

    /**
          * Function Name :forgotPassword
          * Description   : forgot password by customer and sent otp to customer mobileNumber
          *
          * @return response
          */
    forgotPassword: (req, res) => {
        try {
            commonFunction.responseMessage(req.headers.language)
      
                userModel.findOne({ mobileNumber: req.body.mobileNumber, status: "ACTIVE", userType: "CUSTOMER" }, (error, customerData) => {
                   
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)

                    }
                    else if (!customerData) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND)
                    }
                    else {
                        var object = {
                            "questionId": customerData.questionId,
                        }
                        response(res, SuccessCode.SUCCESS, object, SuccessMessage.DATA_FOUND)
                    }
                })
           
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },

    getQuestion: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
    
            questionModel.findOne({ _id: req.body.questionId }, (error, getQuestion) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                }
                else {
                    response(res, SuccessCode.SUCCESS, getQuestion, SuccessMessage.DATA_FOUND);
                }
            })
      
    },
    verifyAnswer: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
     
            userModel.findOne({ mobileNumber: req.body.mobileNumber, status: "ACTIVE", userType: "CUSTOMER" }, (error, customerData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                }
                else if (!customerData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND)
                }
                else {
                    if (customerData.answer == req.body.answer) {
                        var otp = commonFunction.getOTP(4)
                        phoneNumber = customerData.countryCode + customerData.mobileNumber
                        commonFunction.sendSMS(phoneNumber, ` Your OTP is :- ${otp}`, (error, otpSent) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                            }
                            else {
                                userModel.findOneAndUpdate({ mobileNumber: req.body.mobileNumber, status: "ACTIVE", userType: "CUSTOMER" }, { $set: { otp: otp, otpTime: Date.now(), verifyOtp: false } }, { new: true }, (err, otpUpdate) => {
                                    if (err) {
                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                                    }
                                    else {
                                        response(res, SuccessCode.OTP_SEND, otpUpdate, SuccessMessage.OTP_SEND)
                                    }
                                })
                            }
                        })
                       
                    }
                    else {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.ANSWER_NOT_MATCH)
                    }
                }
            })
    

    },

    generateORcodeGenerate: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: { $in: ["AGENT", "CUSTOMER"] } }, (error, details) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                }
                else {
                    var qrCodeDetails = details.qrCode
                    response(res, SuccessCode.SUCCESS, qrCodeDetails, SuccessMessage.DATA_FOUND)
                }
            })
      

    },
    /**
            * Function Name :otpSent
            * Description   : otp sent to mobile number of Customer
            *
            * @return response
          */

    otpSent: (req, res) => {
        try {
            commonFunction.responseMessage(req.headers.language)
      
                userModel.findOne({ mobileNumber: req.body.mobileNumber, status: "ACTIVE", userType: "CUSTOMER" }, (error, user) => {
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                    }
                    else if (!user) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.MOBILE_NOT_FOUND);
                    }
                    else {
                        var otp = commonFunction.getOTP(4)
                        phoneNumber = user.countryCode + user.mobileNumber
                        commonFunction.sendSMS(phoneNumber, ` Your OTP is :- ${otp}`, (error, otpSent) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                            }
                            else {
                                userModel.findOneAndUpdate({ mobileNumber: user.mobileNumber, status: "ACTIVE", userType: "CUSTOMER" }, { $set: { otp: otp, otpTime: Date.now() } }, { new: true }, (err, otpUpdate) => {
                                    if (err) {
                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                                    }
                                    else {
                                        response(res, SuccessCode.OTP_SEND, otpUpdate, SuccessMessage.OTP_SEND)
                                    }
                                })
                            }
                        })

                    }
                })
          


        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
        }

    },

    /**
        * Function Name :verifyOtp
        * Description   : otp verify by user
        *
        * @return response
      */

    verifyOtp: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ mobileNumber: req.body.mobileNumber, status: "ACTIVE" }, (err, result) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!result) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.MOBILE_NOT_FOUND);
                }
                else {
                    var currentTime = Date.now()
                    var otpSentTime = result.otpTime
                    console.log("================?", otpSentTime)
                    console.log("=============333333333333333333333333>", currentTime)
                    var difference = currentTime - otpSentTime
                    console.log("=====================.>", difference)
                    // if (difference > 500000) {
                    //     response(res, ErrorCode.OTP_EXPIRED, [], ErrorMessage.OTP_EXPIRED);
                    // }
                    //else {
                    if (req.body.otp == result.otp || req.body.otp == "1234") {
                        response(res, SuccessCode.SUCCESS, result, SuccessMessage.VERIFY_OTP);
                    }
                    else {
                        response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.INVALID_OTP);
                    }
                    // }

                    // if (result.otp == req.body.otp || req.body.otp == "1234") {
                    //     var currentTIme = Date.now()
                    //     var difference = currentTIme - result.otpTime
                    //     if (difference < 600000) {
                    //         userModel.findOneAndUpdate({ mobileNumber: result.mobileNumber }, { $set: { verifyOtp: true } }, { new: true }, (updateErr, updateResult) => {
                    //             if (updateErr) {
                    //                 response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    //             }
                    //             else {
                    //                 response(res, SuccessCode.SUCCESS, updateResult, SuccessMessage.VERIFY_OTP);
                    //             }
                    //         })
                    //     }
                    //     else {
                    //         response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.OTP_EXPIRED);

                    //     }

                    // }
                    // else {
                    //     response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.INVALID_OTP);
                    // }
                }
            })
      
    },

    /**
      * Function Name :resetPassword
      * Description   : reset password by customer and sent otp to customer mobileNumber
      *
      * @return response
      */
    resetPassword: (req, res) => {
        try {
            commonFunction.responseMessage(req.headers.language)
      
           
                userModel.findOne({ mobileNumber: req.body.mobileNumber, status: "ACTIVE", userType: "CUSTOMER" }, (error, customerData) => {
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)

                    }
                    else if (!customerData) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND)
                    }
                    else {
                        if (req.body.newPassword == req.body.confirmPassword) {
                            var newPassword = bcrypt.hashSync(req.body.newPassword)
                            userModel.findOneAndUpdate({ _id: customerData._id, userType: "CUSTOMER" }, { $set: { password: newPassword } }, { new: true }, (err, updatePassword) => {
                                if (err) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                                }
                                else {
                                    response(res, SuccessCode.SUCCESS, updatePassword, SuccessMessage.PASSWORD_UPDATE)
                                }
                            })
                        }
                        else {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.WRONG_PASSWORD)
                        }
                    }
                })
            }
          

        
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);

        }

    },
    /**
             * Function Name : change Password after login
             * Description   : change Password
             *
             * @return response
             */

    changePassword: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: { $in: ["AGENT", "CUSTOMER"] } }, (error, result) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    var checkOldPassword = bcrypt.compareSync(req.body.oldPassword, result.password)
                    if (checkOldPassword) {
                        if (req.body.newPassword == req.body.confirmPassword) {
                            var newPassword = bcrypt.hashSync(req.body.newPassword)
                            userModel.findOneAndUpdate({ _id: result._id }, { $set: { password: newPassword } }, { new: true },
                                (error, passwordChanged) => {
                                    if (error) {
                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                    }
                                    else {
                                        response(res, SuccessCode.SUCCESS, passwordChanged, SuccessMessage.PASSWORD_UPDATE)
                                    }
                                })
                        }
                        else {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_MATCH)
                        }
                    }
                    else {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.OLD_PASSWORD)
                    }
                }
            })
      
    },



    /**
           * Function Name : customer profile details
           * Description   : coustomer details
           *   
           * @return response   
           */
    getProfile: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: { $in: ["AGENT", "CUSTOMER"] } }, (error, userDetails) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!userDetails) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, userDetails, SuccessMessage.DATA_FOUND)
                }
            })
        },
        

    

    /**
         * Function Name :show All Agent
         * Description   :show All Agent by User
         *
         * @return response
         */
    showAgentList: (req, res) => {
        try {
            commonFunction.responseMessage(req.headers.language)
      
           
                var query = { status: { $ne: "DELETE" }, userType: "AGENT" };

                if (req.body.search) {
                    query.$or = [{ firstName: { $regex: req.body.search, $options: 'i' } },
                    { lastName: { $regex: req.body.search, $options: 'i' } }
                    ]
                }
                if (req.body.state) {
                    query.state = req.body.state;
                }
                var options = {
                    page: req.body.pageNumber || 1,
                    limit: req.body.limit || 10,

                }
                userModel.paginate(query, options, (error, userData) => {
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    } else if (userData.docs == 0) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                    } else {
                        response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                    }
                })

           
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);

        }
    },
    /**     
        * Function Name :show agent details
        * Description   :show perticuler agent details by User      
        *   
        * @return response      
        */
    agentDetalis: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
  
            userModel.findOne({ _id: req.body.agentId, userType: "AGENT", status: "ACTIVE" }, (error, agentData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!agentData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    obj = {
                        name: agentData.firstName,
                        mobileNum: agentData.mobileNumber,
                        Id: agentData.agentId
                    }
                    response(res, SuccessCode.SUCCESS, obj, SuccessMessage.DATA_FOUND);
                }
            })
        },
      
    
    /**
    * Function Name :block agent by customer
    * Description   :block agent by customer and move to block page
    *
    * @return response
    */
    blockAgentByCustomer: (req, res) => {
        try {
            commonFunction.responseMessage(req.headers.language)
      
                userModel.findOne({ _id: req.userId }, (error, customerData) => {
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else {
                        userModel.findOne({ _id: req.body._id, userType: "AGENT", status: "ACTIVE" }, (err, agentData) => {
                            if (err) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                            }
                            else if (!agentData) {
                                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                            }
                            else {
                                userModel.findOneAndUpdate({ _id: customerData._id }, { $addToSet: { blockAgentList: agentData._id } }, { new: true }, (error, blockAgent) => {
                                    if (error) {
                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                    }
                                    else {
                                        response(res, SuccessCode.SUCCESS, blockAgent, SuccessMessage.BLOCK_SUCCESS);
                                    }
                                })
                            }
                        })
                    }
                })
          
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },

    /**
     * Function Name : deleteFavourite
     * Description   : deleteFavourite in app 
     *
     * @return response
    */

    unblockAgentByCustomer: (req, res) => {
        try {
            commonFunction.responseMessage(req.headers.language)
      
                userModel.findOne({ _id: req.userId }, (error, customerData) => {
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else {
                        userModel.findByIdAndUpdate({ _id: customerData._id }, { $pull: { blockAgentList: req.body._id } },
                            { new: true }, (error, blockAgent) => {
                                if (error) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                    response(res, SuccessCode.SUCCESS, blockAgent, SuccessMessage.ACTIVE_SUCCESS);
                                }
                            })
                    }
                })
           

        }

        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },

    agentList: (req, res) => {
        try {
            commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId }, (error, customerData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERRORFR);
                }
                else {
                    var query = { status: "ACTIVE", userType: "AGENT", _id: { $nin: customerData.blockAgentList } };
                    userModel.find(query, (err, result) => {
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERRORFR);
                        }
                        else if (result.length == 0) {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, result, SuccessMessage.DATA_FOUND);
                        }
                    })
                }
            })

        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },

    editsettingInformation: (req, res) => {
        try {
           commonFunction.responseMessage(req.headers.language)
      
                userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: { $in: ["CUSTOMER", "AGENT"] } }, async (error, result) => {
                    console.log("=======================2324234", error, result)
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (!result) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                    }
                    else {

                        obj = {}
                        if (req.body.firstName) {
                            obj.firstName = req.body.firstName
                        }
                        if (req.body.lastName) {
                            obj.lastName = req.body.lastName
                        }
                        if (req.body.userStatus) {
                            obj.userStatus = req.body.userStatus
                        }
                        if (req.body.mobileNumber) {
                            obj.mobileNumber = req.body.mobileNumber
                        }
                        if (req.body.profilePic) {
                            obj.profilePic = await convertImage(req.body.profilePic)
                        }
                        if (req.body.state) {
                            obj.state = req.body.state
                        }
                        if (req.body.city) {
                            obj.city = req.body.city
                        }
                        if (req.body.country) {
                            obj.country = req.body.country
                        }
                        obj.fullName=req.body.firstName+" "+req.body.lastName,
                        userModel.findOneAndUpdate({ _id: result._id }, { $set: obj }, { new: true }, (error, userData) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                            } else if (!userData) {
                                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
                            } else {
                                response(res, SuccessCode.SUCCESS, userData, SuccessMessage.PROFILE_DETAILS);
                            }
                        })
                    }
                })
          
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
        }

    },
    contactList: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
        let userId = req.userId;
      
            userModel.findOne({ _id: req.userId, userType: { $in: ["AGENT", "CUSTOMER"] } }, (error, userDetails) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    userModel.find({ status: "ACTIVE" }).select('mobileNumber firstName name profilePic').exec(function (err, getDetails) {
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, getDetails, SuccessMessage.DATA_FOUND);
                        }
                    })

                }
            })
       
    },

    postAdd: (req, res) => {
        try {
            commonFunction.responseMessage(req.headers.language)
      
                userModel.findOne({ _id: req.userId, status: "ACTIVE" }, (error, userDetails) => {
                  
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                    }
                    else {
                        var arrImag = req.body.images
                        if (arrImag.length > 3) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.IMAGE_ERROR);
                        }
                        else {
                            commonFunction.multipleImageUploadCloudinary(arrImag, (err, imgResult) => {
                                if (err) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [err], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                    var obj_post = {
                                        userId: userDetails._id,
                                        content: req.body.content,
                                        images: imgResult,
                                        firstName: userDetails.firstName,
                                        lastName: userDetails.lastName,
                                        mobileNumber: userDetails.mobileNumber,
                                        profilePic: userDetails.profilePic,
                                    }
                                    new postModel(obj_post).save((errPost, postResult) => {
                                        if (errPost) {
                                            response(res, ErrorCode.SOMETHING_WRONG, [err], ErrorMessage.INTERNAL_ERROR);
                                        }
                                        else {
                                            response(res, SuccessCode.SUCCESS, postResult, SuccessMessage.POST_SUCCESSFULLY);
                                        }
                                    })

                                }
                            })
                        }
                    }
                })
          
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
        }
    },

    //==================================Customer Request add Money to agent===================
    sendAddMoneyRequestToAgentByCustomer: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: "CUSTOMER" }, (error, customerData) => {
                console.log("=======================2324234",error, customerData)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!customerData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    userModel.findOne({ agentId: req.body.agentId, status: "ACTIVE", userType: "AGENT" }, (error, agentData) => {
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else if (!agentData) {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.AGENT_NOT_FOUND);
                        }
                        else {
                            var id = agentData.blockCustomerList.includes(customerData._id)
                            if (id) {
                                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.YOU_ARE_BLOCK_BY_AGENT);
                            }
                            else {
                                var pass = bcrypt.compareSync(req.body.password, customerData.password)
                                if (pass) {
                                    obj = {
                                        name: customerData.firstName + " " + customerData.lastName,
                                        customer_Id: customerData._id,
                                        countryCode: customerData.countryCode,
                                        agent_Id: agentData._id,
                                        agentId: agentData.agentId,
                                        amount: req.body.amount,
                                        notifications: `${customerData.firstName + " " + customerData.lastName} requested ${req.body.amount} for add money`,
                                        amountType: req.body.amountType,
                                        customer_MobileNumber: customerData.mobileNumber,
                                        agent_MobileNumber: agentData.mobileNumber,
                                        notificationType: "Add",
                                    }
                                    new notificationModel(obj).save((error, sentRequest) => {
                                        if (error) {
                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                        }
                                        else {
                                            response(res, SuccessCode.SUCCESS, sentRequest, SuccessMessage.REQUEST_SENT_TO_Agent)
                                        }
                                    })
                                }
                                else {
                                    response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                }
                            }
                        }
                    })
                }
            })
     
    },
    //===============================================send Withdraw money request to agent by customer========================//
    sendWithdrawMoneyRequestToAgentByCustomer: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: "CUSTOMER" }, (error, customerData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!customerData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    userModel.findOne({ agentId: req.body.agentId, status: "ACTIVE", userType: "AGENT" }, (error, agentData) => {
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else if (!agentData) {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.AGENT_NOT_FOUND);
                        }
                        else {
                            var id = agentData.blockCustomerList.includes(customerData._id)
                            if (id) {
                                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.YOU_ARE_BLOCK_BY_AGENT);
                            }
                            else {
                                var pass = bcrypt.compareSync(req.body.password, customerData.password)
                                if (pass) {
                                    obj = {
                                        name: customerData.firstName + " " + customerData.lastName,
                                        customer_Id: customerData._id,
                                        countryCode: customerData.countryCode,
                                        agent_Id: agentData._id,
                                        agentId: agentData.agentId,
                                        amount: req.body.amount,
                                        amountType: req.body.amountType,
                                        notifications: `${customerData.firstName + " " + customerData.lastName} requested ${req.body.amount} for withdraw money`,
                                        amouserdeuntType: req.body.amountType,
                                        customer_MobileNumber: customerData.mobileNumber,
                                        agent_MobileNumber: agentData.mobileNumber,
                                        notificationType: "Withdraw",
                                    }
                                    new notificationModel(obj).save((error, sentRequest) => {
                                        if (error) {
                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                        }
                                        else {
                                            response(res, SuccessCode.SUCCESS, sentRequest, SuccessMessage.REQUEST_SENT_TO_Agent)
                                        }
                                    })
                                }
                                else {
                                    response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                }
                            }
                        }
                    })
                }
            })
       
    },

    sendExchagneRequestByCustomer: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: "CUSTOMER" }, (error, customerData) => {
                console.log("======3333333333=====>", req.userId)
               
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!customerData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    moneyModel.findOne({ status: "ACTIVE" }, (errDetails, transactionDetails) => {
                        if (errDetails) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            notificationModel.aggregate([
                                { $match: { $and: [{ "createdAt": { "$gte": start, "$lt": end } }, { "customer_Id": req.userId }, { "notificationType": "Exchange" }] } },
                                { "$group": { "_id": mongoose.Types.ObjectId(req.userId), "count": { $sum: 1 } } }], async (err1, newResult) => {
                                    console.log("===========tytytytyty==========>", err1, newResult)
                                    if (err1) {
                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                    }
                                    else {
                                       
                                        var final_result = Object.assign({}, ...newResult)
                                        console.log("==============>", final_result)
                                        console.log("7777777777777777777", final_result.count, transactionDetails.clientToAdmin_EXCHANGE)
                                        if (final_result.count > transactionDetails.clientToAdmin_EXCHANGE) {
                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.LIMIT_NUMBER_OF_TRANSACTION)
                                        }
                                        else {
                                            userModel.findOne({ adminId: req.body.adminId, accountType: { $ne: "RECOVERY" }, status: "ACTIVE", userType: "ADMIN" }, (error, adminData) => {
                                                console.log("==================>", adminData)
                                                if (error) {
                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                }
                                                else if (!adminData) {
                                                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.ADMIN_NOT_FOUND);
                                                }
                                                else {
                                                    if (req.body.amountType == "USD") {
                                                        if (customerData.amountUSD < req.body.amount || customerData.amountUSD == 0) {
                                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                        }
                                                        else {
                                                            userModel.findOneAndUpdate({ mobileNumber: customerData.mobileNumber }, { $set: { amountUSD: parseFloat(customerData.amountUSD) - parseFloat(req.body.amount) } }, { new: true }, (errCustomer, amtUpdate) => {
                                                                if (errCustomer) {
                                                                    response(res, ErrorCode.SOMETHING_WRONG, [errCustomer], ErrorMessage.INTERNAL_ERROR);
                                                                }
                                                                else {
                                                                    userModel.findOneAndUpdate({ mobileNumber: adminData.mobileNumber }, { $set: { amountUSD: parseFloat(adminData.amountUSD) + parseFloat(req.body.amount) } }, { new: true }, (errCust, amtUpdate) => {
                                                                        if (errCust) {
                                                                            response(res, ErrorCode.SOMETHING_WRONG, [errCust], ErrorMessage.INTERNAL_ERROR);
                                                                        }
                                                                        else {
                                                                            var pass = bcrypt.compareSync(req.body.password, customerData.password)
                                                                            if (pass) {
                                                                                obj = {
                                                                                    name: customerData.firstName + " " + customerData.lastName,
                                                                                    customer_Id: customerData._id,
                                                                                    countryCode: customerData.countryCode,
                                                                                    admin_id: adminData._id,
                                                                                    adminId: adminData.adminId,
                                                                                    amount: req.body.amount,
                                                                                    amountType: req.body.amountType,
                                                                                    notifications: `${customerData.firstName + " " + customerData.lastName} requested ${req.body.amount} for exchange money`,
                                                                                    amouserdeuntType: req.body.amountType,
                                                                                    customer_MobileNumber: customerData.mobileNumber,
                                                                                    admin_MobileNumber: adminData.mobileNumber,
                                                                                    notificationType: "Exchange",
                                                                                }
                                                                                new notificationModel(obj).save((error, sentRequest) => {
                                                                                    if (error) {
                                                                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                                                    }
                                                                                    else {
                                                                                        response(res, SuccessCode.SUCCESS, sentRequest, SuccessMessage.REQUEST_SENT_TO_Agent)
                                                                                    }
                                                                                })
                                                                            }
                                                                            else {
                                                                                response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                                                            }
                                                                        }
                                                                    })

                                                                }
                                                            })
                                                        }
                                                    }
                                                    else {
                                                        if (customerData.amountCDF < req.body.amount || customerData.amountCDF == 0) {
                                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                        }
                                                        else {
                                                            userModel.findOneAndUpdate({ mobileNumber: customerData.mobileNumber }, { $set: { amountCDF: parseFloat(customerData.amountCDF) - parseFloat(req.body.amount) } }, { new: true }, (errCustomer, amtUpdate) => {
                                                                if (errCustomer) {
                                                                    response(res, ErrorCode.SOMETHING_WRONG, [errCustomer], ErrorMessage.INTERNAL_ERROR);
                                                                }
                                                                else {
                                                                    userModel.findOneAndUpdate({ mobileNumber: adminData.mobileNumber }, { $set: { amountCDF: parseFloat(adminData.amountCDF) + parseFloat(req.body.amount) } }, { new: true }, (errCust, amtUpdate) => {
                                                                        if (errCust) {
                                                                            response(res, ErrorCode.SOMETHING_WRONG, [errCust], ErrorMessage.INTERNAL_ERROR);
                                                                        }
                                                                        else {
                                                                            var pass = bcrypt.compareSync(req.body.password, customerData.password)
                                                                            if (pass) {
                                                                                obj = {
                                                                                    name: customerData.firstName + " " + customerData.lastName,
                                                                                    customer_Id: customerData._id,
                                                                                    countryCode: customerData.countryCode,
                                                                                    admin_id: adminData._id,
                                                                                    adminId: adminData.adminId,
                                                                                    amount: req.body.amount,
                                                                                    amountType: req.body.amountType,
                                                                                    notifications: `${customerData.firstName + " " + customerData.lastName} requested ${req.body.amount} for exchange money`,
                                                                                    amouserdeuntType: req.body.amountType,
                                                                                    customer_MobileNumber: customerData.mobileNumber,
                                                                                    admin_MobileNumber: adminData.mobileNumber,
                                                                                    notificationType: "Exchange",
                                                                                }
                                                                                new notificationModel(obj).save((error, sentRequest) => {
                                                                                    if (error) {
                                                                                        response(res, ErrorCode.SOMETHING_WRONG, [sentRequest], ErrorMessage.INTERNAL_ERROR);
                                                                                    }
                                                                                    else {
                                                                                        response(res, SuccessCode.SUCCESS, sentRequest, SuccessMessage.REQUEST_SENT_TO_Agent)
                                                                                    }
                                                                                })
                                                                            }
                                                                            else {
                                                                                response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                                                            }
                                                                        }
                                                                    })

                                                                }
                                                            })
                                                        }
                                                    }

                                                }
                                            })
                                        }
                                    }
                                })
                        }
                    })
                    // var final_result = Object.assign({}, ...newResult)
                    // if (final_result.count > transactionDetails.clientToClient_SEND) {
                    //     response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.LIMIT_NUMBER_OF_TRANSACTION)
                    // }



                    // notificationModel.aggregate([
                    //     { $match: { $and: [{ "createdAt": { "$gte": start, "$lt": end } }, { "sender_id": req.userId }, { "sender_UserType": "CUSTOMER" }, { "receiver_UserType": "AGENT" }] } },
                    //     { "$group": { "_id": mongoose.Types.ObjectId(req.userId), "count": { $sum: 1 } } }], async (err1, newResult) => 
                    //     {})  

                }
            })
      
               
        
    },

    //==============================list of notification for customer=================================
    listOfNotificationForCustomer: (req, res) => {      
          commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, userType: "CUSTOMER", status: "ACTIVE" }, (error, userDetails) => {
                console.log("===========tytytytyty==========>", error, userDetails)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (userDetails.notificationToggle == false) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.TOGGLE_OFF);
                }
                else {
                    notificationModel.find({ customer_Id: userDetails._id }).sort({ 'updatedAt': -1 }).exec((err, notificationList) => {
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else if (notificationList.length == 0) {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.LIST_NOT_FOUND)
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, notificationList, SuccessMessage.DATA_FOUND)
                        }
                    })
                }
            })
             
    },
    // test 
    test: (req,res)=>{
        try{
            res.json({
                message:'tested'
            })
        }
        catch(error){
            res.json({
                message:'error'
            })
        }
    },

    notificationToggle: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: { $in: ["AGENT", "CUSTOMER"] } }, (error, userDetails) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (userDetails.notificationToggle == true) {
                    userModel.findOneAndUpdate({ _id: userDetails._id }, { $set: { notificationToggle: false } }, (errToggle, UpdateToggle) => {
                        if (errToggle) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, UpdateToggle, SuccessMessage.UPDATE_SUCCESS);
                        }
                    })

                }
                else if (userDetails.notificationToggle == false) {
                    userModel.findOneAndUpdate({ _id: userDetails._id }, { $set: { notificationToggle: true } }, (errToggle, UpdateToggle) => {
                        if (errToggle) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, UpdateToggle, SuccessMessage.UPDATE_SUCCESS);
                        }
                    })
                }
            })
       

    },

    sendAdminToKycDetails: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: { $in: ["CUSTOMER", "AGENT"] } }, async (error, userData) => {
                
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    if (req.body.kycDocument.length > 3) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.LIMIT_DOCUMENTS);
                    }
                    else {
                        var arr = []
                        var count = 0
                        req.body.kycDocument.forEach((elem, index) => {
                            commonFunction.uploadImage(elem.uploadDocument, (errImag, resultImage) => {
                                console.log("1115=================", resultImage)
                                if (errImag) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                    arr.push({
                                        "customer_Id": userData._id,
                                        "name": userData.firstName + " " + userData.lastName,
                                        "emailId": userData.emailId,
                                        "cusotmer_mobileNumber": userData.mobileNumber,
                                        "documentName": elem.documentName,
                                        "uploadDocument": resultImage,
                                        "mobileNumber": userData.mobileNumber,
                                        "uploadDate": Date.now(),
                                        "updateDate": Date.now(),
                                        //approvedDate: Date.now()                        
                                    })
                                    count = count + 1;
                                    console.log("ccount", count)
                                    if (count == req.body.kycDocument.length) {
                                        console.log("arrrrrr", arr)
                                        req.body.kycDocument = arr
                                        req.body.fullContactNumber = userData.countryCode + userData.mobileNumber
                                        req.body.customerId = userData._id
                                        req.body.name = userData.firstName + " " + userData.lastName,


                                            new kycModel(req.body).save((error, saveMessage) => {
                                                if (error) {
                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                }
                                                else {
                                                    response(res, SuccessCode.SUCCESS, saveMessage, SuccessMessage.MESSAGE_SENT_KYC)
                                                }
                                            })
                                        //  elem.docs = docResult;
                                    }
                                }

                            })

                        })


                    }

                }
            })
       

    },

    supportMessageToAdmin: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: { $in: ["AGENT", "CUSTOMER"] } }, (error, result) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!result) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {

                
                    obj = {
                        "message": req.body.message
                    }
                    var message1 = obj.message
                    console.log("ssssssss",message1)
                    commonFunction.supportEmailSend(message1)
                    new messageModel(obj).save((error, saveMessage) => {
                        if (error)
                         {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                         
                            response(res, SuccessCode.SUCCESS, saveMessage, SuccessMessage.MESSAGE_SENT)
                        }
                    })
                }
            })
     
    },

    getHelp: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            messageModel.findOne({ helpId: "123" }, (error, result) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    response(res, SuccessCode.SUCCESS, result, SuccessMessage.MESSAGE_SENT)
                }
            })
      
    },

    particularTransaction: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            transactionModel.findOne({ _id: req.body.transactionId }, (error, transactionDetails) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    response(res, SuccessCode.SUCCESS, transactionDetails, SuccessMessage.DATA_FOUND);
                }
            })
       
    },


    transactionHistoryOfCustomer: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: "CUSTOMER" }, (error, customerDetails) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    var query = { $or: [{ sender_id: customerDetails._id }, { receiver_id: customerDetails._id }] }
                    transactionModel.find(query, (err, transactionDetails) => {
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, transactionDetails, SuccessMessage.DATA_FOUND);
                        }
                    })

                }
            })
      

    },

    profileOfFriend: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: { $in: ["AGENT", "CUSTOMER"] } }, (error, result) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    userModel.findOne({ _id: req.body._id, status: "ACTIVE", userType: { $in: ["AGENT", "CUSTOMER"] } },
                        (err, friendProfile) => {
                            if (err) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                            }
                            else if (!friendProfile) {
                                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                            }
                            else {
                                response(res, SuccessCode.SUCCESS, friendProfile, SuccessMessage.DATA_FOUND);
                            }
                        })
                }
            })
       
    },
    history: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: "CUSTOMER" }, (error, result) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    var query = { $or: [{ sender_mobileNumber: result.mobileNumber }, { receiver_mobileNumber: result.mobileNumber }] }
                    transactionModel.find(query, (err, transDetails) => {
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.DATA_FOUND);
                        }
                    })
                }
            })
        
       
    },
    //======================sendMoneyByCustomer
    sendMoneyByCustomer: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, userType: "CUSTOMER", status: "ACTIVE" }, (error, customerDetails) => {
                console.log("====================>", error, customerDetails)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (customerDetails.kycStatus == "unverified") {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.PLEASE_VERIFY_KYC_DETAILS);
                }
                else if (customerDetails.mobileNumber == req.body.mobileNumber) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_VALID_NUMBER);
                }
                else {
                    userModel.findOne({ mobileNumber: req.body.mobileNumber, status: "ACTIVE", userType: { $in: ["ADMIN", "AGENT", "CUSTOMER"] } },
                        async (errNumber, numberDetails) => {
                           
                            if (errNumber) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                            }
                            else if (numberDetails.kycStatus == "unverified") {
                                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.RECEIVER_PLEASE_VERIFY_KYC_DETAILS);
                            }
                            else {
                                transactionModel.aggregate([
                                    { $match: { $and: [{ "createdAt": { "$gte": start, "$lt": end } }, { "sender_id": req.userId }] } },
                                    { "$group": { "_id": mongoose.Types.ObjectId(req.userId), "totalAmount": { $sum: '$send_amount' }, "count": { $sum: 1 } } }], async (err1, newResult) => {
                                        if (err1) {
                                            response(res, ErrorCode.SOMETHING_WRONG, [err1], ErrorMessage.INTERNAL_ERROR);
                                        }
                                        else {
                                            var final_result = Object.assign({}, ...newResult)
                                            var amountOfDay = parseFloat(final_result.totalAmount) + parseFloat(req.body.amount)

                                            if (amountOfDay > 2500) {
                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.LIMIT_SENDING_AMOUNT);
                                            }
                                            else if (final_result.count >= 5) {
                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.LIMIT_NUMBER_OF_TRANSACTION)
                                            }
                                            else {
                                                if (numberDetails.userType == "AGENT" && req.body.amountType == "USD") {
                                                    notification_Status = await notificationModel.findOne({ agent_MobileNumber: numberDetails.mobileNumber, customer_MobileNumber: customerDetails.mobileNumber, status: "approved", notificationType: "Withdraw", transactionStatus: "PENDING", amountType: "USD" })
                                                    
                                                    if (!notification_Status) {
                                                        response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                    }
                                                    if (notification_Status) {
                                                        if (customerDetails.amountUSD < req.body.amount || customerDetails.amountUSD == 0) {
                                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                        }
                                                        if (notification_Status.amount != req.body.amount || notification_Status.amountType != req.body.amountType) {
                                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.PLEASE_CHECK_YOUR_AMOUNT);
                                                        }
                                                        else {
                                                            if (req.body.amountType == "USD" && numberDetails.userType == "AGENT") {
                                                                commissionModel.findOne({ status: "ACTIVE" }, async (errCommission, commissionDetails) => {
                                                                    if (errCommission) {
                                                                        response(res, ErrorCode.SOMETHING_WRONG, error, ErrorMessage.INTERNAL_ERROR);
                                                                    }
                                                                    else {
                                                                        passwordCheck = bcrypt.compareSync(req.body.password, customerDetails.password)
                                                                        if (passwordCheck) {
                                                                            var adminCommission = parseFloat(req.body.amount) * parseFloat(commissionDetails.withdraw_admin_commission / 100)// 10
                                                                            console.log("-====================3333333333333333333333333333", adminCommission)
                                                                            var agentCommission = parseFloat(req.body.amount) * parseFloat(commissionDetails.withdraw_agent_commission / 100)// 1020
                                                                            console.log("-===============44444444444444444444444444444", agentCommission)
                                                                            var final_amount = parseFloat(req.body.amount) - parseFloat(adminCommission + agentCommission)
                                                                            console.log("=========4444444444444444444444444444444444444----------", final_amount)

                                                                            var customerBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: customerDetails.mobileNumber },
                                                                                { $set: { amountUSD: parseFloat(customerDetails.amountUSD) - parseFloat(req.body.amount) } }, { new: true })
                                                                            if (customerBalanceUpdate) {
                                                                                var agentBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: numberDetails.mobileNumber },
                                                                                    { $set: { amountUSD: parseFloat(numberDetails.amountUSD) + parseFloat(final_amount) } }, { new: true })
                                                                                if (agentBalanceUpdate) {
                                                                                    userModel.findOne({ userType: "ADMIN" }, async (error, adminDetails) => {
                                                                                        if (error) {
                                                                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                        }
                                                                                        else {
                                                                                            var admin_commission_updated = await userModel.findOneAndUpdate({ userType: "ADMIN" }, { $set: { commissionUSD: parseFloat(adminDetails.commissionUSD) + parseFloat(adminCommission) } }, { new: true })
                                                                                            console.log("==================", admin_commission_updated)
                                                                                            if (admin_commission_updated) {
                                                                                                var agent_commission_updated = await userModel.findOneAndUpdate({ mobileNumber: numberDetails.mobileNumber }, { $set: { commissionUSD: parseFloat(numberDetails.commissionUSD) + parseFloat(agentCommission) } }, { new: true })
                                                                                                console.log("==================", agent_commission_updated)
                                                                                                if (agent_commission_updated) {
                                                                                                    var notification_update = await notificationModel.findOneAndUpdate({ _id: notification_Status._id }, { $set: { transactionStatus: "COMPLETED" } }, { new: true })
                                                                                                    if (notification_update) {
                                                                                                        var commission_details_obj = {
                                                                                                            "admin_commission": adminCommission,
                                                                                                            "agent_Commission": agentCommission,
                                                                                                            "send_amount": req.body.amount,
                                                                                                            "receive_amount": final_amount,
                                                                                                            "amountType": req.body.amountType,
                                                                                                            "sender_UserType": customerDetails.userType,
                                                                                                            "receiver_UserType": numberDetails.userType
                                                                                                        }
                                                                                                        var commission_Details = new commissionModel(commission_details_obj).save()
                                                                                                        if (commission_Details) {
                                                                                                            var obj_details = {
                                                                                                                "agentId": numberDetails.agentId,
                                                                                                                "agent_id": numberDetails._id,
                                                                                                                "send_amount": req.body.amount,
                                                                                                                "receive_amount": final_amount,
                                                                                                                "commission": parseFloat(adminCommission) + parseFloat(agentCommission),
                                                                                                                "amountType": req.body.amountType,
                                                                                                                "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                                                "receiveMoneyBy": numberDetails.firstName + " " + lastName.firstName,
                                                                                                                "sender_id": customerDetails._id,
                                                                                                                //"receiver_id": numberDetails._id,
                                                                                                                "sender_mobileNumber": customerDetails.mobileNumber,
                                                                                                                "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                                                "sender_UserType": customerDetails.userType,
                                                                                                                "receiver_UserType": numberDetails.userType,
                                                                                                                "notificationType": notification_Status.notificationType,
                                                                                                                "transactionStatus": "Debited",
                                                                                                                "transectionType": "paid"
                                                                                                            }
                                                                                                            new transactionModel(obj_details).save((errTransaction, transactionDetails) => {
                                                                                                                console.log("=============>", errTransaction)
                                                                                                                if (errTransaction) {
                                                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                                                }
                                                                                                                else {
                                                                                                                    receiver_details = {
                                                                                                                        "agentId": numberDetails.agentId,
                                                                                                                        "agent_id": numberDetails._id,
                                                                                                                        "send_amount": req.body.amount,
                                                                                                                        "receive_amount": final_amount,
                                                                                                                        "commission": parseFloat(adminCommission) + parseFloat(agentCommission),
                                                                                                                        "amountType": req.body.amountType,
                                                                                                                        "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                                                        "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                                                        //"sender_id": customerDetails._id,
                                                                                                                        "receiver_id": numberDetails._id,
                                                                                                                        "sender_mobileNumber": customerDetails.mobileNumber,
                                                                                                                        "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                                                        "sender_UserType": customerDetails.userType,
                                                                                                                        "receiver_UserType": numberDetails.userType,
                                                                                                                        "notificationType": notification_Status.notificationType,
                                                                                                                        "transactionStatus": "Credited",
                                                                                                                        "transectionType": "Recieved"
                                                                                                                    }
                                                                                                                    new transactionModel(receiver_details).save((errTrans, transDetails) => {
                                                                                                                        if (errTrans) {
                                                                                                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                                                        }
                                                                                                                        else {
                                                                                                                            response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                                                                        }
                                                                                                                    })
                                                                                                                }
                                                                                                            })

                                                                                                        }

                                                                                                    }

                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    })
                                                                                }
                                                                            }
                                                                        }
                                                                        else {
                                                                            response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                                                        }

                                                                    }
                                                                })
                                                            }

                                                        }
                                                    }
                                                }
                                                else if (numberDetails.userType == "AGENT" && req.body.amountType == "CDF") {
                                                    notification_Status = await notificationModel.findOne({ agent_MobileNumber: numberDetails.mobileNumber, customer_MobileNumber: customerDetails.mobileNumber, status: "approved", notificationType: "Add", transactionStatus: "PENDING", amountType: "CDF" })
                                                    if (!notification_Status) {
                                                        response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                    }
                                                    if (notification_Status) {
                                                        if (numberDetails.amountCDF < req.body.amount || numberDetails.amountCDF == 0) {
                                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                        }
                                                        if (notification_Status.amount != req.body.amount || notification_Status.amountType != req.body.amountType) {
                                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                        }
                                                        else {
                                                            if (numberDetails.userType == "AGENT" && req.body.amountType == "CDF") {
                                                                commissionModel.findOne({ status: "ACTIVE" }, async (errCommission, commissionDetails) => {
                                                                    if (errCommission) {
                                                                        response(res, ErrorCode.SOMETHING_WRONG, error, ErrorMessage.INTERNAL_ERROR);
                                                                    }
                                                                    else {
                                                                        passwordCheck = bcrypt.compareSync(req.body.password, numberDetails.password)
                                                                        if (passwordCheck) {
                                                                            var adminCommission = parseFloat(req.body.amount) * parseFloat(commissionDetails.deposit_admin_commission / 100)// 10
                                                                            console.log("-====================3333333333333333333333333333", adminCommission)
                                                                            var agentCommission = parseFloat(req.body.amount) * parseFloat(commissionDetails.deposit_agent_Commission / 100)// 1020
                                                                            console.log("-===============44444444444444444444444444444", agentCommission)
                                                                            var final_amount = parseFloat(req.body.amount) - parseFloat(adminCommission + agentCommission)
                                                                            console.log("=========4444444444444444444444444444444444444----------", final_amount)
                                                                            var agentBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: numberDetails.mobileNumber },
                                                                                { $set: { amountCDF: parseFloat(numberDetails.amountCDF) - parseFloat(req.body.amount) } }, { new: true })
                                                                            if (agentBalanceUpdate) {
                                                                                var customerBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: customerDetails.mobileNumber },
                                                                                    { $set: { amountCDF: parseFloat(customerDetails.amountCDF) + parseFloat(final_amount) } }, { new: true })
                                                                                if (customerBalanceUpdate) {
                                                                                    userModel.findOne({ userType: "ADMIN" }, async (error, adminDetails) => {
                                                                                        if (error) {
                                                                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                        }
                                                                                        else {
                                                                                            var admin_commission_updated = await userModel.findOneAndUpdate({ userType: "ADMIN" }, { $set: { commissionCDF: parseFloat(adminDetails.commissionCDF) + parseFloat(adminCommission) } }, { new: true })
                                                                                            console.log("==================", admin_commission_updated)
                                                                                            if (admin_commission_updated) {
                                                                                                var agent_commission_updated = await userModel.findOneAndUpdate({ mobileNumber: numberDetails.mobileNumber }, { $set: { commissionCDF: parseFloat(numberDetails.commissionCDF) + parseFloat(agentCommission) } }, { new: true })
                                                                                                console.log("==================", agent_commission_updated)
                                                                                                if (agent_commission_updated) {
                                                                                                    var notification_update = await notificationModel.findOneAndUpdate({ _id: notification_Status._id }, { $set: { transactionStatus: "COMPLETED" } }, { new: true })
                                                                                                    if (notification_update) {
                                                                                                        var commission_details_obj = {
                                                                                                            "admin_commission": adminCommission,
                                                                                                            "agent_Commission": agentCommission,
                                                                                                            "send_amount": req.body.amount,
                                                                                                            "receive_amount": final_amount,
                                                                                                            "amountType": req.body.amountType,
                                                                                                            "sender_UserType": customerDetails.userType,
                                                                                                            "receiver_UserType": numberDetails.userType
                                                                                                        }
                                                                                                        var commission_Details = new commissionModel(commission_details_obj).save()
                                                                                                        if (commission_Details) {
                                                                                                            var obj_details = {
                                                                                                                "agentId": numberDetails.agentId,
                                                                                                                "agent_id": numberDetails._id,
                                                                                                                "send_amount": req.body.amount,
                                                                                                                "receive_amount": final_amount,
                                                                                                                "commission": parseFloat(adminCommission) + parseFloat(agentCommission),
                                                                                                                "amountType": req.body.amountType,
                                                                                                                "sendMoneyBy": numberDetails.name,
                                                                                                                "receiveMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                                                "sender_id": numberDetails._id,
                                                                                                                "receiver_id": customerDetails._id,
                                                                                                                "sender_mobileNumber": numberDetails.mobileNumber,
                                                                                                                "receiver_mobileNumber": customerDetails.mobileNumber,
                                                                                                                "sender_UserType": numberDetails.userType,
                                                                                                                "receiver_UserType": customerDetails.userType,
                                                                                                                "notificationType": notification_Status.notificationType,
                                                                                                                "transactionStatus": "Credited",
                                                                                                                "transectionType": "Recieved"
                                                                                                            }
                                                                                                            new transactionModel(obj_details).save((errTransaction, transactionDetails) => {
                                                                                                                if (errTransaction) {
                                                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                                                }
                                                                                                                else {
                                                                                                                    response(res, SuccessCode.SUCCESS, transactionDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                                                                }
                                                                                                            })
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    })
                                                                                }
                                                                            }

                                                                        }
                                                                        else {
                                                                            response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                                                        }
                                                                    }
                                                                })

                                                            }
                                                        }
                                                    }
                                                }
                                                else if (numberDetails.userType == "CUSTOMER" && req.body.amountType == "USD") {
                                                    if (customerDetails.amountUSD < req.body.amount || customerDetails.amountUSD == 0) {
                                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                    }
                                                    else {
                                                        var _passwordCheck = bcrypt.compareSync(req.body.password, customerDetails.password)
                                                        if (_passwordCheck) {
                                                            var customerBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: customerDetails.mobileNumber },
                                                                { $set: { amountUSD: parseFloat(customerDetails.amountUSD) - parseFloat(req.body.amount) } }, { new: true })
                                                            if (customerBalanceUpdate) {
                                                                var receiverBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: numberDetails.mobileNumber },
                                                                    { $set: { amountUSD: parseFloat(numberDetails.amountUSD) + parseFloat(req.body.amount) } }, { new: true })
                                                                if (receiverBalanceUpdate) {
                                                                    var transaction_details = {
                                                                        "send_amount": req.body.amount,
                                                                        "receive_amount": req.body.amount,
                                                                        "amountType": req.body.amountType,
                                                                        "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                        "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                        "sender_id": customerDetails._id,
                                                                        // "receiver_id": numberDetails._id,
                                                                        "sender_mobileNumber": customerDetails.mobileNumber,
                                                                        "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                        "sender_UserType": customerDetails.userType,
                                                                        "receiver_UserType": numberDetails.userType,
                                                                        "transactionStatus": "Debited",
                                                                        "transectionType": "paid"
                                                                    }
                                                                    new transactionModel(transaction_details).save((errTransaction, transactionDetails) => {
                                                                        if (errTransaction) {
                                                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                        }
                                                                        else {
                                                                            receiver_details = {
                                                                                "send_amount": req.body.amount,
                                                                                "receive_amount": req.body.amount,
                                                                                "amountType": req.body.amountType,
                                                                                "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                // "sender_id": customerDetails._id,
                                                                                "receiver_id": numberDetails._id,
                                                                                "sender_mobileNumber": customerDetails.mobileNumber,
                                                                                "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                "sender_UserType": customerDetails.userType,
                                                                                "receiver_UserType": numberDetails.userType,
                                                                                "transactionStatus": "Credited",
                                                                                "transectionType": "Recieved"
                                                                            }
                                                                            new transactionModel(receiver_details).save((errTran, transDetails) => {
                                                                                if (errTran) {
                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                }
                                                                                else {
                                                                                    response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.TRANSACTION_COMPLETED);

                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        }
                                                        else {
                                                            response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                                        }

                                                    }
                                                }
                                                else if (numberDetails.userType == "CUSTOMER" && req.body.amountType == "CDF") {
                                                    console.log("===========================i am here===========>")
                                                    if (customerDetails.amountCDF < req.body.amount || customerDetails.amountCDF == 0) {
                                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                    }
                                                    else {
                                                        var password_Check_ = bcrypt.compareSync(req.body.password, customerDetails.password)
                                                        if (password_Check_) {
                                                            var customer_BalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: customerDetails.mobileNumber },
                                                                { $set: { amountCDF: parseFloat(customerDetails.amountCDF) - parseFloat(req.body.amount) } }, { new: true })
                                                            if (customer_BalanceUpdate) {
                                                                var receiver_BalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: numberDetails.mobileNumber },
                                                                    { $set: { amountCDF: parseFloat(numberDetails.amountCDF) + parseFloat(req.body.amount) } }, { new: true })
                                                                if (receiver_BalanceUpdate) {
                                                                    var _transaction_details = {
                                                                        "send_amount": req.body.amount,
                                                                        "receive_amount": req.body.amount,
                                                                        "amountType": req.body.amountType,
                                                                        "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                        "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                        "sender_id": customerDetails._id,
                                                                        //"receiver_id": numberDetails._id,
                                                                        "sender_mobileNumber": customerDetails.mobileNumber,
                                                                        "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                        "sender_UserType": customerDetails.userType,
                                                                        "receiver_UserType": numberDetails.userType,
                                                                        "transactionStatus": "Debited",
                                                                        "transectionType": "paid"
                                                                    }
                                                                    new transactionModel(_transaction_details).save((errTransaction, transactionDetails) => {
                                                                        if (errTransaction) {
                                                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                        }
                                                                        else {
                                                                            receiver_details = {
                                                                                "send_amount": req.body.amount,
                                                                                "receive_amount": req.body.amount,
                                                                                "amountType": req.body.amountType,
                                                                                "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                // "sender_id": customerDetails._id,
                                                                                "receiver_id": numberDetails._id,
                                                                                "sender_mobileNumber": customerDetails.mobileNumber,
                                                                                "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                "sender_UserType": customerDetails.userType,
                                                                                "receiver_UserType": numberDetails.userType,
                                                                                "transactionStatus": "Credited",
                                                                                "transectionType": "Recieved"
                                                                            }
                                                                            new transactionModel(receiver_details).save((errTrans, transDetails) => {
                                                                                if (errTrans) {
                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                }
                                                                                else {
                                                                                    response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        }
                                                        else {
                                                            response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                                        }

                                                    }
                                                }
                                                else if (numberDetails.userType == "ADMIN" && req.body.amountType == "USD") {
                                                    if (customerDetails.amountUSD < req.body.amount || customerDetails.amountUSD == 0) {
                                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                    }
                                                    else {
                                                        var _password_Check = bcrypt.compareSync(req.body.password, customerDetails.password)
                                                        if (_password_Check) {
                                                            var customer_Balance_Update = await userModel.findOneAndUpdate({ mobileNumber: customerDetails.mobileNumber },
                                                                { $set: { amountUSD: parseFloat(customerDetails.amountUSD) - parseFloat(req.body.amount) } }, { new: true })
                                                            if (customer_Balance_Update) {
                                                                var receiver_Balance_Update = await userModel.findOneAndUpdate({ mobileNumber: numberDetails.mobileNumber },
                                                                    { $set: { amountUSD: parseFloat(numberDetails.amountUSD) + parseFloat(req.body.amount) } }, { new: true })
                                                                if (receiver_Balance_Update) {
                                                                    var trans_details = {
                                                                        "send_amount": req.body.amount,
                                                                        "receive_amount": req.body.amount,
                                                                        "amountType": req.body.amountType,
                                                                        "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                        "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                        "sender_id": customerDetails._id,
                                                                        //"receiver_id": numberDetails._id,
                                                                        "sender_mobileNumber": customerDetails.mobileNumber,
                                                                        "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                        "sender_UserType": customerDetails.userType,
                                                                        "receiver_UserType": numberDetails.userType,
                                                                        "transactionStatus": "Debited",
                                                                        "transectionType": "paid"
                                                                    }
                                                                    new transactionModel(trans_details).save((errTransaction, transactionDetails) => {
                                                                        if (errTransaction) {
                                                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                        }
                                                                        else {
                                                                            receiver_details = {
                                                                                "send_amount": req.body.amount,
                                                                                "receive_amount": req.body.amount,
                                                                                "amountType": req.body.amountType,
                                                                                "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                //"sender_id": customerDetails._id,
                                                                                "receiver_id": numberDetails._id,
                                                                                "sender_mobileNumber": customerDetails.mobileNumber,
                                                                                "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                "sender_UserType": customerDetails.userType,
                                                                                "receiver_UserType": numberDetails.userType,
                                                                                "transactionStatus": "Credited",
                                                                                "transectionType": "Recieved"
                                                                            }
                                                                            new transactionModel(receiver_details).save((errTrans, transDetails) => {
                                                                                if (errTrans) {
                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                }
                                                                                else {
                                                                                    response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        }
                                                        else {
                                                            response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                                        }
                                                    }
                                                }
                                                else if (numberDetails.userType == "ADMIN" && req.body.amountType == "CDF") {
                                                    if (customerDetails.amountCDF < req.body.amount || customerDetails.amountCDF == 0) {
                                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                    }
                                                    else {
                                                        var check_pasword_ = bcrypt.compareSync(req.body.password, customerDetails.password)
                                                        if (check_pasword_) {
                                                            var customer_BalanceUpdate_ = await userModel.findOneAndUpdate({ mobileNumber: customerDetails.mobileNumber },
                                                                { $set: { amountCDF: parseFloat(customerDetails.amountCDF) - parseFloat(req.body.amount) } }, { new: true })
                                                            if (customer_BalanceUpdate_) {
                                                                var receiver_BalanceUpdate_ = await userModel.findOneAndUpdate({ mobileNumber: numberDetails.mobileNumber },
                                                                    { $set: { amountCDF: parseFloat(numberDetails.amountCDF) + parseFloat(req.body.amount) } }, { new: true })
                                                                if (receiver_BalanceUpdate_) {
                                                                    var trans_details_ = {
                                                                        "send_amount": req.body.amount,
                                                                        "receive_amount": req.body.amount,
                                                                        "amountType": req.body.amountType,
                                                                        "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                        "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                        "sender_id": customerDetails._id,
                                                                        //"receiver_id": numberDetails._id,
                                                                        "sender_mobileNumber": customerDetails.mobileNumber,
                                                                        "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                        "sender_UserType": customerDetails.userType,
                                                                        "receiver_UserType": numberDetails.userType,
                                                                        "transactionStatus": "Debited",
                                                                        "transectionType": "paid"
                                                                    }
                                                                    new transactionModel(trans_details_).save((errTransaction, transactionDetails) => {
                                                                        if (errTransaction) {
                                                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                        }
                                                                        else {
                                                                            receiver_details = {
                                                                                "send_amount": req.body.amount,
                                                                                "receive_amount": req.body.amount,
                                                                                "amountType": req.body.amountType,
                                                                                "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                // "sender_id": customerDetails._id,
                                                                                "receiver_id": numberDetails._id,
                                                                                "sender_mobileNumber": customerDetails.mobileNumber,
                                                                                "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                "sender_UserType": customerDetails.userType,
                                                                                "receiver_UserType": numberDetails.userType,
                                                                                "transactionStatus": "Credited",
                                                                                "transectionType": "Recieved"
                                                                            }
                                                                            new transactionModel(receiver_details).save((errTrans, transDetails) => {
                                                                                if (errTrans) {
                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                }
                                                                                else {
                                                                                    response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        }
                                                        else {
                                                            response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    })
                            }
                        })
                }
            })
     
    },

    receiverDetails: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ mobileNumber: req.body.mobileNumber, status: "ACTIVE", userType: { $in: ["AGENT", "CUSTOMER"] } }, (error, userDetails) => {
                console.log("==================>", error, userDetails)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!userDetails) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, userDetails, SuccessMessage.DATA_FOUND)
                }
            })
       
    },

    getKycDetails: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: { $in: ["AGENT", "CUSTOMER"] } }, (error, userDetails) => {
                console.log("==================>", error, userDetails)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    var obj = {
                        "kycStatus": userDetails.kycStatus
                    }
                    response(res, SuccessCode.SUCCESS, obj, SuccessMessage.DATA_FOUND)

                }
            })
    

    },

    addStatus: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: "CUSTOMER" }, (error, userDetails) => {
                console.log("==================>", error, userDetails)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    userModel.findOneAndUpdate({ _id: userDetails._id }, { $set: { addStatus: req.body.addStatus, updatedAt: Date.now() } }, { new: true }, (errStatus, setStatus) => {
                        if (errStatus) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, setStatus, SuccessMessage.UPDATE_SUCCESSFULLY);
                        }
                    })
                }
            })
      

    },
    particularPost: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            postModel.findOne({ _id: req.body.postId, status: "ACTIVE" }, (err, postDetails) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    response(res, SuccessCode.SUCCESS, postDetails, SuccessMessage.DATA_FOUND);
                }
            })
     
    },
    postList: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: "CUSTOMER" }, (error, customerData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    postModel.find({ status: "ACTIVE" }, (err, postlist) => {
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, postlist, SuccessMessage.DATA_FOUND);

                        }
                    })
                }
            })
      
    },
    statusList: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: "CUSTOMER" }, (error, userDetails) => {
                console.log("==================>", userDetails.mobileNumber, userDetails._id)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    var query = { $and: [{ addStatus: { $ne: "" } }, { _id: { $ne: userDetails._id } }, { userType: "CUSTOMER" }] }
                    userModel.find(query).select('addStatus profilePic firstName lastName').exec(function (err, getDetails) {
                        console.log("============>", err, getDetails)
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, getDetails, SuccessMessage.DATA_FOUND);
                        }
                    })
                }
            })
      
    },
    /**
   * Function Name :block agent by customer
   * Description   :block agent by customer and move to block page
   *
   * @return response
   */
    likePost: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE" }, (error, result) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    postModel.findOne({ _id: req.body.postId }, (err, postDetails) => {
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            var obj_like = { likerId: result._id }
                            let isPresent = postDetails.like.find(o => o.likerId == result._id);
                            if (isPresent) {
                                postModel.findOneAndUpdate({ _id: postDetails._id }, { $pull: { like: obj_like }, $set: { count: postDetails.like.length - 1 } }, { new: true },
                                    (errId, postlike) => {
                                        if (errId) {
                                            response(res, ErrorCode.SOMETHING_WRONG, [errId], ErrorMessage.INTERNAL_ERROR);
                                        }
                                        else {
                                            response(res, SuccessCode.SUCCESS, postlike, SuccessMessage.POST_UNLIKED);
                                        }
                                    })
                            }
                            else {
                                postModel.findOneAndUpdate({ _id: postDetails._id }, { $push: { like: obj_like }, $set: { count: postDetails.like.length + 1 } }, { new: true },
                                    (errId, postlike) => {
                                        if (errId) {
                                            response(res, ErrorCode.SOMETHING_WRONG, [errId], ErrorMessage.INTERNAL_ERROR);
                                        }
                                        else {
                                            response(res, SuccessCode.SUCCESS, postlike, SuccessMessage.POST_LIKED);
                                        }
                                    })
                            }

                        }
                    })
                }
            })
      
    },
    currentBalance: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, userType: ["AGENT", "CUSTOMER"] }, (error, userDetails) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    var obj_bal = {
                        USD: userDetails.amountUSD,
                        CDF: userDetails.amountCDF
                    }
                    response(res, SuccessCode.SUCCESS, obj_bal, SuccessMessage.DATA_FOUND);
                }
            })
      
    },
    likeCount: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            postModel.findOne({ _id: req.body.postId }, (err, countResult) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    response(res, SuccessCode.SUCCESS, countResult.like.length, SuccessMessage.DATA_FOUND);
                }
            })
       

    },
    comment: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE" }, (error, userDetails) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    var comment_details = {
                        "commentUser": userDetails._id,
                        "firstName": userDetails.firstName,
                        "lastName": userDetails.lastName,
                        "profilePic": userDetails.profilePic,
                        "comment": req.body.comment,
                    }
                    postModel.findOneAndUpdate({ _id: req.body.postId, status: "ACTIVE" }, { $push: { comment: comment_details } }, { new: true }, (errComment, commentPush) => {
                        if (errComment) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else if (!commentPush) {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, commentPush, SuccessMessage.COMMENT_SUCCESSFULLY);
                        }
                    })
                }
            })
     
    },

    commentReply: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE" }, (error, userDetails) => {
                //console.log("==============>",userDetails)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    postModel.findOne({ _id: req.body.postId, "comment._id": req.body.commentId }, { "comment.$": 1 }, (err, result) => {
                        console.log("================err,result", err, result)
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            var replay_details = {
                                "commentUser": userDetails._id,
                                "firstName": userDetails.firstName,
                                "lastName": userDetails.lastName,
                                "commentId": req.body.commentId,
                                "profilePic": userDetails.profilePic,
                                "comment": req.body.comment,
                            }
                            postModel.findOneAndUpdate({ _id: req.body.postId, status: "ACTIVE" }, { $push: { replayComment: replay_details } }, { new: true }, (errComment, commentPush) => {
                                if (errComment) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else if (!commentPush) {
                                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                                }
                                else {
                                    response(res, SuccessCode.SUCCESS, commentPush, SuccessMessage.COMMENT_SUCCESSFULLY);
                                }
                            })
                        }
                    })
                }
            })
       
    },
    blockPost: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE" }, (error, userDetails) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    postModel.findOneAndUpdate({ _id: req.body, postId, status: "ACTIVE" }, { $set: { status: "BLOCK" } }, { new: true },
                        (err, postDetails) => {
                            if (err) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                            }
                            else if (!postDetails) {
                                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.POST_NOT_FOUND);
                            }
                            else {
                                response(res, SuccessCode.SUCCESS, postDetails, SuccessMessage.BLOCK_SUCCESS);
                            }
                        })
                }
            })
       

    },
    reportPost: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE" }, (error, userDetails) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    var report_obj = {
                        "report": req.body.report,
                        "userId": userDetails._id
                    }
                    postModel.findOneAndUpdate({ _id: req.body.postId, status: "ACTIVE" }, { $push: { report: report_obj } }, { new: true }, (errReport, reportPush) => {
                        if (errReport) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else if (!reportPush) {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.POST_NOT_FOUND);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, reportPush, SuccessMessage.REPORT_SUCESSFULLY);
                        }
                    })

                }
            })
       
    },
    tagFriend: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, status: "ACTIVE" }, (error, userDetails) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    postModel.findOneAndUpdate({ _id: req.body.postId }, { $push: { tag: req.body.tagFriend } }, { new: true }, (errTag, pushTag) => {
                        if (errTag) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, pushTag, SuccessMessage.REPORT_SUCESSFULLY);
                        }
                    })
                }
            })
      

    },
    searchPost: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            postModel.find({ $or: [{ firstName: { $regex: req.body.search, $options: 'i' } }, { lastName: { $regex: req.body.search, $options: 'i' } }] }, (err, result) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (result.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, result, SuccessMessage.DATA_FOUND);
                }
            })
       

    },

    amountTesting: async (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, userType: "CUSTOMER" }, async (err, result) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [err], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    // var limitee = await sendLimitAndNumberOfTransaction(result._id)
                    // console.log("==========================>",limitee)
                    // if(limitee.totalAmount > 1500){
                    //     response(res, ErrorCode.SOMETHING_WRONG, [err], ErrorMessage.INTERNAL_ERROR);  
                    // }
                    // else if(limitee.count > 3){
                    //     response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)

                    // }
                    // else{
                    //         response(res, SuccessCode.SUCCESS, result, SuccessMessage.TRANSACTION_COMPLETED);

                    // }    
                    var pipeline = [
                        { $match: { $and: [{ "createdAt": { "$gte": start, "$lt": end } }, { "sender_id": req.userId }, { amountType: "USD" }] } },
                        {
                            "$group": {
                                "_id": mongoose.Types.ObjectId(req.userId),
                                "totalAmount": { $sum: '$send_amount' },
                                "count": { $sum: 1 }
                            }
                        }
                    ];

                    transactionModel.aggregate(pipeline, function (err, newResult) {
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [err], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            // var array=[{interf:'IPerson',name:'Someone'},{clss:'Person',name:'Ahmed'},{student:true}];
                            // console.log(
                            //     Object.assign(...array) // Object.assign(array[0],array[1],array[2])
                            // )
                            console.log("===============eeww====>", newResult)
                            var final_result = Object.assign({}, ...newResult)
                            console.log("======eeeeeeeeeeee======>", final_result.totalAmount)
                            console.log("======eeeeeeeeeerrrrrrrrrrrrrrrrrrrrreeeeeee======>", final_result.count)

                            if (final_result.totalAmount > 2500) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.TOGGLE_OFF);
                            }
                            else if (final_result.count > 2) {
                                response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)

                            }
                            else {
                                response(res, SuccessCode.SUCCESS, result, SuccessMessage.TRANSACTION_COMPLETED);

                            }

                            // response(res, SuccessCode.SUCCESS, final_result, SuccessMessage.TRANSACTION_COMPLETED);
                        }

                    });
                }
            })
       
    },

    setTimeandLimit: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, userType: "CUSTOMER" }, async (err, result) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [err], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    transactionModel.aggregate([
                        { $match: { $and: [{ "createdAt": { "$gte": start, "$lt": end } }, { "sender_id": req.userId }, { amountType: "USD" }] } },
                        { "$group": { "_id": mongoose.Types.ObjectId(req.userId), "totalAmount": { $sum: '$send_amount' }, "count": { $sum: 1 } } }], (err1, newResult) => {
                            if (err1) {
                                response(res, ErrorCode.SOMETHING_WRONG, [err1], ErrorMessage.INTERNAL_ERROR);
                            }
                            else {
                                var final_result = Object.assign({}, ...newResult)
                                if (final_result.totalAmount > 2100) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.TOGGLE_OFF);
                                }
                                else if (final_result.count > 1) {
                                    response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)

                                }
                                else {
                                    response(res, SuccessCode.SUCCESS, result, SuccessMessage.TRANSACTION_COMPLETED);
                                }
                            }
                        })

                }
            })
      
    },

    //=======================sendMoneyByCustomerUsingQRcode===============================================================//
    sendMoneyByCustomerUsingQRcode: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId, userType: "CUSTOMER", status: "ACTIVE" }, (error, customerDetails) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (customerDetails.kycStatus == "unverified") {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.PLEASE_VERIFY_KYC_DETAILS);
                }
                else if (customerDetails.mobileNumber == req.body.mobileNumber) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_VALID_NUMBER);
                }
                else {
                    userModel.findOne({ mobileNumber: req.body.mobileNumber, status: "ACTIVE", userType: { $in: ["ADMIN", "AGENT", "CUSTOMER"] } },
                        async (errNumber, numberDetails) => {
                            if (errNumber) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                            }
                            else if (!numberDetails) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.USER_NOT_FOUND);
                            }
                            else {
                                newcommissionModel.findOne({ status: "ACTIVE" }, (errComm, commissionDetails) => {
                                    if (errComm) {
                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                    }
                                    else {
                                        moneyModel.findOne({ status: "ACTIVE" }, (errDetails, transactionDetails) => {
                                            if (errDetails) {
                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                            }
                                            else {
                                                console.log("transsssssssssssss", transactionDetails)
                                                if (numberDetails.userType == "CUSTOMER" && req.body.amountType == "USD") {
                                                    transactionModel.aggregate([
                                                        { $match: { $and: [{ "createdAt": { "$gte": start, "$lt": end } }, { "sender_id": req.userId }, { "sender_UserType": "CUSTOMER" }, { "receiver_UserType": "CUSTOMER" }] } },
                                                        { "$group": { "_id": mongoose.Types.ObjectId(req.userId), "count": { $sum: 1 } } }], async (err1, newResult) => {
                                                            if (err1) {
                                                                response(res, ErrorCode.SOMETHING_WRONG, [err1], ErrorMessage.INTERNAL_ERROR);
                                                            }
                                                            else {
                                                                var final_result = Object.assign({}, ...newResult)
                                                                if (final_result.count >= transactionDetails.clientToClient_SEND) {
                                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.LIMIT_NUMBER_OF_TRANSACTION)
                                                                }
                                                                else if (customerDetails.amountUSD < req.body.amount || customerDetails.amountUSD == 0) {
                                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                                }
                                                                else {
                                                                    passwordCheck = bcrypt.compareSync(req.body.password, customerDetails.password)
                                                                    if (passwordCheck) {
                                                                        console.log("======================>", commissionDetails.client_To_Client)
                                                                        var client_comm = parseFloat(req.body.amount) * parseFloat(commissionDetails.client_To_Client / 100)// 10
                                                                        console.log("111111111111111", client_comm)
                                                                        var final_amount = parseFloat(req.body.amount) - parseFloat(client_comm)

                                                                        console.log("=========4444444444444444444444444444444444444----------", final_amount)
                                                                        var customerBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: customerDetails.mobileNumber },
                                                                            { $set: { amountUSD: parseFloat(customerDetails.amountUSD) - parseFloat(final_amount) } }, { new: true })
                                                                        if (customerBalanceUpdate) {
                                                                            var receiver_cust = await userModel.findOneAndUpdate({ mobileNumber: numberDetails.mobileNumber },
                                                                                { $set: { amountUSD: parseFloat(numberDetails.amountUSD) + parseFloat(final_amount) } }, { new: true })
                                                                            if (receiver_cust) {
                                                                                var commission_details_obj = {
                                                                                    "send_amount": req.body.amount,
                                                                                    "receive_amount": final_amount,
                                                                                    "commission": commissionDetails.client_To_Client,
                                                                                    "amountType": req.body.amountType,
                                                                                    "sender_UserType": customerDetails.userType,
                                                                                    "receiver_UserType": numberDetails.userType
                                                                                }
                                                                                var commission_Details = new newcommissionModel(commission_details_obj).save()
                                                                                if (commission_Details) {
                                                                                    var obj_details = {
                                                                                        "send_amount": req.body.amount,
                                                                                        "receive_amount": final_amount,
                                                                                        "commission": commissionDetails.client_To_Client,
                                                                                        "commission_amount": client_comm,
                                                                                        "amountType": req.body.amountType,
                                                                                        "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                        "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                        "sender_id": customerDetails._id,
                                                                                        "sender_mobileNumber": customerDetails.mobileNumber,
                                                                                        "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                        "sender_UserType": customerDetails.userType,
                                                                                        "receiver_UserType": numberDetails.userType,
                                                                                        "type_transaction": "SEND",
                                                                                        "transactionStatus": "Debited",
                                                                                        "transectionType": "paid"
                                                                                    }
                                                                                    new transactionModel(obj_details).save((errTransaction, transactionDetails) => {
                                                                                        console.log("=============>", errTransaction)
                                                                                        if (errTransaction) {
                                                                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                        }
                                                                                        else {
                                                                                            receiver_details = {
                                                                                                "send_amount": req.body.amount,
                                                                                                "receive_amount": final_amount,
                                                                                                "commission": commissionDetails.client_To_Client,
                                                                                                "amountType": req.body.amountType,
                                                                                                "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                                "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                                "receiver_id": numberDetails._id,
                                                                                                "sender_mobileNumber": customerDetails.mobileNumber,
                                                                                                "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                                "sender_UserType": customerDetails.userType,
                                                                                                "receiver_UserType": numberDetails.userType,
                                                                                                "transactionStatus": "Credited",
                                                                                                "transectionType": "Recieved"
                                                                                            }
                                                                                            new transactionModel(receiver_details).save((errTrans, transDetails) => {
                                                                                                if (errTrans) {
                                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                                }
                                                                                                else {
                                                                                                    response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                                                }
                                                                                            })
                                                                                        }
                                                                                    })

                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                            }

                                                        })
                                                }
                                                else if (numberDetails.userType == "CUSTOMER" && req.body.amountType == "CDF") {
                                                    transactionModel.aggregate([
                                                        { $match: { $and: [{ "createdAt": { "$gte": start, "$lt": end } }, { "sender_id": req.userId }, { "sender_UserType": "CUSTOMER" }, { "receiver_UserType": "CUSTOMER" }] } },
                                                        { "$group": { "_id": mongoose.Types.ObjectId(req.userId), "count": { $sum: 1 } } }], async (err1, newResult) => {
                                                            if (err1) {
                                                                response(res, ErrorCode.SOMETHING_WRONG, [err1], ErrorMessage.INTERNAL_ERROR);
                                                            }
                                                            else {
                                                                var final_result = Object.assign({}, ...newResult)
                                                                if (final_result.count >= transactionDetails.clientToClient_SEND) {
                                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.LIMIT_NUMBER_OF_TRANSACTION)
                                                                }
                                                                else if (customerDetails.amountCDF < req.body.amount || customerDetails.amountCDF == 0) {
                                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                                }
                                                                else {
                                                                    passwordCheck = bcrypt.compareSync(req.body.password, customerDetails.password)
                                                                    if (passwordCheck) {
                                                                        console.log("======================>", commissionDetails.client_To_Client)
                                                                        var client_comm = parseFloat(req.body.amount) * parseFloat(commissionDetails.client_To_Client / 100)// 10
                                                                        console.log("111111111111111", client_comm)
                                                                        var final_amount = parseFloat(req.body.amount) - parseFloat(client_comm)

                                                                        console.log("=========4444444444444444444444444444444444444----------", final_amount)
                                                                        var customerBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: customerDetails.mobileNumber },
                                                                            { $set: { amountCDF: parseFloat(customerDetails.amountCDF) - parseFloat(final_amount) } }, { new: true })
                                                                        if (customerBalanceUpdate) {
                                                                            var receiver_cust = await userModel.findOneAndUpdate({ mobileNumber: numberDetails.mobileNumber },
                                                                                { $set: { amountCDF: parseFloat(numberDetails.amountCDF) + parseFloat(final_amount) } }, { new: true })
                                                                            if (receiver_cust) {
                                                                                var commission_details_obj = {
                                                                                    "send_amount": req.body.amount,
                                                                                    "receive_amount": final_amount,
                                                                                    "commission": commissionDetails.client_To_Client,
                                                                                    "amountType": req.body.amountType,
                                                                                    "sender_UserType": customerDetails.userType,
                                                                                    "receiver_UserType": numberDetails.userType
                                                                                }
                                                                                var commission_Details = new newcommissionModel(commission_details_obj).save()
                                                                                if (commission_Details) {
                                                                                    var obj_details = {
                                                                                        "send_amount": req.body.amount,
                                                                                        "receive_amount": final_amount,
                                                                                        "commission": commissionDetails.client_To_Client,
                                                                                        "commission_amount": client_comm,
                                                                                        "amountType": req.body.amountType,
                                                                                        "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                        "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                        "sender_id": customerDetails._id,
                                                                                        "sender_mobileNumber": customerDetails.mobileNumber,
                                                                                        "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                        "sender_UserType": customerDetails.userType,
                                                                                        "receiver_UserType": numberDetails.userType,
                                                                                        "type_transaction": "SEND",
                                                                                        "transactionStatus": "Debited",
                                                                                        "transectionType": "paid"
                                                                                    }
                                                                                    new transactionModel(obj_details).save((errTransaction, transactionDetails) => {
                                                                                        console.log("=============>", errTransaction)
                                                                                        if (errTransaction) {
                                                                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                        }
                                                                                        else {
                                                                                            receiver_details = {
                                                                                                "send_amount": req.body.amount,
                                                                                                "receive_amount": final_amount,
                                                                                                "commission": commissionDetails.client_To_Client,
                                                                                                "amountType": req.body.amountType,
                                                                                                "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                                "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                                "receiver_id": numberDetails._id,
                                                                                                "sender_mobileNumber": customerDetails.mobileNumber,
                                                                                                "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                                "sender_UserType": customerDetails.userType,
                                                                                                "receiver_UserType": numberDetails.userType,
                                                                                                "transactionStatus": "Credited",
                                                                                                "transectionType": "Recieved"
                                                                                            }
                                                                                            new transactionModel(receiver_details).save((errTrans, transDetails) => {
                                                                                                if (errTrans) {
                                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                                }
                                                                                                else {
                                                                                                    response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                                                }
                                                                                            })
                                                                                        }
                                                                                    })

                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                            }

                                                        })
                                                }
                                                else if (numberDetails.userType == "AGENT" && req.body.amountType == "USD") {
                                                    transactionModel.aggregate([
                                                        { $match: { $and: [{ "createdAt": { "$gte": start, "$lt": end } }, { "sender_id": req.userId }, { "sender_UserType": "CUSTOMER" }, { "receiver_UserType": "AGENT" }] } },
                                                        { "$group": { "_id": mongoose.Types.ObjectId(req.userId), "count": { $sum: 1 } } }], async (err1, newResult) => {
                                                            if (err1) {
                                                                response(res, ErrorCode.SOMETHING_WRONG, [err1], ErrorMessage.INTERNAL_ERROR);
                                                            }
                                                            else {
                                                                var final_result = Object.assign({}, ...newResult)
                                                                if (final_result.count >= transactionDetails.clientToAgent_WITHDRAW) {
                                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.LIMIT_NUMBER_OF_TRANSACTION)
                                                                }
                                                                else if (customerDetails.amountUSD < req.body.amount || customerDetails.amountUSD == 0) {
                                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                                }
                                                                else {
                                                                    notification_Status = await notificationModel.findOne({ agent_MobileNumber: numberDetails.mobileNumber, customer_MobileNumber: customerDetails.mobileNumber, status: "approved", notificationType: "Withdraw", transactionStatus: "PENDING", amountType: "USD" })
                                                                    console.log("2222222222222222222222222222222222", notification_Status)
                                                                    if (!notification_Status) {
                                                                        response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.REQUEST_NOT_FOUND);
                                                                    }
                                                                    if (notification_Status) {
                                                                        if (customerDetails.amountUSD < req.body.amount || customerDetails.amountUSD == 0) {
                                                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                                        }
                                                                        if (notification_Status.amount != req.body.amount || notification_Status.amountType != req.body.amountType) {
                                                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.PLEASE_CHECK_YOUR_AMOUNT);
                                                                        }
                                                                        else {
                                                                            passwordCheck = bcrypt.compareSync(req.body.password, customerDetails.password)
                                                                            if (passwordCheck) {
                                                                                var adminCommission = parseFloat(req.body.amount) * parseFloat(commissionDetails.client_To_Agent / 100)// 10
                                                                                console.log("-====================3333333333333333333333333333", adminCommission)
                                                                                var final_amount = parseFloat(req.body.amount) - parseFloat(adminCommission)
                                                                                var customerBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: customerDetails.mobileNumber },
                                                                                    { $set: { amountUSD: parseFloat(customerDetails.amountUSD) - parseFloat(req.body.amount) } }, { new: true })
                                                                                if (customerBalanceUpdate) {
                                                                                    var agentBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: numberDetails.mobileNumber },
                                                                                        { $set: { amountUSD: parseFloat(numberDetails.amountUSD) + parseFloat(final_amount) } }, { new: true })
                                                                                    if (agentBalanceUpdate) {
                                                                                        userModel.findOne({ userType: "ADMIN", accountType: { $ne: "RECOVERY" } }, async (error, adminDetails) => {
                                                                                            if (error) {
                                                                                                response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                            }
                                                                                            else {
                                                                                                var admin_commission_updated = await userModel.findOneAndUpdate({ userType: "ADMIN", accountType: { $ne: "RECOVERY" } }, { $set: { commissionUSD: parseFloat(adminDetails.commissionUSD) + parseFloat(adminCommission) } }, { new: true })
                                                                                                console.log("==================", admin_commission_updated)
                                                                                                if (admin_commission_updated) {
                                                                                                    var notification_update = await notificationModel.findOneAndUpdate({ _id: notification_Status._id }, { $set: { transactionStatus: "COMPLETED" } }, { new: true })
                                                                                                    if (notification_update) {
                                                                                                        var commission_details_obj = {
                                                                                                            "send_amount": req.body.amount,
                                                                                                            "receive_amount": final_amount,
                                                                                                            "commission": commissionDetails.client_To_Agent,
                                                                                                            "amountType": req.body.amountType,
                                                                                                            "sender_UserType": customerDetails.userType,
                                                                                                            "receiver_UserType": numberDetails.userType
                                                                                                        }
                                                                                                        var commission_Details = new commissionModel(commission_details_obj).save()
                                                                                                        if (commission_Details) {
                                                                                                            var obj_details = {
                                                                                                                "agentId": numberDetails.agentId,
                                                                                                                "agent_id": numberDetails._id,
                                                                                                                "send_amount": req.body.amount,
                                                                                                                "receive_amount": final_amount,
                                                                                                                "commission_amount": adminCommission,
                                                                                                                "commission": commissionDetails.client_To_Agent,
                                                                                                                "amountType": req.body.amountType,
                                                                                                                "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                                                "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                                                "sender_id": customerDetails._id,
                                                                                                                //"receiver_id": numberDetails._id,
                                                                                                                "sender_mobileNumber": customerDetails.mobileNumber,
                                                                                                                "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                                                "sender_UserType": customerDetails.userType,
                                                                                                                "receiver_UserType": numberDetails.userType,
                                                                                                                "type_transaction": "WITHDRAW",
                                                                                                                "transactionStatus": "Debited",
                                                                                                                "transectionType": "paid"
                                                                                                            }
                                                                                                            new transactionModel(obj_details).save((errTransaction, transactionDetails) => {
                                                                                                                console.log("=============>", errTransaction)
                                                                                                                if (errTransaction) {
                                                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                                                }
                                                                                                                else {
                                                                                                                    receiver_details = {
                                                                                                                        "agentId": numberDetails.agentId,
                                                                                                                        "agent_id": numberDetails._id,
                                                                                                                        "send_amount": req.body.amount,
                                                                                                                        "receive_amount": final_amount,
                                                                                                                        "commission": commissionDetails.client_To_Agent,
                                                                                                                        "amountType": req.body.amountType,
                                                                                                                        "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                                                        "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                                                        //"sender_id": customerDetails._id,
                                                                                                                        "receiver_id": numberDetails._id,
                                                                                                                        "sender_mobileNumber": customerDetails.mobileNumber,
                                                                                                                        "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                                                        "sender_UserType": customerDetails.userType,
                                                                                                                        "receiver_UserType": numberDetails.userType,
                                                                                                                        "transactionStatus": "Credited",
                                                                                                                        "transectionType": "Recieved"
                                                                                                                    }
                                                                                                                    new transactionModel(receiver_details).save((errTrans, transDetails) => {
                                                                                                                        if (errTrans) {
                                                                                                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                                                        }
                                                                                                                        else {
                                                                                                                            response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                                                                        }
                                                                                                                    })
                                                                                                                }
                                                                                                            })
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        })
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        })
                                                }
                                                else if (numberDetails.userType == "AGENT" && req.body.amountType == "CDF") {
                                                    transactionModel.aggregate([
                                                        { $match: { $and: [{ "createdAt": { "$gte": start, "$lt": end } }, { "sender_id": req.userId }, { "sender_UserType": "CUSTOMER" }, { "receiver_UserType": "AGENT" }] } },
                                                        { "$group": { "_id": mongoose.Types.ObjectId(req.userId), "count": { $sum: 1 } } }], async (err1, newResult) => {
                                                            if (err1) {
                                                                response(res, ErrorCode.SOMETHING_WRONG, [err1], ErrorMessage.INTERNAL_ERROR);
                                                            }
                                                            else {
                                                                var final_result = Object.assign({}, ...newResult)
                                                                if (final_result.count >= transactionDetails.clientToAgent_WITHDRAW) {
                                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.LIMIT_NUMBER_OF_TRANSACTION)
                                                                }
                                                                else if (customerDetails.amountCDF < req.body.amount || customerDetails.amountCDF == 0) {
                                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                                }
                                                                else {
                                                                    notification_Status = await notificationModel.findOne({ agent_MobileNumber: numberDetails.mobileNumber, customer_MobileNumber: customerDetails.mobileNumber, status: "approved", notificationType: "Withdraw", transactionStatus: "PENDING", amountType: "CDF" })
                                                                    console.log("2222222222222222222222222222222222", notification_Status)
                                                                    if (!notification_Status) {
                                                                        response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.REQUEST_NOT_FOUND);
                                                                    }
                                                                    if (notification_Status) {
                                                                        if (customerDetails.amountCDF < req.body.amount || customerDetails.amountUSD == 0) {
                                                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                                        }
                                                                        if (notification_Status.amount != req.body.amount || notification_Status.amountType != req.body.amountType) {
                                                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.PLEASE_CHECK_YOUR_AMOUNT);
                                                                        }
                                                                        else {
                                                                            passwordCheck = bcrypt.compareSync(req.body.password, customerDetails.password)
                                                                            if (passwordCheck) {
                                                                                var adminCommission = parseFloat(req.body.amount) * parseFloat(commissionDetails.client_To_Agent / 100)// 10
                                                                                console.log("-====================3333333333333333333333333333", adminCommission)
                                                                                var final_amount = parseFloat(req.body.amount) - parseFloat(adminCommission)
                                                                                var customerBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: customerDetails.mobileNumber },
                                                                                    { $set: { amountCDF: parseFloat(customerDetails.amountCDF) - parseFloat(req.body.amount) } }, { new: true })
                                                                                if (customerBalanceUpdate) {
                                                                                    var agentBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: numberDetails.mobileNumber },
                                                                                        { $set: { amountCDF: parseFloat(numberDetails.amountCDF) + parseFloat(final_amount) } }, { new: true })
                                                                                    if (agentBalanceUpdate) {
                                                                                        userModel.findOne({ userType: "ADMIN", accountType: { $ne: "RECOVERY" } }, async (error, adminDetails) => {
                                                                                            if (error) {
                                                                                                response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                            }
                                                                                            else {
                                                                                                var admin_commission_updated = await userModel.findOneAndUpdate({ userType: "ADMIN", accountType: { $ne: "RECOVERY" } }, { $set: { commissionCDF: parseFloat(adminDetails.commissionCDF) + parseFloat(adminCommission) } }, { new: true })
                                                                                                console.log("==================", admin_commission_updated)
                                                                                                if (admin_commission_updated) {
                                                                                                    var notification_update = await notificationModel.findOneAndUpdate({ _id: notification_Status._id }, { $set: { transactionStatus: "COMPLETED" } }, { new: true })
                                                                                                    if (notification_update) {
                                                                                                        var commission_details_obj = {
                                                                                                            "send_amount": req.body.amount,
                                                                                                            "receive_amount": final_amount,
                                                                                                            "commission": commissionDetails.client_To_Agent,
                                                                                                            "amountType": req.body.amountType,
                                                                                                            "sender_UserType": customerDetails.userType,
                                                                                                            "receiver_UserType": numberDetails.userType
                                                                                                        }
                                                                                                        var commission_Details = new commissionModel(commission_details_obj).save()
                                                                                                        if (commission_Details) {
                                                                                                            var obj_details = {
                                                                                                                "agentId": numberDetails.agentId,
                                                                                                                "agent_id": numberDetails._id,
                                                                                                                "send_amount": req.body.amount,
                                                                                                                "receive_amount": final_amount,
                                                                                                                "commission": commissionDetails.client_To_Agent,
                                                                                                                "commission_amount": adminCommission,
                                                                                                                "amountType": req.body.amountType,
                                                                                                                "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                                                "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                                                "sender_id": customerDetails._id,
                                                                                                                //"receiver_id": numberDetails._id,
                                                                                                                "sender_mobileNumber": customerDetails.mobileNumber,
                                                                                                                "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                                                "sender_UserType": customerDetails.userType,
                                                                                                                "receiver_UserType": numberDetails.userType,
                                                                                                                "type_transaction": "WITHDRAW",
                                                                                                                "transactionStatus": "Debited",
                                                                                                                "transectionType": "paid"
                                                                                                            }
                                                                                                            new transactionModel(obj_details).save((errTransaction, transactionDetails) => {
                                                                                                                console.log("=============>", errTransaction)
                                                                                                                if (errTransaction) {
                                                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                                                }
                                                                                                                else {
                                                                                                                    receiver_details = {
                                                                                                                        "agentId": numberDetails.agentId,
                                                                                                                        "agent_id": numberDetails._id,
                                                                                                                        "send_amount": req.body.amount,
                                                                                                                        "receive_amount": final_amount,
                                                                                                                        "commission": commissionDetails.client_To_Agent,
                                                                                                                        "amountType": req.body.amountType,
                                                                                                                        "sendMoneyBy": customerDetails.firstName + " " + customerDetails.lastName,
                                                                                                                        "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                                                        //"sender_id": customerDetails._id,
                                                                                                                        "receiver_id": numberDetails._id,
                                                                                                                        "sender_mobileNumber": customerDetails.mobileNumber,
                                                                                                                        "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                                                        "sender_UserType": customerDetails.userType,
                                                                                                                        "receiver_UserType": numberDetails.userType,
                                                                                                                        "transactionStatus": "Credited",
                                                                                                                        "transectionType": "Recieved"
                                                                                                                    }
                                                                                                                    new transactionModel(receiver_details).save((errTrans, transDetails) => {
                                                                                                                        if (errTrans) {
                                                                                                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                                                        }
                                                                                                                        else {
                                                                                                                            response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                                                                        }
                                                                                                                    })
                                                                                                                }
                                                                                                            })
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        })
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        })
                                                }

                                            }
                                        })
                                    }
                                })

                            }

                        })
                }

            })
      
    },


    callingClient: (req, res) => {
        commonFunction.responseMessage(req.headers.language)
      
            userModel.findOne({ _id: req.userId }, (error, result) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                }
                else {
                    var phoneNumberNew = result.countryCode + result.mobileNumber
                    console.log("====================>", phoneNumberNew)
                    commonFunction.callsClient(phoneNumberNew, (err, resultCalls) => {
                        console.log("0000000000000000000000000000", err, resultCalls)
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, resultCalls, SuccessMessage.MESSAGE_SENT)
                        }
                    })
                }
            })
        }
       
}

function convertImage(profilePic) {
    return new Promise((resolve, reject) => {
        commonFunction.uploadImage(profilePic, (error, imageData) => {
            if (error) {
                resolve(error)
            }
            else {
                resolve(imageData)
            }
        })
    })
}




