const userModel = require("../model/userModel");
const bankCardModel = require('../model/BankCardModel')
const moneyModel = require("../model/moneyModel")
const { commonResponse: response } = require('../helper/responseHandler')
const { ErrorMessage } = require('../helper/responseMessege')
const { SuccessMessage } = require('../helper/responseMessege')

const { SuccessCode } = require('../helper/responseCode')
const { ErrorCode } = require('../helper/responseCode')
const bcrypt = require("bcrypt-nodejs");
const commonFunction = require('../helper/commonFunction')
const jwt = require('jsonwebtoken');
var auth = require('.././middleWare/auth');
var valid = require("card-validator");
const { resolve } = require("swagger-parser");

module.exports = {
    setMoney: (req, res) => {
      
        commonFunction.jwtDecode(req.headers.token, (error, result) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                req.body.forEach(item => {
                    new moneyModel(item).save((error, savedData) => {
                        if (error) {
                            console.log(error, savedData)
                        }
                        else {
                            console.log(error, savedData)
                        }
                    })
                })
                response(res, SuccessCode.SUCCESS, [], SuccessMessage.DATA_SAVED)

            }
        })
    },

    addBankCard: async (req, res) => {

        let body = req.body;
        // let result = await isValidCardNumber({test: "Test"});
        // if(result === 'valid')
        //     resolve(res, SuccessCode.SUCCESS, "Valid", SuccessMessage.DATA_SAVED);
        // else resolve(res, ErrorCode.INTERNAL_ERROR, "Valid", ErrorMessage.INTERNAL_ERROR);
        return new Promise((resolve, reject) => {
            
        });      
    },

    rechargeFromBankCard:  (req,res) => {
        let body = req.body;
        let tag = "[rechargeFromBankCard]"
        bankCardModel.findOne({cardNumber : body.cardNumber}, async (error, cardData) => {
            if(cardData){
                userModel.findOne({_id : body.userId}, (error, userData) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                            }
    
                            else if(!userData)
                                response(res, ErrorCode.NOT_FOUND, [error], ErrorMessage.USER_NOT_FOUND);
    
                            else if (userData.kycStatus == "unverified") {
                                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.PLEASE_VERIFY_KYC_DETAILS);
                            } else {
                                if(body.amountType === 'USD'){
                                    let newBalance = parseFloat(userData.amountUSD) + parseFloat(body.amount);
                                    userModel.findOneAndUpdate({_id: body.userId}, {$set: {amountUSD:newBalance }}, (error, updatedUserData) => {
                                        if(error || !updatedUserData)
                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                        else response(res, SuccessCode.SUCCESS, {}, SuccessMessage.TRANSACTION_COMPLETED); 
                                    });

                                } else {
                                    let newBalance = parseFloat(userData.amountCDF) + parseFloat(body.amount);
                                    userModel.findOneAndUpdate({_id: body.userId}, {$set: {amountCDF:newBalance }}, (error, updatedUserData) => {
                                        if(error || !updatedUserData)
                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                        else response(res, SuccessCode.SUCCESS, {}, SuccessMessage.TRANSACTION_COMPLETED); 
                                    });
                                }
                            }
                        });
            } else { 
                new bankCardModel(req.body).save((error, data) => {
                    if(error || !data)
                        resolve(res, ErrorCode.INTERNAL_ERROR, {}, ErrorMessage.INTERNAL_ERROR);
                    else {
                        userModel.findOne({_id : body.userId}, (error, userData) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                            }
    
                            else if(!userData)
                                response(res, ErrorCode.NOT_FOUND, [error], ErrorMessage.USER_NOT_FOUND);
    
                            else if (userData.kycStatus == "unverified") {
                                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.PLEASE_VERIFY_KYC_DETAILS);
                            } else {
                                if(body.amountType === 'USD'){
                                    let newBalance = parseFloat(userData.amountUSD) + parseFloat(body.amount);
                                    userModel.findOneAndUpdate({_id: body.userId}, {$set: {amountUSD:newBalance }}, (error, updatedUserData) => {
                                        if(error || !updatedUserData)
                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                        else response(res, SuccessCode.SUCCESS, {}, SuccessMessage.TRANSACTION_COMPLETED); 
                                    });

                                } else {
                                    let newBalance = parseFloat(userData.amountCDF) + parseFloat(body.amount);
                                    userModel.findOneAndUpdate({_id: body.userId}, {$set: {amountCDF:newBalance }}, (error, updatedUserData) => {
                                        if(error || !updatedUserData)
                                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                        else response(res, SuccessCode.SUCCESS, {}, SuccessMessage.TRANSACTION_COMPLETED); 
                                    });
                                }
                            }
                        });
                    }
                });
            }
        });
    },

    getBankCards :(req,res) => {
        bankCardModel.find({userId: req.body.userId, cardType: req.body.cardType}, (error,data) => {
            if(error)
                response(res, ErrorCode.INTERNAL_ERROR, {}, ErrorMessage.INTERNAL_ERROR);
            else response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);

        });
    },

    topupMobileFromWallet: (req,res) => {
        let data = req.body;
        let tag  = "[topupMobileFromWallet]: ";
        
        if(!data.mobileNumber)
            response(res, ErrorCode.SOMETHING_WRONG, [], "Mobile number is required (mobileNumber)");
        
            else {
                    userModel.findOne({ _id: data.userId, status: "ACTIVE"}, (error, userDetail) => {
                        console.log(tag + "getting user", error, userDetail)
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                        }

                        else if(!userDetail)
                            response(res, ErrorCode.NOT_FOUND, [error], ErrorMessage.USER_NOT_FOUND);

                        else if (userDetail.kycStatus == "unverified") {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.PLEASE_VERIFY_KYC_DETAILS);
                        }
                        else {
                            commissionModel.findOne({ status: "ACTIVE" }, async (errComm, commissionDetails) => {
                                if (errComm) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                  //  var commission = parseFloat(data.amount) * parseFloat(commissionDetails.billPayment_commision / 100)// 10
                                   // let totalAmount = parseFloat(data.amount) + commission;
                                   let commission = 0;
                                   let totalAmount = parseFloat(data.amount);
                                        if(userDetail.amountCDF < totalAmount)
                                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                        else {
                                            let passwordCheck = bcrypt.compareSync(data.password, userDetail.password);
                                            if(passwordCheck) {
                                                let newUserBalance = parseFloat(userDetail.amountCDF) - parseFloat(totalAmount);
                                               let userBalanceUpdate = await userModel.findOneAndUpdate({ _id: data.userId },
                                                { $set: { amountCDF: newUserBalance } }, {new : true})
                                                
                                                if(userBalanceUpdate) {
                                                   merchantModel.findOne({ _id: data.merchantId, status: "ACTIVE"}, async (error, merchantDetail) => {
                                                       if(merchantDetail) {
                                                        var merchantBalanceUpdate = await merchantModel.findOneAndUpdate({ _id: data.merchantId },
                                                            { $set: { balanceSDG: parseFloat(merchantDetail.balanceSDG) + parseFloat(data.amount) } }, { new: true })
                                                           // End of find marchant

                                                           if(merchantBalanceUpdate) {
                                                               let obj_details = {
                                                                "merchantId": merchantDetail._id,
                                                                "merchant_id": merchantDetail._id,
                                                                "send_amount": totalAmount,
                                                                "receive_amount": totalAmount,
                                                                "commission": commission,
                                                                "commission_amount": commission,
                                                                "amountType": data.amountType,
                                                                "sendMoneyBy": userDetail.firstName + " " + userDetail.lastName,
                                                                "receiveMoneyBy": merchantDetail.companyName,
                                                                "sender_id": userDetail._id,
                                                                "sender_mobileNumber": userDetail.mobileNumber,
                                                                "receiver_mobileNumber": merchantDetail.contactNumber,
                                                                "sender_UserType": userDetail.userType,
                                                                "receiver_UserType": "MERCHANT",
                                                                "type_transaction": "MOBILE_TOPUP",
                                                                "transactionStatus": "Debited",
                                                                "transectionType": "paid"
                                                            }
                                                            new transactionModel(obj_details).save((errSbmit, transDetails) => {
                                                                if (errSbmit) {
                                                                    response(res, ErrorCode.SOMETHING_WRONG, [errSbmit], ErrorMessage.INTERNAL_ERROR);
                                                                } else {
                                                                    response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                }
                                                                
                                                            });

                                                           } else response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);

                                                       } else response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                   });
                                                } else response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                            }  
                                            else {
                                                response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                            }
                                        }
                                }
                            });
                            
                        }
                   })                  
        }
    },
}

function isValidCardNumber(req) {
    console.log("isValidCardNumber", req)
    return new Promise((resolve, reject) => {
        var numberValidation = valid.number("4111");
        if (!numberValidation.isPotentiallyValid) {
            resolve({valid:'Invalid'});
        }
            
        if (numberValidation.card) {
            console.log(numberValidation.card.type); // 'visa'
            resolve({valid:'Valid'});
        }
    });
}