const merchantModel = require("../model/merchantModel");
const { commonResponse: response } = require('../helper/responseHandler')
const { ErrorMessage } = require('../helper/responseMessege')
const { SuccessMessage } = require('../helper/responseMessege')

const { SuccessCode } = require('../helper/responseCode')
const { ErrorCode } = require('../helper/responseCode')
const bcrypt = require("bcrypt-nodejs");
const commonFunction = require('../helper/commonFunction')
const jwt = require('jsonwebtoken');
var auth = require('.././middleWare/auth');
const MobileMoneyController = require("./MobileMoneyController");

module.exports = {
    /**
      * Function Name : authenticate
      * Description   : authorization for Mobile Money
      *
      * @return response  
      */
     addMerchant: (req, res) => {

        if(!req.body.companyImage) {
            response(res, ErrorCode.SOMETHING_WRONG, [], "Company Image is required");
        }

        else if(!req.body.companyImage.includes("http")) 
        {
            commonFunction.uploadImage(req.body.companyImage, (error, result) => {
                if (error) {
                     console.log("Error while Uploading Image", error);
                     response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                } else {
                     console.log("Image uploaded :)");
                     req.body.companyImage = result;

                     new merchantModel(req.body).save((err, success) => {
                        console.log("merchantModel like ",err,success)
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, success, SuccessMessage.DATA_SAVED)
                        }
                    });
                }
            });
        } 
        else {
            new merchantModel(req.body).save((err, success) => {
                console.log("merchantModel like ",err,success)
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                }
                else {
                    response(res, SuccessCode.SUCCESS, success, SuccessMessage.DATA_SAVED)
                }
        });
    }
},
getMerchants: async (req, res) => {
    try {
        if(req.body.id) { 
          //  var merchants = await MobileMoneyController.getbillcompanies();
            merchantModel.find({categoryId: req.body.id, status: "ACTIVE" }, (error, data) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    // if(merchants && merchants.length > 0) {
                    //     for(let m of merchants) {
                    //         data.push({
                    //             companyName: m.companyName
                    //         });
                    //     }
                    // }
                    
                    response(res, SuccessCode.SUCCESS, data ? data : [], SuccessMessage.DATA_FOUND);
                }
            })
        } else {
            merchantModel.find({ status: { $ne: "DELETE" } }, (error, data) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    response(res, SuccessCode.SUCCESS, data ? data : [], SuccessMessage.DATA_FOUND);
                }
            })
        }
        
    }
    catch (error) {
        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
    }
},

getMerchant: (req, res) => {
    try {
        merchantModel.find({_id: req.body.id, status: { $ne: "DELETE" } }, (error, data) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else if(!data.length)
            response(res, ErrorCode.NOT_FOUND,null, ErrorMessage.USER_NOT_FOUND);
            else {
                response(res, SuccessCode.SUCCESS, data ? data : [], SuccessMessage.DATA_FOUND);
            }
        })
    }
    catch (error) {
        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
    }
},

updateMerchant: (req,res) => {
    merchantModel.findOne({ _id: req.body._id } , async (error, result) => {
        console.log("updateMerchant", error, result)
        if (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
        }
        else if (!result) {
            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
        }
        else {

            obj = {}
            if (req.body.status) {
                obj.status = req.body.status
            }
            if (req.body.merchantName) {
                obj.merchantName = req.body.merchantName
            }
            if (req.body.contactNumber) {
                obj.contactNumber = req.body.contactNumber
            }
            if (req.body.companyName) {
                obj.companyName = req.body.companyName
            }
            if (req.body.officeAddress) {
                obj.officeAddress = req.body.officeAddress
            }
            if (req.body.officeContactNumber) {
                obj.officeContactNumber = req.body.officeContactNumber
            }
            if (req.body.accountNumber) {
                obj.accountNumber = req.body.accountNumber
            }
            if (req.body.categoryId) {
                obj.categoryId = req.body.categoryId
            }
            if (req.body.currencyDealing) {
                obj.currencyDealing = req.body.currencyDealing
            }
            if (req.body.nationality) {
                obj.nationality = req.body.nationality
            }
            if (req.body.homeAddress) {
                obj.homeAddress = req.body.homeAddress
            }
            if (req.body.categoryName) {
                obj.categoryName = req.body.categoryName
            }
            if (req.body.emailId) {
                obj.emailId = req.body.emailId
            }
            if (req.body.countryCode) {
                obj.countryCode = req.body.countryCode
            }
            if (req.body.companyImage && !req.body.companyImage.includes("https")) {
                obj.companyImage = await convertImage(req.body.companyImage)
            }
            
            merchantModel.findOneAndUpdate({ _id: result._id }, { $set: obj }, { new: true }, (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (!userData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
                } else {
                    response(res, SuccessCode.SUCCESS, null, SuccessMessage.UPDATE_SUCCESS);
                }
            })
        }
    })

}
}

function convertImage(image) {
    return new Promise((resolve, reject) => {
        commonFunction.uploadImage(image, (error, imageData) => {
            if (error) {
                resolve(error)
            }
            else {
                resolve(imageData)
            }
        })
    })
}