const userModel = require("../model/userModel");
const questionModel = require("../model/securityQuestionModel");
const notificationModel = require("../model/notificationModel")
const moneyModel = require("../model/moneyModel")
const kycModel = require("../model/kycModel")
const qrCodeModel = require("../model/qrCodeModel")
const commissionModel = require("../model/commissionModel")
const advModel = require("../model/advertismentModel")
const postModel = require("../model/postModel")
var transactionModel = require("../model/transactionModel")
const exchangeModel = require("../model/exchangeModel")
const newcommissionModel = require("../model/newCommissionModel")
const messageModel = require("../model/messageModel")
//const stripe = require('stripe')('sk_test_i0zrTmLCXuczLIe6kWH0wUFK00JIHdQp1A');
const stripe = require('stripe')('sk_test_L8oA9O5IOgtmflzWMndmmEhR');
const mongoose = require('mongoose')

const { commonResponse: response } = require('../helper/responseHandler')
const { ErrorMessage } = require('../helper/responseMessege')
const { SuccessMessage } = require('../helper/responseMessege')

const { SuccessCode } = require('../helper/responseCode')
const { ErrorCode } = require('../helper/responseCode')
const bcrypt = require("bcrypt-nodejs");
const commonFunction = require('../helper/commonFunction')
const jwt = require('jsonwebtoken');
var auth = require('.././middleWare/auth');
const billCategoryModel = require("../model/billCategoryModel");

var subAdmin, receiver_details
var start = new Date();
start.setHours(0, 0, 0, 0);
var end = new Date();
end.setHours(23, 59, 59, 999);
module.exports = {
    /**
       * Function Name :login  
       * Description   : login by admin
       * 
       * @return response
       */
    login: (req, res) => {
        userModel.findOne({ "mobileNumber": req.body.mobileNumber, userType: { $in: ["ADMIN", "SUBADMIN"] } }, (error, adminData) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
            }
            else if (!adminData) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND)
            }
            else {
                if (req.body.password == req.body.confirmPassword) {
                    const check = bcrypt.compareSync(req.body.password, adminData.password)
                    if (check) {
                        var token = jwt.sign({ id: adminData._id, iat: Math.floor(Date.now() / 1000) - 30 }, 'WALLET-APP');
                        var result1 = {
                            token: token,
                            mobileNumber: req.body.mobileNumber,
                            userType: adminData.userType,
                            permissions: adminData.permissions
                        }
                        response(res, SuccessCode.SUCCESS, result1, SuccessMessage.LOGIN_SUCCESS)
                    }
                    else {
                        response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                    }
                }
                else {
                    response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.NOT_MATCH)
                }
            }
        })
    },
    /**
      * Function Name :viewProfile
      * Description   : view profile details of admin
      *
      * @return response     
    */
    viewProfile: (req, res) => {
        try {
            commonFunction.jwtDecode(req.headers.token, (err, result) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                }
                else {
                    userModel.findOne({ _id: result,userType: { $in: ["ADMIN", "SUBADMIN"] } }, (err2, adminData) => {
                        if (err2) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                        }
                        else if (!adminData) {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND)
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, adminData, SuccessMessage.DETAIL_GET);

                        }
                    })
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)

        }

    },

    // /**
    //     * Function Name :editProfile
    //     * Description   : edit profile of admin
    //     *
    //     * @return response
    //     */

    // editProfile: (req, res) => {
    //     try {
    //         if (req.body.profilePic) {
    //             commonFunction.uploadImage(req.body.profilePic, (error, imageData) => {
    //                 if (error) {
    //                     response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
    //                 }
    //                 else {
    //                     req.body.profilePic = imageData
    //                     userModel.findOneAndUpdate({ _id: req.body.userId, status: "ACTIVE", userType: { $in: ["ADMIN", "SUBADMIN"] } }, { $set: req.body }, { new: true }, (error, adminData) => {
    //                         if (error) {
    //                             response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
    //                         } else if (!adminData) {
    //                             response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
    //                         } else {
    //                             response(res, SuccessCode.SUCCESS, adminData, SuccessMessage.PROFILE_DETAILS);
    //                         }
    //                     })
    //                 }
    //             })
    //         }
    //         else {
    //             userModel.findOneAndUpdate({ _id: req.body.userId, status: "ACTIVE", userType: { $in: ["ADMIN", "SUBADMIN"] } }, { $set: req.body }, { new: true }, (error, adminData) => {
    //                 if (error) {
    //                     response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
    //                 }
    //                 else if (!adminData) {
    //                     response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
    //                 }
    //                 else {
    //                     response(res, SuccessCode.SUCCESS, adminData, SuccessMessage.PROFILE_DETAILS);
    //                 }
    //             })
    //         }
    //     }
    //     catch (error) {
    //         response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
    //     }
    // },



      /**
     * Function Name :editProfile
     * Description   : editProfile for admin
     *
     * @return response
    */

   editProfile: (req, res) => {
    try {
        userModel.findOne({ _id: req.userId, status: "ACTIVE" }, (err, adminResult) => {
            if (err) {
                response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!adminResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            else {
                let query = {   mobileNumber: req.body.mobileNumber,  status: { $ne: "DELETE" } , _id: { $ne: adminResult._id }  }
                userModel.findOne(query, (err, result) => {
                    if (err) {
                        response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (result) {
                        if (result.mobileNumber == req.body.mobileNumber) {
                            response(res, ErrorCode.ALREADY_EXIST, [], ErrorMessage.MOBILE_EXIST);
                        }
                       
                    }
                    else {
                        if (req.body.oldPassword && !req.body.profilePic) {
                            var check = bcrypt.compareSync(req.body.oldPassword, adminResult.password);
                            if (!check) {
                                response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.OLD_PASSWORD);
                            }
                            else {
                                req.body.password = bcrypt.hashSync(req.body.newPassword);
                                userModel.findByIdAndUpdate({ _id: adminResult._id }, { $set: req.body }, { new: true }, (err, updateResult) => {
                                    if (err) {
                                        response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                                    }
                                    else {
                                        response(res, SuccessCode.SUCCESS, updateResult, SuccessMessage.PROFILE_DETAILS);
                                    }
                                })
                            }
                        }
                        else if (!req.body.oldPassword && req.body.profilePic) {
                            commonFunction.uploadImage(req.body.profilePic, (err, imageResult) => {
                                if (err) {
                                    response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                    req.body.profilePic = imageResult;
                                    userModel.findByIdAndUpdate({ _id: adminResult._id }, { $set: req.body }, { new: true }, (err, updateResult) => {
                                        if (err) {
                                            response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                                        }
                                        else {
                                            response(res, SuccessCode.SUCCESS, updateResult, SuccessMessage.PROFILE_DETAILS);
                                        }
                                    })
                                }
                            })
                        }
                        else if (req.body.oldPassword && req.body.profilePic) {
                            commonFunction.uploadImage(req.body.profilePic, (err, imageResult) => {
                                if (err) {
                                    response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                    var check = bcrypt.compareSync(req.body.oldPassword, adminResult.password);
                                    if (!check) {
                                        response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.OLD_PASSWORD);
                                    }
                                    else {
                                        req.body.profilePic = imageResult;
                                        req.body.password = bcrypt.hashSync(req.body.newPassword);
                                        userModel.findByIdAndUpdate({ _id: adminResult._id }, { $set: req.body }, { new: true }, (err, updateResult) => {
                                            if (err) {
                                                response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                                            }
                                            else {
                                                response(res, SuccessCode.SUCCESS, updateResult, SuccessMessage.PROFILE_DETAILS);
                                            }
                                        })
                                    }
                                }
                            })
                        }
                        else {
                            userModel.findByIdAndUpdate({ _id: adminResult._id }, { $set: req.body }, { new: true }, (err, updateResult) => {
                                if (err) {
                                    response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                    response(res, SuccessCode.SUCCESS, updateResult, SuccessMessage.PROFILE_DETAILS);
                                }
                            })
                        }
                    }
                })
            }
        })
    }
    catch (error) {
        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
    }
},

    /**
             * Function Name :otpSent
             * Description   : otp sent by admin to mobile number of admin
             *
             * @return response
           */
    otpSent: (req, res) => {
        try {
            userModel.findOne({ "mobileNumber": req.body.mobileNumber, status: "ACTIVE", userType: { $in: ["ADMIN", "SUBADMIN"] } }, (error, result) => {

                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                }
                else if (!result) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND)
                }
                else {
                    var otp = commonFunction.getOTP(4)
                    var phoneNumber = result.countryCode + result.mobileNumber
                    commonFunction.sendSMS(phoneNumber, `Your OTP is :- ${otp}`, (error, otpSent) => {
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                        }
                        else {
                            userModel.findOneAndUpdate({ _id: result._id }, { $set: { otp: otp, otpTime: Date.now() } }, { new: true }, (err, otpUpdate) => {
                                if (err) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                                }
                                else if (!otpUpdate) {
                                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.MOBILE_NOT_FOUND);
                                }
                                else {
                                    response(res, SuccessCode.OTP_SEND, otpUpdate, SuccessMessage.OTP_SEND)
                                }
                            })
                        }
                    })

                }
            })

        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
        }
    },




    /**
         * Function Name :verifyOtp
         * Description   : otp verify by user
         *
         * @return response
       */

    verifyOtp: (req, res) => {
        userModel.findOne({ mobileNumber: req.body.mobileNumber, status: "ACTIVE" }, (err, result) => {
            if (err) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!result) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.MOBILE_NOT_FOUND);
            }
            else {
                if (result.otp == req.body.otp || req.body.otp == 1234) {
                    var newTime = Date.now()
                    var difference = newTime - result.otpTime
                    
                    // if (difference < 60000) {
                    userModel.findByIdAndUpdate(result._id, { verifyOtp: true }, { new: true }, (updateErr, updateResult) => {
                        if (updateErr) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, updateResult, SuccessMessage.VERIFY_OTP);
                        }
                    })
                    // }
                    // else {
                    //     response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.OTP_EXPIRED);

                    // }

                }
                else {
                    response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.INVALID_OTP);
                }
            }
        })
    },

    addSecurityQuestion: (req, res) => {
        new questionModel(req.body).save((err, success) => {
            console.log("question like ",err,success)
            if (err) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
            }
            else {
                response(res, SuccessCode.SUCCESS, success, SuccessMessage.DATA_SAVED)
            }
        })
    },

    verifyAnswer: (req, res) => {
        userModel.findOne({ "_id": req.userId, userType: { $in: ["ADMIN", "SUBADMIN"] } }, (error, data) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
            }
            else if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            }
            else {
                if (data.answer == req.body.answer) {
                    response(res, SuccessCode.SUCCESS, [], SuccessMessage.LOGIN_SUCCESS)
                }
                else {
                    response(res, ErrorCode.NOT_MATCH, [], ErrorMessage.ANSWER_NOT_MATCH)
                }
            }
        })

    },
    changePassword: (req, res) => {
        userModel.findOne({ _id: req.userId, status: "ACTIVE" }, (error, data) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
            }
            else if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            }
            else {
                var comp = bcrypt.compareSync(req.body.oldPassword, data.password)
                if (comp == true) {
                    if (req.body.newPassword == req.body.confirmPassword) {
                        var newPassword = bcrypt.hashSync(req.body.newPassword)
                        userModel.findOneAndUpdate({ _id: data._id }, { $set: { password: newPassword } }, { new: true }, (error, updatePassword) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                            }
                            else {
                                response(res, SuccessCode.SUCCESS, updatePassword, SuccessMessage.PASSWORD_UPDATE)
                            }
                        })
                    }
                    else {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.WRONG_PASSWORD)
                    }
                }
                else {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.OLD_PASSWORD);
                }
            }
        })
    },

    forgotPassword: (req, res) => {

        userModel.findOne({ "mobileNumber": req.body.mobileNumber, status: "ACTIVE", userType: { $in: ["ADMIN", "SUBADMIN"] } }, (error, result) => {

            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
            }
            else if (!result) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND)
            }
            else {
                var otp = commonFunction.getOTP(4)
                var phoneNumber = result.countryCode + result.mobileNumber
                commonFunction.sendSMS(phoneNumber, `Your OTP is :- ${otp}`, (error, otpSent) => {
                    console.log("gredsfaef", error, otpSent)
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                    }
                    else {
                        userModel.findOneAndUpdate({ _id: result._id }, { $set: { otp: otp } }, { new: true }, (err, otpUpdate) => {
                            if (err) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                            }
                            else {
                                response(res, SuccessCode.OTP_SEND, otpUpdate, SuccessMessage.OTP_SEND)
                            }
                        })
                    }
                })

            }
        })

    },

    resetPassword: (req, res) => {
        userModel.findOne({ _id: req.body.adminId, userType: { $in: ["ADMIN", "SUBADMIN"] } }, (error, adminData) => {
            console.log("i ma herhe", adminData)
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
            }
            else if (!adminData) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND)
            }
            else {
                if (req.body.newPassword == req.body.confirmPassword) {
                    var newPassword = bcrypt.hashSync(req.body.newPassword)
                    userModel.findOneAndUpdate({ _id: adminData._id }, { $set: { password: newPassword } }, { new: true }, (error, updatePassword) => {
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, updatePassword, SuccessMessage.PASSWORD_UPDATE)
                        }
                    })
                }
                else {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.WRONG_PASSWORD)
                }

            }
        })
    },

    /**
    * Function Name :add sub admin
    * Description : sub-admin adding by admin
    *
    * @return response
    */

    addSubAdmin: (req, res) => {
        try {
            userModel.findOne({ 'mobileNumber': req.body.mobileNumber, status: { $ne: "DELETE" } }, (error, result2) => {
                
                if (error) {

                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                }
                else if (result2) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.MOBILE_EXIST)
                }
                else {
                    if (req.body.profilePic) {
                        commonFunction.uploadImage(req.body.profilePic, (error, profilePic) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                            }
                            else {
                                commonFunction.uploadImage(req.body.kycImage, (err, imageResult) => {

                                    if (err) {

                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                                    }

                                    else {
                                        req.body.fullName=req.body.firstName+" "+req.body.lastName
                                        var pass = req.body.password;
                                        req.body.password = bcrypt.hashSync(req.body.password);
                                        req.body.userType = "SUBADMIN";
                                        req.body.profilePic = profilePic
                                        req.body.kycImage = imageResult
                                        req.body.kycStatus = "verified"
                                        //req.body.password = bcrypt.hashSync(req.body.password)
                                        req.body.permissions = [{
                                            dashboard: req.body.dashboard,
                                            userManagement: req.body.userManagement,
                                            subAdminManagement: req.body.subAdminManagement,
                                            moneyManagement: req.body.moneyManagement,
                                            commissionManagement: req.body.commissionManagement,
                                            transactionManagement: req.body.transactionManagement,
                                            chatManagement: req.body.chatManagement,
                                            staticContentManagement: req.body.staticContentManagement,
                                            journalManagement: req.body.journalManagement,
                                            agentTransactionManagement: req.body.agentTransactionManagement,
                                            kycManagement: req.body.kycManagement
                                        }]

                                        commonFunction.emailSend(req.body.emailId, `Dear ${req.body.firstName}, Congratulations your account has been created as a Sub-
                                    Admin.<br> Your email and password are:-<br> Email: ${req.body.emailId}<br> Password: ${pass}`, (err, emailResult) => {

                                            console.log("Sub admin email send result", err, emailResult)
                                            if (err) {

                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                            }
                                            else {
                                                new userModel(req.body).save((saveErr, saveResult) => {
                                                  
                                                    if (saveErr) {
                                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                    }
                                                    else {
                                                        response(res, SuccessCode.SUCCESS, saveResult, SuccessMessage.SUB_ADMIN);
                                                    }
                                                })
                                            }
                                        })

                                    }
                                })

                            }
                        })
                    }
                    else {
                        commonFunction.uploadImage(req.body.kycImage, (err, imageResult) => {
                         
                            if (err) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                            }
                            else {
                                req.body.fullName=req.body.firstName+" "+req.body.lastName
                                req.body.userType = "SUBADMIN"
                                req.body.kycImage = imageResult
                                req.body.kycStatus = "verified"
                                var pass = req.body.password
                                req.body.password = bcrypt.hashSync(req.body.password)
                                req.body.permissions = [{
                                    dashboard: req.body.dashboard,
                                    userManagement: req.body.userManagement,
                                    subAdminManagement: req.body.subAdminManagement,
                                    moneyManagement: req.body.moneyManagement,
                                    commissionManagement: req.body.commissionManagement,
                                    transactionManagement: req.body.transactionManagement,
                                    chatManagement: req.body.chatManagement,
                                    staticContentManagement: req.body.staticContentManagement,
                                    journalManagement: req.body.journalManagement,
                                    agentTransactionManagement: req.body.agentTransactionManagement,
                                    kycManagement: req.body.kycManagement
                                }]
                                commonFunction.emailSend(req.body.emailId, `Dear ${req.body.firstName}, Congratulations your account has been created as a Sub-
                                 Admin.<br> Your email and password are:-<br> Email: ${req.body.emailId}<br> Password: ${pass}`, (err, emailResult) => {
                                    console.log("gredsfaef593", err, emailResult)
                                    if (err) {

                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                    }
                                    else {
                                        new userModel(req.body).save((saveErr, saveResult) => {

                                            if (saveErr) {
                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                            }
                                            else {
                                                response(res, SuccessCode.SUCCESS, saveResult, SuccessMessage.SUB_ADMIN);
                                            }
                                        })
                                    }
                                })

                            }
                        })
                    }

                }
            })

        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
               * Function Name :edit profile Sub-admin
               * Description   : change in sub-admin details
               *
               * @return response
               */
    editSubAdmin: (req, res) => {
        try {
            userModel.findOne({ "_id": req.body._id, userType: "SUBADMIN", status: "ACTIVE" }, (error, subData) => {
                console.log("gredsfaef633", error, subData)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                }
                else if (!subData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                }
                else {
                    if (req.body.kycImage && !req.body.profilePic) {
                        commonFunction.uploadImage(req.body.kycImage, (error, imageResult) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                            }
                            else {
                                subAdmin = {};
                                subAdmin.kycImage = imageResult

                                if (req.body.firstName) {
                                    subAdmin.firstName = req.body.firstName
                                }
                                if (req.body.lastName) {
                                    subAdmin.lastName = req.body.lastName
                                }
                                if (req.body.password) {
                                    subAdmin.password = bcrypt.hashSync(req.body.password);
                                }
                                if (req.body.mobileNumber) {
                                    subAdmin.mobileNumber = req.body.mobileNumber
                                }
                                if (req.body.permissionId) {
                                    subAdmin.permissions = [{
                                        _id: req.body.permissionId,
                                        dashboard: req.body.dashboard,
                                        userManagement: req.body.userManagement,
                                        subAdminManagement: req.body.subAdminManagement,
                                        agentManagement: req.body.agentManagement,
                                        moneyManagement: req.body.moneyManagement,
                                        commissionManagement: req.body.commissionManagement,
                                        transactionManagement: req.body.transactionManagement,
                                        chatManagement: req.body.chatManagement,
                                        staticContentManagement: req.body.staticContentManagement,
                                        journalManagement: req.body.journalManagement,
                                        agentTransactionManagement: req.body.agentTransactionManagement,
                                        kycManagement: req.body.kycManagement
                                    }]
                                }

                        subAdmin.fullName=req.body.firstName+" "+req.body.lastName
                                userModel.findOneAndUpdate({ "_id": subData._id }, { $set: subAdmin }, { new: true }, (error, updateData) => {
                                    console.log("gredsfaef633", error, updateData)
                                    if (error) {
                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                    }
                                    else {
                                        response(res, SuccessCode.SUCCESS, updateData, SuccessMessage.UPDATE_SUCCESS);
                                    }
                                })
                            }
                        })
                    }
                    else if (!req.body.kycImage && req.body.profilePic) {
                        commonFunction.uploadImage(req.body.profilePic, (error, imageResult) => {
                            console.log("gredsfaef633", error, imageResult)
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                            }
                            else {
                                subAdmin = {};
                                subAdmin.profilePic = imageResult

                                if (req.body.firstName) {
                                    subAdmin.firstName = req.body.firstName
                                }
                                if (req.body.lastName) {
                                    subAdmin.lastName = req.body.lastName
                                }
                                if (req.body.password) {
                                    subAdmin.password = bcrypt.hashSync(req.body.password);
                                }
                                if (req.body.mobileNumber) {
                                    subAdmin.mobileNumber = req.body.mobileNumber
                                }
                                if (req.body.permissionId) {
                                    subAdmin.permissions = [{
                                        _id: req.body.permissionId,
                                        dashboard: req.body.dashboard,
                                        userManagement: req.body.userManagement,
                                        subAdminManagement: req.body.subAdminManagement,
                                        agentManagement: req.body.agentManagement,
                                        moneyManagement: req.body.moneyManagement,
                                        commissionManagement: req.body.commissionManagement,
                                        transactionManagement: req.body.transactionManagement,
                                        chatManagement: req.body.chatManagement,
                                        staticContentManagement: req.body.staticContentManagement,
                                        journalManagement: req.body.journalManagement,
                                        agentTransactionManagement: req.body.agentTransactionManagement,
                                        kycManagement: req.body.kycManagement
                                    }]
                                }
                                subAdmin.fullName=req.body.firstName+" "+req.body.lastName
                                userModel.findOneAndUpdate({ "_id": subData._id }, { $set: subAdmin }, { new: true }, (error, updateData) => {
                                    if (error) {
                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                    }
                                    else {
                                        response(res, SuccessCode.SUCCESS, updateData, SuccessMessage.UPDATE_SUCCESS);
                                    }
                                })
                            }
                        })
                    }
                    else if (req.body.kycImage && req.body.profilePic) {
                        
                        commonFunction.uploadImage(req.body.kycImage, (error, kycData) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                            }
                            else {
                                commonFunction.uploadImage(req.body.profilePic, (error, imageResult) => {
                                    if (error) {
                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                                    }
                                    else {
                                        subAdmin = {};
                                        subAdmin.profilePic = imageResult
                                        subAdmin.kycImage = kycData

                                        if (req.body.firstName) {
                                            subAdmin.firstName = req.body.firstName
                                        }
                                        if (req.body.lastName) {
                                            subAdmin.lastName = req.body.lastName
                                        }
                                        if (req.body.password) {
                                            subAdmin.password = bcrypt.hashSync(req.body.password);
                                        }
                                        if (req.body.mobileNumber) {
                                            subAdmin.mobileNumber = req.body.mobileNumber
                                        }
                                        if (req.body.permissionId) {
                                            subAdmin.permissions = [{
                                                _id: req.body.permissionId,
                                                dashboard: req.body.dashboard,
                                                userManagement: req.body.userManagement,
                                                subAdminManagement: req.body.subAdminManagement,
                                                agentManagement: req.body.agentManagement,
                                                moneyManagement: req.body.moneyManagement,
                                                commissionManagement: req.body.commissionManagement,
                                                transactionManagement: req.body.transactionManagement,
                                                chatManagement: req.body.chatManagement,
                                                staticContentManagement: req.body.staticContentManagement,
                                                journalManagement: req.body.journalManagement,
                                                agentTransactionManagement: req.body.agentTransactionManagement,
                                                kycManagement: req.body.kycManagement
                                            }]
                                        }
                                        subAdmin.fullName=req.body.firstName+" "+req.body.lastName
                                        userModel.findOneAndUpdate({ "_id": subData._id }, { $set: subAdmin }, { new: true }, (error, updateData) => {
                                            console.log("gredsfaef633", error, updateData)
                                            if (error) {
                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                            }
                                            else {
                                                response(res, SuccessCode.SUCCESS, updateData, SuccessMessage.UPDATE_SUCCESS);
                                            }
                                        })
                                    }
                                })
                            }
                        })

                    }

                    else {
                        subAdmin = {};
                        if (req.body.firstName) {
                            subAdmin.firstName = req.body.firstName
                        }
                        if (req.body.lastName) {
                            subAdmin.lastName = req.body.lastName
                        }
                        if (req.body.password) {
                            subAdmin.password = bcrypt.hashSync(req.body.password);
                        }
                        if (req.body.mobileNumber) {
                            subAdmin.mobileNumber = req.body.mobileNumber
                        }
                        if (req.body.permissionId) {
                            subAdmin.permissions = [{
                                _id: req.body.permissionId,
                                dashboard: req.body.dashboard,
                                userManagement: req.body.userManagement,
                                subAdminManagement: req.body.subAdminManagement,
                                agentManagement: req.body.agentManagement,
                                moneyManagement: req.body.moneyManagement,
                                commissionManagement: req.body.commissionManagement,
                                transactionManagement: req.body.transactionManagement,
                                chatManagement: req.body.chatManagement,
                                staticContentManagement: req.body.staticContentManagement,
                                journalManagement: req.body.journalManagement,
                                agentTransactionManagement: req.body.agentTransactionManagement,
                                kycManagement: req.body.kycManagement
                            }]
                        }
                        subAdmin.fullName=req.body.firstName+" "+req.body.lastName
                        userModel.findOneAndUpdate({ "_id": subData._id }, { $set: subAdmin }, { new: true }, (error, updateData) => {
                            console.log("gredsfaef633", error, updateData)
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                            }
                            else {
                                response(res, SuccessCode.SUCCESS, updateData, SuccessMessage.UPDATE_SUCCESS);
                            }
                        })
                    }
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
          * Function Name :viewSubAdmin
          * Description   : view perticuler subadmin by admin
          *
          * @return response
          */
    viewSubAdmin: (req, res) => {
        userModel.findOne({ "_id": req.body.id, userType: "SUBADMIN", status: { $ne: "DELETE" } }, (error, subData) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!subData) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND)
            }
            else {
                response(res, SuccessCode.SUCCESS, subData, SuccessMessage.PROFILE_DETAILS);
            }
        })
    },


    /**
           * Function Name :block/unblock sub admin
           * Description   : block or unblock particular sub-admin
           *
           * @return response
           */
    blockUnblockSubAdmin: (req, res) => {
        try {
            userModel.findOne({ "_id": req.body.id, userType: "SUBADMIN", status: { $ne: "DELETE" } }, (error, subData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!subData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND)
                }
                else {
                    if (subData.status == "ACTIVE") {
                        userModel.findOneAndUpdate({ _id: subData._id, userType: "SUBADMIN" }, { $set: { status: "BLOCK" } }, { new: true }, (error, statusUpdate) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                            }
                            else {
                                response(res, SuccessCode.SUCCESS, statusUpdate, SuccessMessage.SUB_ADMIN_BLOCK)
                            }
                        })
                    }
                    else {
                        userModel.findOneAndUpdate({ _id: subData._id, userType: "SUBADMIN" }, { $set: { status: "ACTIVE" } }, { new: true }, (error, statusUpdate) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                            }
                            else {
                                response(res, SuccessCode.SUCCESS, statusUpdate, SuccessMessage.SUB_ADMIN_UNBLOCK)
                            }
                        })
                    }

                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
    * Function Name : get all sub-admin
    * Description : get all the sub-admin details
    *
    * @return response
    */
    getAllSubAdmin: (req, res) => {
      
        try {
            var query = { $and: [{ status: { $ne: "DELETE" } }, { userType: "SUBADMIN" }] };

            if (req.body.search) {
                query.$or = [{ subAdmin_Id: { $regex: req.body.search, $options: 'i' } },
                { firstName: { $regex: req.body.search, $options: 'i' } },
                { lastName: { $regex: req.body.search, $options: 'i' } },
                { fullName: { $regex: req.body.search, $options: 'i' } }
                ]
            }

            var options = {
                page: req.body.pageNumber || 1,
                limit: req.body.limit || 10,

            }
            userModel.paginate(query, options, (error, userData) => {
                console.log("gredsfaef633", error, userData)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (userData.docs == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                }
            })
        }

        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
* Function Name : delete sub-admin
* Description : delete sub-admin
*
* @return response
*/
    deleteSubAdmin: (req, res) => {
        try {
            userModel.findOne({ "_id": req.body.id, userType: "SUBADMIN" }, (error, subData) => {
                console.log("gredsfaef633", error, subData)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!subData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND)
                }
                else {
                    userModel.findOneAndUpdate({ "_id": subData._id, userType: "SUBADMIN" }, { $set: { status: "DELETE" } }, { new: true }, (error, deleteSub) => {
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, deleteSub, SuccessMessage.DELETE_SUCCESS)
                        }
                    })
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },

    /**
          * Function Name :viewUser
          * Description   : view perticuler user by admin
          *
          * @return response
          */
    viewUser: (req, res) => {
        try {
            userModel.findOne({ _id: req.body.userId, userType: "CUSTOMER", status: { $ne: "DELETE" } }, (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!userData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);

        }

    },
    /**
     * Function Name :ACTIVE/BLOCK User
     * Description   :Active and Block status in user Management
     *
     * @return response
     */
    activeBlockUser: (req, res) => {
        try {
            userModel.findOne({ _id: req.body.userId, status: { $ne: "DELETE" } }, (err, result) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
                } else if (!result) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
                }
                else {
                    if (result.status == "ACTIVE") {
                        userModel.findOneAndUpdate({ _id: req.body.userId, status: "ACTIVE", userType: "CUSTOMER" }, { $set: { status: "BLOCK" } }, { new: true }, (error, userData) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
                            } else if (!userData) {
                                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
                            } else {
                                response(res, SuccessCode.SUCCESS, userData, SuccessMessage.BLOCK_SUCCESS)
                            }
                        })
                    }
                    else {
                        userModel.findOneAndUpdate({ _id: req.body.userId, status: "BLOCK", userType: "CUSTOMER" }, { $set: { status: "ACTIVE" } }, { new: true }, (error, userData) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
                            } else if (!userData) {
                                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
                            } else {
                                response(res, SuccessCode.SUCCESS, userData, SuccessMessage.ACTIVE_SUCCESS)
                            }
                        })
                    }
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);

        }
    },

    /**
         * Function Name :show All Customer
         * Description   :show All Customer in user Management
         *
         * @return response
         */
    showallCustomers: (req, res) => {
        try {
            var query = { status: { $ne: "DELETE" }, userType: "CUSTOMER" };

            if (req.body.search) {
                query.$or = [{ firstName: { $regex: req.body.search, $options: 'i' } },
                { lastName: { $regex: req.body.search, $options: 'i' } },
                { fullName: { $regex: req.body.search, $options: 'i' } }
                ]
            }
            if (req.body.state) {
                query.state = req.body.state;
            }
            var options = {
                page: req.body.pageNumber || 1,
                limit: req.body.limit || 10,

            }
            userModel.paginate(query, options, (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (userData.docs == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
        }

    },
    /**
         * Function Name :deleteUser
         * Description   : delete user by admin
         *
         * @return response
         */
    deleteUser: (req, res) => {
        try {
            userModel.findByIdAndUpdate({ _id: req.body.userId, status: "ACTIVE", userType: "CUSTOMER" }, { $set: { status: "DELETE" } }, { new: true }, (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!userData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DELETE_SUCCESS);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
        }
    },


    /**
          * Function Name :viewAgent
          * Description   : view agent by admin
          *
          * @return response
          */
    viewAgent: (req, res) => {
        try {
            userModel.findOne({ agentId: req.body.agentId, userType: "AGENT", status: { $ne: "DELETE" } }, (error, agentData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!agentData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, agentData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);

        }

    },
    /**
     * Function Name :deleteAgent
     * Description   : delete agent by admin
     *
     * @return response
     */
    deleteAgent: (req, res) => {
        try {
            userModel.findByIdAndUpdate({ _id: req.body.userId, status: "ACTIVE", userType: "AGENT" }, { $set: { status: "DELETE" } }, { new: true }, (error, agentData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!agentData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, agentData, SuccessMessage.DELETE_SUCCESS);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);

        }

    },

    /**
 * Function Name :ACTIVE/BLOCK Agent
 * Description   :Active and Block status in user Management
 *
 * @return response
 */
    activeBlockAgent: (req, res) => {
        try {
            userModel.findOne({ _id: req.userId, userType: "ADMIN" }, (err, adminResult) => {
                
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    userModel.findOne({  _id: req.body.userId, userType: "AGENT" }, (errAgent, resultAgent) => {
                        
                        if (errAgent) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else if (resultAgent.status == "ACTIVE") {
                            userModel.findOneAndUpdate({  _id: req.body.userId, status: "ACTIVE", userType: "AGENT" }, { $set: { status: "BLOCK" } }, { new: true }, (error, userData) => {
                                console.log("KIJJ::::::::1",error,userData)
                                if (error) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
                                } else if (!userData) {
                                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
                                } else {
                                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.BLOCK_SUCCESS)
                                }
                            })
                        }
                        else if (resultAgent.status == "BLOCK") {
                            userModel.findOneAndUpdate({ _id: req.body.userId, status: "BLOCK", userType: "AGENT" }, { $set: { status: "ACTIVE" } }, { new: true }, (error, userData) => {
                                console.log("KIJJ::::::::2",error,userData)
                                if (error) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
                                } else if (!userData) {
                                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
                                } else {
                                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.ACTIVE_SUCCESS)
                                }
                            })
                        }
                    })
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);

        }
    },
    //==============================================block customer by admin=============================================================

    /**
 * Function Name :ACTIVE/BLOCK Agent
 * Description   :Active and Block status in user Management
 *
 * @return response
 */
    activeBlockCustomer: (req, res) => {
        try {
            userModel.findOne({ _id: req.userId, userType: "ADMIN" }, (err, adminResult) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else {
                    userModel.findOne({ mobileNumber: req.body.mobileNumber, userType: "CUSTOMER" }, (errAgent, resultCustomer) => {
                        if (errAgent) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else if (resultCustomer.status == "ACTIVE") {
                            userModel.findOneAndUpdate({ mobileNumber: resultCustomer.mobileNumber, status: "ACTIVE", userType: "CUSTOMER" }, { $set: { status: "BLOCK" } }, { new: true }, (error, userData) => {
                                if (error) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
                                } else if (!userData) {
                                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
                                } else {
                                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.BLOCK_SUCCESS)
                                }
                            })
                        }
                        else if (resultCustomer.status == "BLOCK") {
                            userModel.findOneAndUpdate({ mobileNumber: resultCustomer.mobileNumber, status: "BLOCK", userType: "AGENT" }, { $set: { status: "ACTIVE" } }, { new: true }, (error, userData) => {
                                if (error) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
                                } else if (!userData) {
                                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
                                } else {
                                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.ACTIVE_SUCCESS)
                                }
                            })
                        }
                    })
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);

        }
    },



    /**
         * Function Name :show All Agent
         * Description   :show All Agent in user Management
         *
         * @return response
         */
    showallAgent: (req, res) => {
        try {
            var query = { status: { $ne: "DELETE" }, userType: "AGENT" };

            if (req.body.search) {
                query.$or = [{ firstName: { $regex: req.body.search, $options: 'i' } },
                { lastName: { $regex: req.body.search, $options: 'i' } },
                { agentId: { $regex: req.body.search, $options: 'i' } },
                { fullName: { $regex: req.body.search, $options: 'i' } }
                ]
            }
            if (req.body.state) {
                query.state = req.body.state;
            }
            var options = {
                page: req.body.pageNumber || 1,
                limit: req.body.limit || 10,

            }
            userModel.paginate(query, options, (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (userData.docs == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);

        }

    },



    /**
          * Function Name :get all the list of kyc users
          * Description   : list of kyc users
          *
          * @return response
          */

    getAllKycDetails: (req, res) => {
        try {
            var query = { status: { $ne: "DELETE" } };

            if (req.body.search) {
                query = { name: { $regex: req.body.search, $options: 'i' } }

            }
            var options = {
                page: req.body.pageNumber || 1,
                limit: req.body.limit || 10,

            }
            kycModel.paginate(query, options, (error, userData) => {
                console.log("ramanvv", query, userData)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (userData.docs == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);

        }

    },
    /**
          * Function Name :view kyc
          * Description   : view particular user kyc
          *
          * @return response
          */
    viewParticularKycDetails: (req, res) => {
        try {
            kycModel.findOne({ "_id": req.body.kycId }, (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!userData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
        }



    },

    // /**
    //   * Function Name :get all the list of kyc users
    //   * Description   : list of kyc users
    //   *
    //   * @return response
    //   */

    // getAllKycDetails: (req, res) => {
    //     try {

    //         var options = {
    //             page: req.body.pageNumber || 1,
    //             limit: req.body.limit || 10,

    //         }
    //         kycModel.paginate({}, options, (error, userData) => {
    //             if (error) {
    //                 response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
    //             } else if (userData.docs == 0) {
    //                 response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
    //             } else {
    //                 response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
    //             }
    //         })
    //     }
    //     catch (error) {
    //         response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);

    //     }

    // },
    /**
          * Function Name :approved kyc details 
          * Description   : kyc details approve by the admin
          *
          * @return response          
          */
    approveKycByAdmin: (req, res) => {
        try {
            userModel.findOne({ "_id": req.userId, userType: { $in: ["ADMIN", "SUBADMIN"] } }, (error, data) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.REQUEST_NOT_FOUND);
                } else {
                    kycModel.findOne({ "_id": req.body.kycId }, (error, doucmentFound) => {
                        console.log("==================>", error, doucmentFound.kycDocument)
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else if (!doucmentFound) {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.REQUEST_NOT_FOUND);
                        }
                        else {
                            // var phoneNumber = "+91" + doucmentFound.customer_mobileNumber
                            byWhom = data.firstName + data.lastName
                            doucmentFound.fullContactNumber
                            commonFunction.sendSMS(doucmentFound.fullContactNumber, 'Dear Your Kyc Request has been approved successfully', (error, sentMessage) => {
                                console.log("==================>", error, doucmentFound.kycDocument)
                                if (!error) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                                }
                                else {
                                    // kycModel.findByIdAndUpdate({ "_id": doucmentFound._id, "kycDocument.customer_Id": req.body.docId, kycStatus: "requested" }
                                    kycModel.findByIdAndUpdate({ "_id": doucmentFound._id, kycStatus: "requested" }
                                        , { $set: { kycStatus: "approved", approvedDate: Date.now() } },
                                        { new: true }, (error, statusApprove) => {
                                            console.log("09090000000000000000", error, statusApprove)
                                            if (error) {
                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                                            }
                                            else {
                                                userModel.findOneAndUpdate({ "_id": doucmentFound.customerId, kycStatus: "unverified", byWhom: "" }, { $set: { kycStatus: "verified", byWhom: byWhom } },
                                                    { new: true }, (error, statusVerified) => {
                                                        if (error) {
                                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                        }
                                                        else {
                                                            response(res, SuccessCode.SUCCESS, statusVerified, SuccessMessage.KYC_APPROVED);
                                                        }
                                                    })
                                            }
                                        })
                                }
                            })

                        }
                    })
                }


            })
        }

        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
        }

    },
    /**
          * Function Name :delete particular user kyc
          * Description   : deletet particular user kyc details
          *
          * @return response   
          */
    deletekyc: (req, res) => {
        try {
            kycModel.findOne({ "_id": req.body.kycId, status: "ACTIVE" }, (error, data) => {
                console.log("==============>", data)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    kycModel.findOneAndUpdate({ "_id": data._id }, { $set: { status: "DELETE" } }, { new: true }, (error, deletekyc) => {
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else if (!deletekyc) {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, deletekyc, SuccessMessage.DELETE_SUCCESS)
                        }
                    })
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
        }

    },

    rejectedKycByAdmin: (req, res) => {
        try {
            userModel.findOne({ "_id": req.userId, userType: { $in: ["ADMIN", "SUBADMIN"] } }, (error, data) => {
                kycModel.findById({ _id: req.body.kycId, kycStatus: "requested" }, (error, doucmentFound) => {
                    console.log("==================>", error, doucmentFound)
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (!doucmentFound) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.REQUEST_NOT_FOUND);
                    }

                    else {
                        byWhom = data.firstName + data.lastName
                        doucmentFound.fullContactNumber
                        commonFunction.sendSMS(doucmentFound.fullContactNumber, 'Dear Your Kyc Request has been rejected.', (error, sentMessage) => {
                            console.log("==================>", error, doucmentFound.kycDocument)
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                            } else {
                                kycModel.findByIdAndUpdate({ "_id": doucmentFound._id, kycStatus: "requested" }
                                    , { $set: { kycStatus: "rejected", approvedDate: Date.now() } },
                                    { new: true }, (error, statusApprove) => {
                                        console.log("09090000000000000000", error, statusApprove)
                                        if (error) {
                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                                        }
                                        else {
                                            userModel.findOneAndUpdate({ "_id": doucmentFound.customerId, kycStatus: "unverified", byWhom: "" }, { $set: { kycStatus: "rejected", byWhom: byWhom } },
                                                { new: true }, (error, statusReject) => {
                                                    if (error) {
                                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                    }
                                                    else {
                                                        response(res, SuccessCode.SUCCESS, statusReject, SuccessMessage.KYC_REJECT);
                                                    }
                                                })
                                        }
                                    })
                            }
                            // kycModel.findByIdAndUpdate({ "_id": doucmentFound._id, "kycDocument.customer_Id": req.body.docId, kycStatus: "requested" }
                        })
                    }
                })
            })
        }

        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
        }

    },
    /**
         * Function Name :qrcode list of  users
         * Description   : qrcode list of users
         *
         * @return response
         */

    qrCodeList: (req, res) => {
        try {
            var query = { status: { $ne: "DELETE" } };

            if (req.body.search) {
                query.$or = [{ firstName: { $regex: req.body.search, $options: 'i' } },
                { fullName: { $regex: req.body.search, $options: 'i' } }
            ]
            }
            var options = {
                page: req.body.page || 1,
                limit: parseInt(req.body.limit) || 10,
            }
            qrCodeModel.paginate(query, options, (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (userData.docs.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);

        }

    },

    viewQRCode: (req, res) => {
        try {
            qrCodeModel.findOne({ _id: req.params.qrId, status: { $ne: "DELETE" } }, (err, result) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (!result) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, result, SuccessMessage.DETAIL_GET);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
        }
    },

    /**
     * Function Name :qrcode blockUnblock users
     * Description   : qrcode status change of users
     *
     * @return response
     */

    blockQRuser: (req, res) => {
        try {
            qrCodeModel.findOne({ _id: req.body.qrId, status: { $ne: "DELETE" } }, (err, result) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
                } else if (!result) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
                }
                else {
                    if (result.status == "ACTIVE") {
                        qrCodeModel.findOneAndUpdate({ _id: result._id }, { $set: { status: "BLOCK" } }, { new: true }, (error, userData) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
                            } else {
                                response(res, SuccessCode.SUCCESS, userData, SuccessMessage.BLOCK_SUCCESS)
                            }
                        })
                    }
                    else {
                        qrCodeModel.findOneAndUpdate({ _id: result._id }, { $set: { status: "ACTIVE" } }, { new: true }, (error, userData) => {
                            if (error) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
                            } else {
                                response(res, SuccessCode.SUCCESS, userData, SuccessMessage.ACTIVE_SUCCESS)
                            }
                        })
                    }
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);

        }
    },

    /**
     * Function Name :add Money by admin
     * Description   : add Money by admin to customer
     *
     * @return response
     */
    setMoney: (req, res) => {
        commonFunction.jwtDecode(req.headers.token, (error, result) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);

            }
            else {

                req.body.forEach(item => {
                    new moneyModel(item).save((error, savedData) => {
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, ErrorCode.SuccessCode.SUCCESS, savedData, SuccessMessage.DATA_SAVED);
                        }
                    })
                })
                response(res, SuccessCode.SUCCESS, [], SuccessMessage.DATA_SAVED)

            }
        })
    },

    getMoney: (req, res) => {
        try {
            var options = {
                page: req.body.pageNumber || 1,
                limit: req.body.limit || 10,

            }
            moneyModel.paginate({}, options, (error, userData) => {
                console.log("===========>", error, userData)
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (userData.docs == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);

        }
    },

    setCommission: (req, res) => {
        userModel.findOne({ _id: req.userId }, (error, result) => {
           
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!result) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);

            }
            else {
                req.body.forEach(elememt => {
                    new commissionModel(elememt).save((error, saveData) => {
                        if (error) {
                            console.log("==>", error)

                        }

                        else {
                            console.log("==>", saveData)
                        }


                    })

                })
                response(res, SuccessCode.SUCCESS, SuccessMessage.DATA_SAVED)
            }

        })
    },
    getCommission: (req, res) => {
        try {
            // var options = {
            //     page: req.body.pageNumber || 1,
            //     limit: req.body.limit || 10,

            // }
            commissionModel.find({}, (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (userData.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);

        }
    },

    advertisment: (req, res) => {
        userModel.findOne({ _id: req.userId, userType: "ADMIN" }, (errAdmin, resultAdmin) => {
            if (errAdmin) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                commonFunction.multipleImageUploadCloudinary(req.body.image, (error, imageData) => {
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else {
                        var arr = []
                        imageData.forEach((e) => { arr.push({ image: e }) })
                        var obj = {
                            advImage: arr
                        }

                        new advModel(obj).save((err, saveImage) => {
                            if (err) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                            }
                            else {
                                response(res, SuccessCode.SUCCESS, saveImage, SuccessMessage.IMAGE_UPLOAD);
                            }
                        })

                    }
                })
            }
        })

    },

    editAdvertisment: (req, res) => {
        userModel.findOne({ _id: req.userId, userType: "ADMIN" }, (errAdmin, resultAdmin) => {
            if (errAdmin) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                advModel.findOne({ _id: req.body.advertismentId }, (error, data) => {
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (!data) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                    }
                    else {
                        commonFunction.uploadImage(req.body.image, (err, imageUrl) => {
                            if (err) {
                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                            }
                            else {
                                var set = {}

                                set['advImage.$.image'] = imageUrl

                                advModel.findOneAndUpdate({ "advImage._id": req.body.imageId }, { $set: { "advImage.$.image": imageUrl } }, { new: true }, (error, updateImage) => {

                                    if (error) {
                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                    }

                                    else if (!updateImage) {
                                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                                    }

                                    else {
                                        response(res, SuccessCode.SUCCESS, updateImage, SuccessMessage.UPDATE_SUCCESS);
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    },

    deleteImg: (req, res) => {
        advModel.findOneAndUpdate({ "advImage._id": req.body.imageId }, { $set: { "advImage.$.status": "DELETE" } }, { new: true }, (error, updateImage) => {

            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }

            else if (!updateImage) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }

            else {
                response(res, SuccessCode.SUCCESS, updateImage, SuccessMessage.UPDATE_SUCCESS);
            }
        })

    },

    getAdvertisment: (req, res) => {
        advModel.find((error, data) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            else {
                var arr = []
                data.forEach((ele) => {
                    ele.advImage.forEach((e) => {
                        if (e.status == "ACTIVE") {
                            arr.push(e)
                        }
                    })
                })
                response(res, SuccessCode.SUCCESS, arr, SuccessMessage.DATA_FOUND);
            }
        })

    },




    getPost: (req, res) => {
        try {
            var options = {
                page: req.body.pageNumber || 1,
                limit: req.body.limit || 10,

            }
            postModel.paginate({}, options, (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (userData.docs == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);

        }
    },
    particularViewPost: (req, res) => {
        try {
            postModel.findOne({ _id: req.body.postId, }, (error, adminData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                }
                else if (!adminData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND)
                }
                else {
                    response(res, SuccessCode.SUCCESS, adminData, SuccessMessage.DETAIL_GET);

                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },

    addAgentTransaction: (req, res) => {
       
        new transactionModel(req.body).save((error, saveImage) => {


            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                response(res, SuccessCode.SUCCESS, saveImage, SuccessMessage.DATA_SAVED);
            }
        })



    },
    getAllAgentTransaction: (req, res) => {
        try {
            var options = {
                page: req.body.pageNumber || 1,
                limit: req.body.limit || 10,

            }
            transactionModel.paginate({}, options, (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (userData.docs == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
          
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);

        }
    },

    exchangeMoney: (req, res) => {

        new exchangeModel(req.body).save((error, saveData) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                response(res, SuccessCode.SUCCESS, saveData, SuccessMessage.DATA_SAVED);
            }
        })
    },

    editExchangeAmount: (req, res) => {
        exchangeModel.findOne({ _id: req.body.id }, (error, Data) => {
                        
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!Data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            else {
                var obj = {
                }
                if (req.body.sell) {
                    obj.sell = req.body.sell
                }
                if (req.body.buy) {
                    obj.buy = req.body.buy
                }
                exchangeModel.findOneAndUpdate({ _id: req.body.id }, { $set: obj }, { new: true }, (error, updata) => {
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (!updata) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                    }
                    else {
                        response(res, SuccessCode.SUCCESS, updata, SuccessMessage.UPDATE_SUCCESS)
                    }
                })
            }
        })
    },

    viewTransaction: (req, res) => {
        try {
            transactionModel.findOne({ "_id": req.body.trasactionId, status: "ACTIVE" }, (error, transacionData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!transacionData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, transacionData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },

    getAllTransaction: (req, res) => {
        try {
            var options = {
                page: req.body.pageNumber || 1,
                limit: req.body.limit || 10,

            }
            transactionModel.paginate({}, options, (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (userData.docs == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            console.log("=====>", error)
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);

        }
    },


    getExchangeMoney: (req, res) => {
        try {
            // var options = {
            //     page: req.body.pageNumber || 1,
            //     limit: req.body.limit || 10,

            // }
            exchangeModel.find({}, (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (userData.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            console.log("=====>", error)
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);

        }
    },

    addAgentByAdmin: async (req, res) => {
        try {
            userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: "ADMIN" }, (error, userData) => {
           
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!userData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    var query = { $and: [{ $or: [{ mobileNumber: req.body.mobileNumber }, { agentId: req.body.agentId }] }, { status: { $ne: "DELETE" } }] }
                    userModel.findOne(query, (error, agentData) => {
                        console.log("-12333333333333333", error, agentData)
                        if (error) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else if (agentData) {
                            if (agentData.mobileNumber == req.body.mobileNumber) {
                                response(res, ErrorCode.ALREADY_EXIST, [], ErrorMessage.MOBILE_EXIST);
                            }
                            else if (agentData.agentId == req.body.agentId) {
                                response(res, ErrorCode.ALREADY_EXIST, [], ErrorMessage.AGENT_ID_EXIST);
                            }

                        }
                        else {
                            phoneNumber = req.body.countryCode + req.body.mobileNumber
                            commonFunction.sendSMS(phoneNumber, `Dear ${req.body.firstName}, Congratulations your agent account has been created as a AGENT.<br> Your agentId and password are:-<br> AgentId: ${req.body.agentId}
                              <br> Password: ${req.body.password}<br> adminId: ${userData.adminId}`, async (error, emailResult) => {
                                console.log("45666==============>", error)
                                if (error) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                    var qrCodeDetails = `"emailId":"${req.body.emailId}","mobileNumber":"${req.body.mobileNumber}","firstName":"${req.body.firstName}"`
                                    commonFunction.qrcodeGenrate(qrCodeDetails, async (error, qrResult) => {
                                        //console.log("============789===", error)
                                        if (error) {
                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                        }
                                        else {
                                            var obj = new userModel({
                                                profilePic: await convertImage(req.body.profilePic),
                                                firstName: req.body.firstName,
                                                lastName: req.body.lastName,
                                                mobileNumber: req.body.mobileNumber,
                                                emailId: req.body.emailId,
                                                city: req.body.city,
                                                state: req.body.state,
                                                countryCode: req.body.countryCode,
                                                agentId: req.body.agentId,
                                                userType: "AGENT",
                                                qrCode: await convertImage(qrResult),
                                                password: bcrypt.hashSync(req.body.password),
                                                fullName:req.body.firstName+" "+req.body.lastName,
                                                adminId: userData.adminId,
                                                byWhom: "",
                                                location: {
                                                    "type": "Point",
                                                    "coordinates": [0, 0]
                                                },

                                            })
                                            obj.save(async (error, savedData) => {
                                                
                                                if (error) {
                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                }
                                                else {
                                                    var data = {
                                                        userId: savedData._id,
                                                        firstName: savedData.firstName,
                                                        lastName: savedData.lastName,
                                                        emailId: savedData.emailId,
                                                        mobileNumber: savedData.mobileNumber,
                                                        city: savedData.city,
                                                        state: savedData.state,
                                                        qrCode: savedData.qrCode
                                                    };

                                                    await new qrCodeModel(data).save()
                                                    response(res, SuccessCode.SUCCESS, savedData, SuccessMessage.ACCOUNT_CREATION);
                                                }
                                            })
                                        }
                                    })

                                }

                            })

                        }
                    })
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
        }

    },
    approveRequestOfAgentByAdmin: (req, res) => {
        userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: "ADMIN" }, (error, userData) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!userData) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.AGENT_NOT_FOUND);
            }
            else {
                notificationModel.findOne({ _id: req.body._id, status: "requested" }, (error, request) => {
                 
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (!request) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.REQUEST_NOT_FOUND);
                    }
                    else {
                        commonFunction.sendTextOnMobileNumber(request.agent_mobileNumber, "your request has been approved",
                            (error, sentMessage) => {
                                if (error) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                    notificationModel.findByIdAndUpdate({ _id: request._id, status: "requested" },
                                        { $set: { status: "approved", updatedAt: Date.now() } }, { new: true }, (error, reqApproved) => {
                                            if (error) {
                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                            }
                                            else if (!reqApproved) {
                                                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.REQUEST_NOT_FOUND);
                                            }
                                            else {
                                                response(res, SuccessCode.SUCCESS, reqApproved, SuccessMessage.Request_APPROVED)
                                            }
                                        })
                                }
                            })
                    }
                })
            }
        })

    },
    rejectRequestOfCustomerByAgent: (req, res) => {
        userModel.findOne({ _id: req.userId, status: "ACTIVE", userType: "ADMIN" }, (error, userData) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                notificationModel.findOne({ _id: req.body._id, status: "requested" }, (error, request) => {
                   
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (!request) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.REQUEST_NOT_FOUND);
                    }
                    else {
                        commonFunction.sendTextOnMobileNumber(request.agent_mobileNumber, "your request has beeb rejected",
                            (error, sentMessage) => {
                          
                                if (error) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                    notificationModel.findOneAndUpdate({ _id: request._id, status: "requested" },
                                        { $set: { status: "rejected", updatedAt: Date.now() } }, { new: true }, (error, reqRejected) => {
                                            if (error) {
                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                            }
                                            else if (!reqRejected) {
                                                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.REQUEST_NOT_FOUND);
                                            }
                                            else {
                                                response(res, SuccessCode.SUCCESS, reqRejected, SuccessMessage.Request_REJECTED)
                                            }
                                        })
                                }
                            })
                    }
                })
            }

        })

    },

    getAllQuestion: (req, res) => {
        questionModel.find({ status: "ACTIVE" }, (error, result) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                response(res, SuccessCode.SUCCESS, result, SuccessMessage.DETAIL_GET);

            }
        })
    },

    rateChange: (req, res) => {
        userModel.findOne({ _id: req.userId, userType: "ADMIN" }, (error, adminResult) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                var obj = {}
                if (req.body.unitUSD) {
                    obj.unitUSD = req.body.unitUSD
                }
                if (req.body.unitCDF) {
                    obj.unitCDF = req.body.unitCDF
                }

                exchangeModel.findOneAndUpdate({ _id: req.body.rateId }, { $set: obj }, { new: true }, (err, rateUpdate) => {
                    if (err) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else {
                        response(res, SuccessCode.SUCCESS, rateUpdate, SuccessMessage.UPDATE_SUCCESS);
                    }
                })
            }
        })
    },
    getRate: (req, res) => {
        userModel.findOne({ _id: req.userId, userType: "ADMIN" }, (error, adminResult) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                exchangeModel.find({ status: "ACTIVE" }, (err, result) => {
                    if (err) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else {
                        response(res, SuccessCode.SUCCESS, result, SuccessMessage.DATA_FOUND);
                    }
                })
            }
        })
    },

    sendMoneyByAdmin: (req, res) => {
        userModel.findOne({ _id: req.userId, userType: "ADMIN", accountType: { $ne: "RECOVERY" } }, (error, adminDetails) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                userModel.findOne({ mobileNumber: req.body.mobileNumber, status: "ACTIVE", userType: { $in: ["AGENT", "CUSTOMER"] } },
                    async (err, numberDetails) => {
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else if (!numberDetails) {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                        }
                        else {
                            newcommissionModel.findOne({ status: "ACTIVE" }, (errComm, commissionDetails) => {
                                if (errComm) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                    moneyModel.findOne({ status: "ACTIVE" }, async (errDetails, transactionDetails) => {
                                        if (errDetails) {
                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                        }
                                        else {
                                            console.log("2244", req.body.amountType)
                                            if (numberDetails.userType == "AGENT" && req.body.amountType == "USD") {
                                                transactionModel.aggregate([
                                                    { $match: { $and: [{ "createdAt": { "$gte": start, "$lt": end } }, { "sender_id": req.userId }, { "sender_UserType": "ADMIN" }, { "receiver_UserType": "AGENT" }] } },
                                                    { "$group": { "_id": mongoose.Types.ObjectId(req.userId), "count": { $sum: 1 } } }], async (err1, newResult) => {
                                                        if (err1) {
                                                            response(res, ErrorCode.SOMETHING_WRONG, [err1], ErrorMessage.INTERNAL_ERROR);
                                                        }
                                                        else {
                                                            var final_result = Object.assign({}, ...newResult)
                                                            if (final_result.count >= transactionDetails.AdminToAgent_SEND) {
                                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.LIMIT_NUMBER_OF_TRANSACTION)
                                                            }
                                                            else if (adminDetails.amountUSD < req.body.amount || adminDetails.amountUSD == 0) {
                                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                            }
                                                            else {
                                                                var passwordCheck = bcrypt.compareSync(req.body.password, adminDetails.password)
                                                                if (passwordCheck) {
                                                                    var adminCommission = parseFloat(req.body.amount) * parseFloat(commissionDetails.admin_To_Agent / 100)// 10

                                                                    var final_amount = parseFloat(req.body.amount) - parseFloat(adminCommission)

                                                                    var adminBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: adminDetails.mobileNumber },
                                                                        { $set: { amountUSD: parseFloat(adminDetails.amountUSD) - parseFloat(req.body.amount) } }, { new: true })
                                                                    if (adminBalanceUpdate) {
                                                                        var agentBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: numberDetails.mobileNumber },
                                                                            { $set: { amountUSD: parseFloat(numberDetails.amountUSD) + parseFloat(final_amount) } }, { new: true })
                                                                        if (agentBalanceUpdate) {
                                                                            var admin_commission_updated = await userModel.findOneAndUpdate({ userType: "ADMIN", accountType: { $ne: "RECOVERY" } }, { $set: { commissionUSD: parseFloat(adminDetails.commissionUSD) + parseFloat(adminCommission) } }, { new: true })
                                                                            console.log("==================", admin_commission_updated)
                                                                            if (admin_commission_updated) {
                                                                                var commission_details_obj = {
                                                                                    "commission": commissionDetails.admin_To_Agent,
                                                                                    "send_amount": req.body.amount,
                                                                                    "receive_amount": final_amount,
                                                                                    "amountType": req.body.amountType,
                                                                                    "sender_UserType": adminDetails.userType,
                                                                                    "receiver_UserType": numberDetails.userType
                                                                                }
                                                                                var commission_Details = new newcommissionModel(commission_details_obj).save()
                                                                                if (commission_Details) {
                                                                                    var obj_details = {
                                                                                        "send_amount": req.body.amount,
                                                                                        "receive_amount": final_amount,
                                                                                        "commission": commissionDetails.admin_To_Agent,
                                                                                        "commission_amount": adminCommission,
                                                                                        "amountType": req.body.amountType,
                                                                                        "sendMoneyBy": adminDetails.firstName + " " + adminDetails.lastName,
                                                                                        "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                        "sender_id": adminDetails._id,
                                                                                        "sender_mobileNumber": adminDetails.mobileNumber,
                                                                                        "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                        "sender_UserType": adminDetails.userType,
                                                                                        "receiver_UserType": numberDetails.userType,
                                                                                        "type_transaction": "SEND",
                                                                                        "transactionStatus": "Debited",
                                                                                        "transectionType": "paid"
                                                                                    }
                                                                                    new transactionModel(obj_details).save((errSbmit, transDetails) => {
                                                                                        if (errSbmit) {
                                                                                            response(res, ErrorCode.SOMETHING_WRONG, [errSbmit], ErrorMessage.INTERNAL_ERROR);
                                                                                        }
                                                                                        else {
                                                                                            receiver_details = {
                                                                                                "send_amount": req.body.amount,
                                                                                                "receive_amount": final_amount,
                                                                                                "commission": commissionDetails.agent_To_Client, "amountType": req.body.amountType,
                                                                                                "sendMoneyBy": adminDetails.firstName + " " + adminDetails.lastName,
                                                                                                "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                                //"sender_id": agentDetails._id,
                                                                                                "receiver_id": numberDetails._id,
                                                                                                "sender_mobileNumber": adminDetails.mobileNumber,
                                                                                                "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                                "sender_UserType": adminDetails.userType,
                                                                                                "receiver_UserType": numberDetails.userType,
                                                                                                "transactionStatus": "Credited",
                                                                                                "transectionType": "Recieved"
                                                                                            }
                                                                                            new transactionModel(receiver_details).save((err, transactionDetails) => {
                                                                                                if (err) {
                                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [err], ErrorMessage.INTERNAL_ERROR);
                                                                                                }
                                                                                                else {
                                                                                                    response(res, SuccessCode.SUCCESS, transactionDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                                                }
                                                                                            })
                                                                                        }
                                                                                    })
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }else {
                                                                    response(res, ErrorCode.INVALID_CREDENTIAL, [], ErrorMessage.WRONG_PASSWORD)
                                                                }
                                                            }
                                                        }
                                                    })
                                            }
                                            else if (numberDetails.userType == "AGENT" && req.body.amountType == "SDG") {
                                                transactionModel.aggregate([
                                                    { $match: { $and: [{ "createdAt": { "$gte": start, "$lt": end } }, { "sender_id": req.userId }, { "sender_UserType": "ADMIN" }, { "receiver_UserType": "AGENT" }] } },
                                                    { "$group": { "_id": mongoose.Types.ObjectId(req.userId), "count": { $sum: 1 } } }], async (err1, newResult) => {
                                                        if (err1) {
                                                            response(res, ErrorCode.SOMETHING_WRONG, [err1], ErrorMessage.INTERNAL_ERROR);
                                                        }
                                                        else {
                                                            var final_result = Object.assign({}, ...newResult)
                                                            if (final_result.count >= transactionDetails.AdminToAgent_SEND) {
                                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.LIMIT_NUMBER_OF_TRANSACTION)
                                                            }
                                                            else if (adminDetails.amountCDF < req.body.amount || adminDetails.amountCDF == 0) {
                                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                            }
                                                            else {
                                                                passwordCheck = bcrypt.compareSync(req.body.password, adminDetails.password)
                                                                if (passwordCheck) {
                                                                    var adminCommission = parseFloat(req.body.amount) * parseFloat(commissionDetails.admin_To_Agent / 100)// 10

                                                                    var final_amount = parseFloat(req.body.amount) - parseFloat(adminCommission)

                                                                    adminBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: adminDetails.mobileNumber },
                                                                        { $set: { amountCDF: parseFloat(adminDetails.amountCDF) - parseFloat(req.body.amount) } }, { new: true })
                                                                    if (adminBalanceUpdate) {
                                                                        var agentBalanceUpdate = await userModel.findOneAndUpdate({ mobileNumber: numberDetails.mobileNumber },
                                                                            { $set: { amountCDF: parseFloat(numberDetails.amountCDF) + parseFloat(final_amount) } }, { new: true })
                                                                        if (agentBalanceUpdate) {
                                                                            var admin_commission_updated = await userModel.findOneAndUpdate({ userType: "ADMIN", accountType: { $ne: "RECOVERY" } }, { $set: { commissionCDF: parseFloat(adminDetails.commissionCDF) + parseFloat(adminCommission) } }, { new: true })
                                                                            console.log("==================", admin_commission_updated)
                                                                            if (admin_commission_updated) {
                                                                                var commission_details_obj = {
                                                                                    "commission": commissionDetails.admin_To_Agent,
                                                                                    "send_amount": req.body.amount,
                                                                                    "receive_amount": final_amount,
                                                                                    "amountType": req.body.amountType,
                                                                                    "sender_UserType": adminDetails.userType,
                                                                                    "receiver_UserType": numberDetails.userType
                                                                                }
                                                                                var commission_Details = new newcommissionModel(commission_details_obj).save()
                                                                                if (commission_Details) {
                                                                                    obj_details = {
                                                                                        "send_amount": req.body.amount,
                                                                                        "receive_amount": final_amount,
                                                                                        "commission": commissionDetails.admin_To_Agent,
                                                                                        "commission_amount": adminCommission,
                                                                                        "amountType": req.body.amountType,
                                                                                        "sendMoneyBy": adminDetails.firstName + " " + adminDetails.lastName,
                                                                                        "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                        "sender_id": adminDetails._id,
                                                                                        "sender_mobileNumber": adminDetails.mobileNumber,
                                                                                        "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                        "sender_UserType": adminDetails.userType,
                                                                                        "receiver_UserType": numberDetails.userType,
                                                                                        "type_transaction": "SEND",
                                                                                        "transactionStatus": "Debited",
                                                                                        "transectionType": "paid"
                                                                                    }
                                                                                    new transactionModel(obj_details).save((errSbmit, transDetails) => {
                                                                                        if (errSbmit) {
                                                                                            response(res, ErrorCode.SOMETHING_WRONG, [errSbmit], ErrorMessage.INTERNAL_ERROR);
                                                                                        }
                                                                                        else {
                                                                                            receiver_details = {
                                                                                                "send_amount": req.body.amount,
                                                                                                "receive_amount": final_amount,
                                                                                                "commission": commissionDetails.agent_To_Client, "amountType": req.body.amountType,
                                                                                                "sendMoneyBy": adminDetails.firstName + " " + adminDetails.lastName,
                                                                                                "receiveMoneyBy": numberDetails.firstName + " " + numberDetails.lastName,
                                                                                                //"sender_id": agentDetails._id,
                                                                                                "receiver_id": numberDetails._id,
                                                                                                "sender_mobileNumber": adminDetails.mobileNumber,
                                                                                                "receiver_mobileNumber": numberDetails.mobileNumber,
                                                                                                "sender_UserType": adminDetails.userType,
                                                                                                "receiver_UserType": numberDetails.userType,
                                                                                                "transactionStatus": "Credited",
                                                                                                "transectionType": "Recieved"
                                                                                            }
                                                                                            new transactionModel(receiver_details).save((err, transactionDetails) => {
                                                                                                if (err) {
                                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [err], ErrorMessage.INTERNAL_ERROR);
                                                                                                }
                                                                                                else {
                                                                                                    response(res, SuccessCode.SUCCESS, transactionDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                                                }
                                                                                            })
                                                                                        }
                                                                                    })
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    })
                                            }
                                        }
                                    })
                                }
                            })
                        }
                    })
            }
        })
      
    },
    addCommissionCDF: (req, res) => {
        userModel.findOne({ _id: req.userId, userType: "ADMIN", status: "ACTIVE", accountType: { $ne: "RECOVERY" } }, (error, userDetails) => {
            console.log("4444444444444", userDetails)
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
            }
            else if (userDetails.commissionCDF == 0) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE_TO_ADD);
            }
            else {
                var balance_update = parseFloat(userDetails.amountCDF) + parseFloat(userDetails.commissionCDF)
                var commission_update = parseFloat(userDetails.commissionCDF) - parseFloat(userDetails.commissionCDF)
                userModel.findOneAndUpdate({ _id: userDetails._id }, { $set: { amountCDF: balance_update, commissionCDF: commission_update } },
                    { new: true }, (err, updateBalance) => {
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, updateBalance, SuccessMessage.UPDATE_SUCCESS);
                        }
                    })
            }
        })
    },
    addCommissionUSD: (req, res) => {
        userModel.findOne({ _id: req.userId, userType: "ADMIN", status: "ACTIVE", accountType: { $ne: "RECOVERY" } }, (error, userDetails) => {
            console.log("4444444444444", userDetails)
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
            }
            else if (userDetails.commissionUSD == 0) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE_TO_ADD);
            }
            else {
                var balance_update = parseFloat(userDetails.amountUSD) + parseFloat(userDetails.commissionUSD)
                var commission_update = parseFloat(userDetails.commissionUSD) - parseFloat(userDetails.commissionUSD)
                userModel.findOneAndUpdate({ _id: userDetails._id }, { $set: { amountUSD: balance_update, commissionUSD: commission_update } },
                    { new: true }, (err, updateBalance) => {
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, updateBalance, SuccessMessage.UPDATE_SUCCESS);
                        }
                    })
            }
        })
    },
    adminDetails: (req, res) => {
        userModel.findOne({ adminId: req.body.adminId, userType: "ADMIN", accountType: { $ne: "RECOVERY" } }, (error, adminDetails) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                response(res, SuccessCode.SUCCESS, adminDetails, SuccessMessage.DATA_FOUND);
            }
        })
    },
    addAmountToAdminWallet: (req, res) => {
        userModel.findOne({ _id: req.userId, userType: "ADMIN", status: "ACTIVE", accountType: { $ne: "RECOVERY" } }, (error, userDetails) => {
            console.log("4444444444444", userDetails)
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!userDetails) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            else {
                if (req.body.amountType == "USD") {
                    var balance_update = parseFloat(userDetails.amountUSD) + parseFloat(req.body.amount)

                    userModel.findOneAndUpdate({ _id: userDetails._id }, { $set: { amountUSD: parseFloat(balance_update) } }, { new: true }, (amountErr, amountResult) => {
                        if (amountErr) {
                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, amountResult, SuccessMessage.UPDATE_SUCCESS);
                        }
                    })
                }
                else {
                    var balance_updates = parseFloat(userDetails.amountCDF) + parseFloat(req.body.amount)

                    userModel.findOneAndUpdate({ _id: userDetails._id }, { $set: { amountCDF: balance_updates } }, { new: true }, (amountErr, amountResult) => {
                        if (amountErr) {
                            response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, amountResult, SuccessMessage.UPDATE_SUCCESS);
                        }
                    })
                }

            }
        })
    },
    addRecoveryDocument: (req, res) => {
        userModel.findOne({ _id: req.userId, userType: "ADMIN", status: "ACTIVE", accountType: { $ne: "RECOVERY" } }, async (error, userDetails) => {
            console.log("4444444444444", userDetails)
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!userDetails) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            else {
                if (req.body.document) {
                    var pictures = await uploadDocument()
                }
                userModel.findOneAndUpdate({ mobileNumber: req.body.mobileNumber }, { $set: { recoveryDocument: pictures } }, { new: true }, (amountErr, amountResult) => {
                    if (amountErr) {
                        response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                    }
                    else {
                        response(res, SuccessCode.SUCCESS, amountResult, SuccessMessage.UPDATE_SUCCESS);
                    }

                })
                function uploadDocument() {
                    return new Promise((resolve, reject) => {
                        commonFunction.uploadImage(req.body.document, (uploadErr, uploaded) => {
                            if (uploadErr) {
                                console.log("Error uploading image")
                            }
                            else {
                                resolve(uploaded)
                            }
                        })
                    })
                }
            }
        })
    },

    recoverAmountByAdmin: (req, res) => {
        userModel.findOne({ _id: req.userId, accountType: "RECOVERY", userType: "ADMIN" }, (error, adminDetails) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!adminDetails) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            else {
                userModel.findOne({ mobileNumber: req.body.mobileNumber, status: "ACTIVE", userType: { $in: ["AGENT", "CUSTOMER"] } }, (errUser, userDetails) => {
                    if (errUser) {
                        response(res, ErrorCode.SOMETHING_WRONG, [errUser], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (!userDetails) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                    }
                    else {
                        if (req.body.amountType == "USD") {
                            var user_updateBalance = parseFloat(userDetails.amountUSD) - parseFloat(req.body.amount)
                            var admin_updateBalance = parseFloat(adminDetails.amountUSD) + parseFloat(req.body.amount)
                            userModel.findOneAndUpdate({ mobileNumber: userDetails.mobileNumber }, { $set: { amountUSD: user_updateBalance, } }, { new: true },
                                (errBalance, balanceUpdate) => {
                                    if (errBalance) {
                                        response(res, ErrorCode.SOMETHING_WRONG, [errUser], ErrorMessage.INTERNAL_ERROR);
                                    }
                                    else {
                                        userModel.findOneAndUpdate({ mobileNumber: userDetails.mobileNumber }, { $set: { amountUSD: admin_updateBalance } }, { new: true },
                                            (err_Balance, balanceUpdate_) => {
                                                if (err_Balance) {
                                                    response(res, ErrorCode.SOMETHING_WRONG, [err_Balance], ErrorMessage.INTERNAL_ERROR);
                                                }
                                                else if (req.body.amountType == "SDG") {
                                                    var user_update_Balance_ = parseFloat(userDetails.amountCDF) - parseFloat(req.body.amount)
                                                    var admin_update_Balance_ = parseFloat(adminDetails.amountCDF) + parseFloat(req.body.amount)
                                                    userModel.findOneAndUpdate({ mobileNumber: userDetails.mobileNumber }, { $set: { amountCDF: user_update_Balance_ } }, { new: true },
                                                        (err_Balance_, balance_Update) => {
                                                            if (err_Balance_) {
                                                                response(res, ErrorCode.SOMETHING_WRONG, [err_Balance_], ErrorMessage.INTERNAL_ERROR);
                                                            }
                                                            else {
                                                                userModel.findOneAndUpdate({ mobileNumber: adminDetails.mobileNumber }, { $set: { amountCDF: admin_update_Balance_ } }, { new: true },
                                                                    (err_balance, balance_Update_) => {
                                                                        if (err_balance) {
                                                                            response(res, ErrorCode.SOMETHING_WRONG, [err_balance], ErrorMessage.INTERNAL_ERROR);
                                                                        }
                                                                        else {
                                                                            receiver_details = {
                                                                                "receive_amount": req.body.amount,
                                                                                "amountType": req.body.amountType,
                                                                                "receiveMoneyBy": adminDetails.firstName + " " + adminDetails.lastName,
                                                                                "sender_id": userDetails._id,
                                                                                "receiver_id": adminDetails._id,
                                                                                "sender_mobileNumber": userDetails.mobileNumber,
                                                                                "receiver_mobileNumber": adminDetails.mobileNumber,
                                                                                "sender_UserType": userDetails.userType,
                                                                                "receiver_UserType": adminDetails.userType,
                                                                                "transactionStatus": "Credited",
                                                                                "transectionType": "Recieved",
                                                                                "accountType": "RECOVERY"
                                                                            }
                                                                            new transactionModel(receiver_details).save((errTrans, transDetails) => {
                                                                                if (errTrans) {
                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [error], ErrorMessage.INTERNAL_ERROR);
                                                                                }
                                                                                else {
                                                                                    response(res, SuccessCode.SUCCESS, transDetails, SuccessMessage.TRANSACTION_COMPLETED);
                                                                                }
                                                                            })
                                                                            //response(res, SuccessCode.SUCCESS, balance_Update_, SuccessMessage.TRANSACTION_COMPLETED);   
                                                                        }
                                                                    })
                                                            }
                                                        })

                                                }
                                            })
                                    }
                                })

                        }
                    }

                })
            }
        })
    },
    allTransaction: (req, res) => {
        var query = { $or: [{ type_transaction: "DEPOSIT" }, { type_transaction: "SEND" }, { type_transaction: "WITHDRAW" }] }
        transactionModel.find(query, (errTransaction, resultTransaction) => {
            if (errTransaction) {
                response(res, ErrorCode.SOMETHING_WRONG, [errTransaction], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                response(res, SuccessCode.SUCCESS, resultTransaction, SuccessMessage.DATA_FOUND);
            }
        })

    },
    clientTransaction: (req, res) => {
        var query = { $or: [{ sender_UserType: "CUSTOMER" }, { receiver_UserType: "CUSTOMER" }] }
        if (req.body.search) {
            query.$or = [{ sendMoneyBy: { $regex: req.body.search, $options: 'i' } },
            { transectionType: { $regex: req.body.search, $options: 'i' } },
             
            ]
        }
        if (req.body.fromDate && !req.body.toDate) {
            query.createdAt = { $gte: req.body.fromDate };
        }
        if (!req.body.fromDate && req.body.toDate) {
            query.createdAt = { $lte: req.body.toDate }
        }
        if (req.body.fromDate && req.body.toDate) {
            query.$and = [{ createdAt: { $gte: req.body.fromDate } }, { createdAt: { $lte: req.body.toDate } }];
        } req.body.limit = parseInt(req.body.limit);
        var options = {
            page: req.body.page || 1,
            limit: req.body.limit || 5,
            sort: { createdAt: -1 }
        };

        transactionModel.paginate(query, options, (errTransaction, resultTransaction) => {
            if (errTransaction) {
                response(res, ErrorCode.SOMETHING_WRONG, [errTransaction], ErrorMessage.INTERNAL_ERROR);
            } else if (resultTransaction.docs.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            }
            else {
                response(res, SuccessCode.SUCCESS, resultTransaction, SuccessMessage.DATA_FOUND);
            }

        })
    },
    agentTransaction: (req, res) => {
        var query = { $or: [{ sender_UserType: "AGENT" }, { receiver_UserType: "AGENT" }] }
        if (req.body.search) {
            query.$or = [{ sendMoneyBy: { $regex: req.body.search, $options: 'i' } },
            { transectionType: { $regex: req.body.search, $options: 'i' } },
            { agentId: { $regex: req.body.search, $options: 'i' } },
          
            ]
        }
        if (req.body.fromDate && !req.body.toDate) {
            query.createdAt = { $gte: req.body.fromDate };
        }
        if (!req.body.fromDate && req.body.toDate) {
            query.createdAt = { $lte: req.body.toDate }
        }
        if (req.body.fromDate && req.body.toDate) {
            query.$and = [{ createdAt: { $gte: req.body.fromDate } }, { createdAt: { $lte: req.body.toDate } }];
        }
        var options = {
            page: req.body.page || 1,
            limit: req.body.limit || 5,
            sort: { createdAt: -1 }
        };

        transactionModel.paginate(query, options, (errTransaction, resultTransaction) => {
            if (errTransaction) {
                response(res, ErrorCode.SOMETHING_WRONG, [errTransaction], ErrorMessage.INTERNAL_ERROR);
            } else if (resultTransaction.docs.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            }
            else {
                response(res, SuccessCode.SUCCESS, resultTransaction, SuccessMessage.DATA_FOUND);
            }

        })
    },
    adminTransaction: (req, res) => {
        var query = { $or: [{ sender_UserType: "ADMIN" }, { receiver_UserType: "ADMIN" }] }
        if (req.body.search) {
            query.$or = [{ sendMoneyBy: { $regex: req.body.search, $options: 'i' } },
            { transectionType: { $regex: req.body.search, $options: 'i' } },
            ]
        }
        if (req.body.fromDate && !req.body.toDate) {
            query.createdAt = { $gte: req.body.fromDate };
        }
        if (!req.body.fromDate && req.body.toDate) {
            query.createdAt = { $lte: req.body.toDate }
        }
        if (req.body.fromDate && req.body.toDate) {
            query.$and = [{ createdAt: { $gte: req.body.fromDate } }, { createdAt: { $lte: req.body.toDate } }];
        } req.body.limit = parseInt(req.body.limit);
        var options = {
            page: req.body.page || 1,
            limit: req.body.limit || 5,
            sort: { createdAt: -1 }
        };

        transactionModel.paginate(query, options, (errTransaction, resultTransaction) => {
            console.log("rajyajyaua",errTransaction,resultTransaction)
            if (errTransaction) {
                response(res, ErrorCode.SOMETHING_WRONG, [errTransaction], ErrorMessage.INTERNAL_ERROR);
            } else if (resultTransaction.docs.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            }
            else {
                response(res, SuccessCode.SUCCESS, resultTransaction, SuccessMessage.DATA_FOUND);
            }

        })
    },
    recoverTransaction: (req, res) => {
        transactionModel.find({ accountType: "RECOVERY" }, (errTransaction, resultTransaction) => {
            if (errTransaction) {
                response(res, ErrorCode.SOMETHING_WRONG, [errTransaction], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                response(res, SuccessCode.SUCCESS, resultTransaction, SuccessMessage.DATA_FOUND);
            }
        })
    },
    countTotalTransaction: (req, res) => {
        transactionModel.countDocuments({}, (err, countResult) => {
            if (err) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!countResult) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NO_LIST_AVILABALE);
            }
            else {
                response(res, SuccessCode.SUCCESS, countResult, SuccessMessage.DATA_FOUND);
            }
        })
    },
    countTotalForCustomer: (req, res) => {
        var query = { $or: [{ sender_UserType: "CUSTOMER" }, { receiver_UserType: "CUSTOMER" }] }
        transactionModel.countDocuments(query, (err, countResult) => {
            if (err) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!countResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NO_LIST_AVILABALE);
            }
            else {
                response(res, SuccessCode.SUCCESS, countResult, SuccessMessage.DATA_FOUND);
            }
        })
    },
    countTotalForAgent: (req, res) => {
        var query = { $or: [{ sender_UserType: "AGENT" }, { receiver_UserType: "AGENT" }] }
        transactionModel.countDocuments(query, (err, countResult) => {
            if (err) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!countResult) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NO_LIST_AVILABALE);
            }
            else {
                response(res, SuccessCode.SUCCESS, countResult, SuccessMessage.DATA_FOUND);
            }
        })
    },
    countTotalNumberOfUser: (req, res) => {
        userModel.countDocuments({ userType: "CUSTOMER" }, (err, countResult) => {
            if (err) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!countResult) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NO_LIST_AVILABALE);
            }
            else {
                response(res, SuccessCode.SUCCESS, countResult, SuccessMessage.DATA_FOUND);
            }
        })
    },
    countTotalNumberOfAgent: (req, res) => {
        userModel.countDocuments({ userType: "AGENT" }, (err, countResult) => {
            if (err) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!countResult) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NO_LIST_AVILABALE);
            }
            else {
                response(res, SuccessCode.SUCCESS, countResult, SuccessMessage.DATA_FOUND);
            }
        })
    },
    getTransaction: (req, res) => {
        userModel.findOne({ _id: req.userId, userType: "ADMIN" }, (err, adminDetails) => {
            if (err) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                moneyModel.findOne({ status: "ACTIVE" }, (error, moneyFound) => {
                    console.log("========")
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else {
                        response(res, SuccessCode.SUCCESS, moneyFound, SuccessMessage.DATA_FOUND);
                    }
                })
            }
        })
    },
    setTransactioLimit: (req, res) => {
        userModel.findOne({ _id: req.userId, userType: "ADMIN", accountType: { $ne: "RECOVERY" } }, (err, adminDetails) => {
            if (err) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                obj = {}
                if (req.body.clientToClient_SEND) {
                    obj.clientToClient_SEND = req.body.clientToClient_SEND
                }
                if (req.body.clientToAgent_WITHDRAW) {
                    obj.clientToAgent_WITHDRAW = req.body.clientToAgent_WITHDRAW
                }
                if (req.body.clientToAdmin_EXCHANGE) {
                    obj.clientToAdmin_EXCHANGE = req.body.clientToAdmin_EXCHANGE
                }
                if (req.body.AgentToClient_DEPOSITE) {
                    obj.AgentToClient_DEPOSITE = req.body.AgentToClient_DEPOSITE
                }
                if (req.body.AgentToAdmin_EXCHANGE) {
                    obj.AgentToAdmin_EXCHANGE = req.body.AgentToAdmin_EXCHANGE
                }
                if (req.body.AdminToAgent_SEND) {
                    obj.AdminToAgent_SEND = req.body.AdminToAgent_SEND
                }
                if (req.body.AdminToClient_EXCHANGE) {
                    obj.AdminToClient_EXCHANGE = req.body.AdminToClient_EXCHANGE
                }

                moneyModel.findOneAndUpdate({ status: "ACTIVE" }, { $set: obj }, { new: true }, (error, userData) => {
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    } else if (!userData) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
                    } else {
                        response(res, SuccessCode.SUCCESS, userData, SuccessMessage.LIMIT_SET);
                    }
                })
            }
        })

    },
    getCommissionDetails: (req, res) => {
        userModel.findOne({ _id: req.userId, userType: "ADMIN", accountType: { $ne: "RECOVERY" } }, (error, adminResult) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                newcommissionModel.findOne({ status: "ACTIVE" }, (err, result) => {
                    if (err) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else {
                        response(res, SuccessCode.SUCCESS, result, SuccessMessage.DATA_FOUND);
                    }
                })
            }
        })
    },

    setCommissionByAdmin: (req, res) => {
        userModel.findOne({ _id: req.userId, userType: "ADMIN", accountType: { $ne: "RECOVERY" } }, (err, adminDetails) => {
            if (err) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                obj = {}
                if (req.body.admin_To_Agent) {
                    obj.admin_To_Agent = req.body.admin_To_Agent
                }
                if (req.body.agent_To_Admin) {
                    obj.agent_To_Admin = req.body.agent_To_Admin
                }
                if (req.body.agent_To_Client) {
                    obj.agent_To_Client = req.body.agent_To_Client
                }
                if (req.body.client_To_Agent) {
                    obj.client_To_Agent = req.body.client_To_Agent
                }
                if (req.body.client_To_Client) {
                    obj.client_To_Client = req.body.client_To_Client
                }

                newcommissionModel.findOneAndUpdate({ status: "ACTIVE" }, { $set: obj }, { new: true }, (error, userData) => {
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    } else if (!userData) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
                    } else {
                        response(res, SuccessCode.SUCCESS, userData, SuccessMessage.commission_SET);
                    }
                })
            }
        })
    },
    checkBalance: (req, res) => {
        userModel.findOne({ _id: req.userId, status: "ACTIVE", accountType: { $ne: "RECOVERY" }, userType: "ADMIN" },
            (error, balanceData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!balanceData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    var available_balance = {
                        "amountUSD": balanceData.amountUSD,
                        "amountCDF": balanceData.amountCDF,
                        "mobileNumber": balanceData.mobileNumber,
                        "commissionUSD": balanceData.commissionUSD,
                        "commissionCDF": balanceData.commissionCDF
                    }
                    response(res, SuccessCode.SUCCESS, available_balance, SuccessMessage.DATA_FOUND)

                }
            })
    },

    approveExchangeMoneyRequest: (req, res) => {
        userModel.findOne({ _id: req.userId, status: "ACTIVE", accountType: { $ne: "RECOVERY" }, userType: "ADMIN" },
            (error, balanceData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!balanceData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    exchangeModel.findOne({ status: "ACTIVE" }, (err, exchangeDetails) => {
                        console.log("=========eeeeee=========>", exchangeDetails, exchangeDetails.unitUSD, exchangeDetails.unitUSD)

                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [exchangeDetails], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            notificationModel.findOne({ _id: req.body.notificationId, status: "requested" }, (err, notificationDetails) => {
                                console.log("=============?", notificationDetails)
                                if (err) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [notificationDetails], ErrorMessage.INTERNAL_ERROR);
                                }
                                else if (!notificationDetails) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.REQUEST_NOT_FOUND);
                                }
                                else {
                                    userModel.findOne({ _id: notificationDetails.customer_Id, status: "ACTIVE" }, (errDetails, sucDetails) => {
                                        console.log("===============>", sucDetails)
                                        if (errDetails) {
                                            response(res, ErrorCode.SOMETHING_WRONG, [sucDetails], ErrorMessage.INTERNAL_ERROR);
                                        }
                                        else {
                                            var convert_USD = parseFloat(notificationDetails.amount) * parseFloat(exchangeDetails.unitUSD)

                                            var convert_CDF = parseFloat(notificationDetails.amount) * parseFloat(exchangeDetails.unitCDF)

                                            if (notificationDetails.amountType == "USD") {

                                                if (balanceData.amountCDF < convert_CDF) {
                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                }
                                                else {
                                                    console.log("===================>", balanceData.amountCDF, notificationDetails.amount, exchangeDetails.unitCDF)
                                                    userModel.findOneAndUpdate({ _id: balanceData._id }, { $set: { amountCDF: parseFloat(balanceData.amountCDF) - parseFloat(notificationDetails.amount) * parseFloat(exchangeDetails.unitCDF) } },
                                                        { new: true }, (errBalance, updateBalance) => {
                                                            if (errBalance) {
                                                                response(res, ErrorCode.SOMETHING_WRONG, [errBalance], ErrorMessage.INTERNAL_ERROR);
                                                            }
                                                            else {
                                                                userModel.findOneAndUpdate({ _id: notificationDetails.customer_Id }, { $set: { amountCDF: parseFloat(sucDetails.amountCDF) + parseFloat(notificationDetails.amount) * parseFloat(exchangeDetails.unitCDF) } },
                                                                    { new: true }, (errBal, updateBal) => {
                                                                        if (errBal) {
                                                                            response(res, ErrorCode.SOMETHING_WRONG, [updateBal], ErrorMessage.INTERNAL_ERROR);
                                                                        }
                                                                        else {
                                                                            notificationModel.findOneAndUpdate({ _id: notificationDetails._id }, { $set: { status: "approved" } }, { new: true }, (errStatus, updateStatus) => {
                                                                                if (errStatus) {
                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [errStatus], ErrorMessage.INTERNAL_ERROR);
                                                                                }
                                                                                else {
                                                                                    response(res, SuccessCode.SUCCESS, updateBal, SuccessMessage.UPDATE_SUCCESS);
                                                                                }
                                                                            })
                                                                            // response(res, SuccessCode.SUCCESS, updateBal, SuccessMessage.UPDATE_SUCCESS);
                                                                        }
                                                                    })
                                                            }
                                                        })
                                                }
                                            }
                                            else if (notificationDetails.amountType == "SDG") {
                                                if (balanceData.amountUSD < convert_USD) {
                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.NOT_SUFFICIENT_BALANCE);
                                                }
                                                else {
                                                    userModel.findOneAndUpdate({ _id: balanceData._id }, { $set: { amountUSD: parseFloat(balanceData.amountUSD) - parseFloat(notificationDetails.amount) * parseFloat(exchangeDetails.unitUSD) } },
                                                        { new: true }, (errBalance, updateBalance) => {
                                                            if (errBalance) {
                                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                            }
                                                            else {
                                                                userModel.findOneAndUpdate({ _id: notificationDetails.customer_Id }, { $set: { amountUSD: parseFloat(sucDetails.amountUSD) + parseFloat(notificationDetails.amount) * parseFloat(exchangeDetails.unitUSD) } },
                                                                    { new: true }, (errBal, updateBal) => {
                                                                        if (errBal) {
                                                                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                                        }
                                                                        else {
                                                                            notificationModel.findOneAndUpdate({ _id: notificationDetails._id }, { $set: { status: "approved" } }, { new: true }, (errStatus, updateStatus) => {
                                                                                if (errStatus) {
                                                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                                                }
                                                                                else {
                                                                                    response(res, SuccessCode.SUCCESS, updateStatus, SuccessMessage.UPDATE_SUCCESS);
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                            }
                                                        })
                                                }
                                            }
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })

    },
    rejectExchangRequestByAdmin: (req, res) => {
        userModel.findOne({ _id: req.userId, status: "ACTIVE", accountType: { $ne: "RECOVERY" }, userType: "ADMIN" },
            (error, balanceData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!balanceData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    notificationModel.findOne({ _id: req.body.notificationId, status: "requested" }, (err, notificationDetails) => {
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else if (!notificationDetails) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.REQUEST_NOT_FOUND);
                        }
                        else {
                            userModel.findOne({ _id: notificationDetails.customer_Id, status: "ACTIVE" }, (errDetails, sucDetails) => {
                                console.log("==================>", errDetails, sucDetails)
                                if (errDetails) {
                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                    if (notificationDetails.amountType == "USD") {
                                        userModel.findOneAndUpdate({ _id: balanceData._id }, { $set: { amountUSD: parseFloat(balanceData.amountUSD) - parseFloat(notificationDetails.amount) } }, { new: true },
                                            (errAdmin, updateAdmin) => {
                                                if (errAdmin) {
                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                }
                                                else {
                                                    userModel.findOneAndUpdate({ _id: notificationDetails.customer_Id }, { $set: { amountUSD: parseFloat(sucDetails.amountUSD) + parseFloat(notificationDetails.amount) } }, { new: true },
                                                        (errAd, updateAd) => {
                                                            if (errAd) {
                                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                            }
                                                            else {
                                                                notificationModel.findOneAndUpdate({ _id: notificationDetails._id }, { $set: { status: "rejected" } }, { new: true }, (errStatus, updateStatus) => {
                                                                    if (errStatus) {
                                                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                                    }
                                                                    else {
                                                                        response(res, SuccessCode.SUCCESS, updateStatus, SuccessMessage.UPDATE_SUCCESS);
                                                                    }
                                                                })
                                                            }
                                                        })
                                                }
                                            })
                                    }
                                    else if (notificationDetails.amountType == "SDG") {
                                        userModel.findOneAndUpdate({ _id: balanceData._id }, { $set: { amountCDF: parseFloat(balanceData.amountCDF) - parseFloat(notificationDetails.amount) } }, { new: true },
                                            (errAdmin, updateAdmin) => {
                                                if (errAdmin) {
                                                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                }
                                                else {
                                                    userModel.findOneAndUpdate({ _id: notificationDetails.customer_Id }, { $set: { amountCDF: parseFloat(sucDetails.amountCDF) + parseFloat(notificationDetails.amount) } }, { new: true },
                                                        (errAd, updateAd) => {
                                                            if (errAd) {
                                                                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                            }
                                                            else {
                                                                notificationModel.findOneAndUpdate({ _id: notificationDetails._id }, { $set: { status: "rejected" } }, { new: true }, (errStatus, updateStatus) => {
                                                                    if (errStatus) {
                                                                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                                                                    }
                                                                    else {
                                                                        response(res, SuccessCode.SUCCESS, updateStatus, SuccessMessage.UPDATE_SUCCESS);
                                                                    }
                                                                })
                                                            }
                                                        })
                                                }
                                            })
                                    }
                                }
                            })
                        }
                    })
                }
            })
    },
    getAllExchangeRequest: (req, res) => {
        notificationModel.find({ notificationType: "Exchange" }, (error, listNotificatiion) => {
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else {
                response(res, SuccessCode.SUCCESS, listNotificatiion, SuccessMessage.DATA_FOUND);

            }
        })
    },
    getSupportMessage: (req, res) => {
        try {
            messageModel.find({}, (error, userData) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                } else if (userData.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, userData, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);

        }
    },


    getSomeGraphData: (req, res) => {
        
        transactionModel.aggregate([
            { "$match": { sender_UserType: "ADMIN" } },
            {
                "$project":
                {
                    "month": { "$month": "$createdAt" },
                }
            },
            {
                "$group": {
                    "_id": null,
                    "January": { "$sum": { "$cond": [{ "$eq": ["$month", 1] }, 1, 0] } },
                    "February": { "$sum": { "$cond": [{ "$eq": ["$month", 2] }, 1, 0] } },
                    "March": { "$sum": { "$cond": [{ "$eq": ["$month", 3] }, 1, 0] } },
                    "April": { "$sum": { "$cond": [{ "$eq": ["$month", 4] }, 1, 0] } },
                    "May": { "$sum": { "$cond": [{ "$eq": ["$month", 5] }, 1, 0] } },
                    "June": { "$sum": { "$cond": [{ "$eq": ["$month", 6] }, 1, 0] } },
                    "July": { "$sum": { "$cond": [{ "$eq": ["$month", 7] }, 1, 0] } },
                    "August": { "$sum": { "$cond": [{ "$eq": ["$month", 8] }, 1, 0] } },
                    "September": { "$sum": { "$cond": [{ "$eq": ["$month", 9] }, 1, 0] } },
                    "October": { "$sum": { "$cond": [{ "$eq": ["$month", 10] }, 1, 0] } },
                    "November": { "$sum": { "$cond": [{ "$eq": ["$month", 11] }, 1, 0] } },
                    "December": { "$sum": { "$cond": [{ "$eq": ["$month", 12] }, 1, 0] } }
                }
            }
        ],async (err1, newResult) => {
            console.log("effrfff",err1,newResult)
        if (err1) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
        } else if (newResult.length == 0) {
            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
        } else {
            response(res, SuccessCode.SUCCESS, newResult, SuccessMessage.DATA_FOUND);
        }})
    },



     agentGraph : (req, res) => {
        let today = new Date();
        today.setHours(0, 0, 0, 0)
        var offset = (new Date().getTimezoneOffset()) * 60000;
        today = new Date(today - offset).getTime()
        var day = new Date(today)
        var month = day.getTime()
        month = month - 2592000000;
        let monthDate = new Date(month)
        var year = day.getTime()
        year = year - 2592000000 * 12;
        let yearDate = new Date(year)
    
        let day1 = day.getDay()
        let today1 = new Date()
        let endDay = new Date()
    
        //var week = new Date( Date.now()-7*86400000) 
        // console.log   (">>>>>>>>>>>>",week) 
        endDay.setHours(23, 59, 59, 999)
        let Tommorow1 = day.setDate(day.getDate() + 1)
        let Tommorow = new Date(Tommorow1)
    
        let lastTommorow = Tommorow.setHours(23, 59, 59, 999)
        const futureDay = 7 - day1;
        const futureTime = futureDay * 86400000;
        const time = day1 * 86400000;
        let startTime = today - time;
        let startDate = new Date(startTime)
        let endTime = today + futureTime;
        let endDate = new Date(endTime)
        var query = { status: { $ne: 'DELETE' },sender_UserType:"AGENT"  }
        if (req.body.dayCount == "Daily") {
    
            // query.$and = [{ createdAt: { $lte: endDay } }, { createdAt: { $gte: today } }]
            query.$and = [{ createdAt: { $lte: endDate } }, { createdAt: { $gte: startDate } }]
        }
    
        if (req.body.dayCount == "Weekly") {
            // query.$and = [{ createdAt: { $lte: endDate } }, { createdAt: { $gte: startDate } }]
            query.$and = [{ createdAt: { $lte: day } }, { createdAt: { $gte: monthDate } }]
        }
    
        if (req.body.dayCount == "Monthly") {
            //  query.$and = [{ createdAt: { $lte: day } }, { createdAt: { $gte: monthDate } }]
            query.$and = [{ createdAt: { $lte: day } }, { createdAt: { $gte: yearDate } }]
        }
        if (req.body.dayCount == "  ") {
            query.$and = [{ createdAt: { $lte: day } }, { createdAt: { $gte: yearDate } }]
        }
    
        transactionModel.find(query).exec((error1, data) => {
    
            var temp = []
    
            // console.log("@@@@@",error1,a)
            if (error1) {
                res.send({
                    responseCode: 500,
                    responseMessage: "Internal server error", error1
                })
            } else if (!data) {
                res.send({
                    responseCode: 404,
                    responseMessage: "No data found"
                })
            } else {
                var weekday = new Array(7);
                weekday[0] = "Sunday";
                weekday[1] = "Monday";
                weekday[2] = "Tuesday";
                weekday[3] = "Wednesday";
                weekday[4] = "Thursday";
                weekday[5] = "Friday";
                weekday[6] = "Saturday";
                var count = 0;
                var sunCount = 0
                var monCount = 0
                var tueCount = 0
                var wedCount = 0
                var thusCount = 0
                var friCount = 0
                var satCount = 0
    
    
                var month = new Array();
                month[0] = "January";
                month[1] = "February";
                month[2] = "March";
                month[3] = "April";
                month[4] = "May";
                month[5] = "June";
                month[6] = "July";
                month[7] = "August";
                month[8] = "September";
                month[9] = "October";
                month[10] = "November";
                month[11] = "December";
    
    
                var janCount = 0
                var febCount = 0
                var marCount = 0
                var aprCount = 0
                var mayCount = 0
                var junCount = 0
                var julyCount = 0
                var augCount = 0
                var sepCount = 0
                var octCount = 0
                var novCount = 0
                var decCount = 0
    
    
                data.forEach(function (entry) {
                    count = count + 1;
                    if (req.body.dayCount == "Weekly" || req.body.dayCount == "Daily") {
    
                        var dayFind = weekday[entry.createdAt.getDay()];
                        if (dayFind == "Sunday") {
                            sunCount = sunCount + 1;
                        } else if (dayFind == "Monday") {
                            monCount = monCount + 1;
                        } else if (dayFind == "Tueday") {
                            tueCount = tueCount + 1
                        } else if (dayFind == "Wednesday") {
                            wedCount = wedCount + 1
                        } else if (dayFind == "Thursday") {
                            thusCount = thusCount + 1
                        } else if (dayFind == "Friday") {
                            friCount = friCount + 1
                        } else if (dayFind == "Saturday") {
                            satCount = satCount + 1
                        }
                    }
                    else if (req.body.dayCount == "Monthly") {
                        var dayFind = month[entry.createdAt.getMonth()];
                        if (dayFind == "January") {
                            janCount = janCount + 1;
                        } else if (dayFind == "February") {
                            febCount = febCount + 1;
                        } else if (dayFind == "March") {
                            marCount = marCount + 1
                        } else if (dayFind == "April") {
                            aprCount = aprCount + 1
                        } else if (dayFind == "May") {
                            mayCount = mayCount + 1
                        } else if (dayFind == "June") {
                            junCount = junCount + 1
                        } else if (dayFind == "July") {
                            julyCount = julyCount + 1
                        } else if (dayFind == "August") {
                            augCount = augCount + 1
                        } else if (dayFind == "September") {
                            sepCount = sepCount + 1
                        } else if (dayFind == "October") {
                            octCount = octCount + 1
                        } else if (dayFind == "November") {
                            novCount = novCount + 1
                        } else if (dayFind == "December") {
                            decCount = decCount + 1
                        }
                    }
    
                })
                console.log('countcountcount', count)
                console.log('lengthhhh', data.length)
                console.log('sunCountsunCountsunCount', data.length)
                if (count == data.length) {
                    if (req.body.dayCount == "Weekly" || req.body.dayCount == "Daily") {
                        var finalData = [{ day: 'Sunday', count: sunCount }, { day: 'Monday', count: monCount }, { day: 'Tueday', count: tueCount }, { day: 'Wednesday', count: wedCount }, { day: 'Thursday', count: thusCount }, { day: 'Friday', count: friCount }, { day: 'Saturday', count: satCount }]
                    } else if (req.body.dayCount == "Monthly") {
                        var finalData = [{ day: 'January', count: janCount }, { day: 'February', count: marCount }, { day: 'March', count: wedCount }, { day: 'April', count: aprCount }, { day: 'May', count: mayCount }, { day: 'June', count: junCount }, { day: 'July', count: julyCount }, { day: 'August', count: augCount }, { day: 'September', count: sepCount }, { day: 'October', count: octCount }, { day: 'November', count: novCount }, { day: 'December', count: decCount }]
                    } else if (req.body.dayCount == "Yearly") {
    
                        var finalData = [{ day: 2020, count: data.length }]
                    }
                    res.send({
                        responseCode: 200,
                        responseMessage: "User graph list",
                        totalTransaction: finalData
                    })
                }
    
            }
        })
    },
    

    clientGraph : (req, res) => {
        let today = new Date();
        today.setHours(0, 0, 0, 0)
        var offset = (new Date().getTimezoneOffset()) * 60000;
        today = new Date(today - offset).getTime()
        var day = new Date(today)
        var month = day.getTime()
        month = month - 2592000000;
        let monthDate = new Date(month)
        var year = day.getTime()
        year = year - 2592000000 * 12;
        let yearDate = new Date(year)
    
        let day1 = day.getDay()
        let today1 = new Date()
        let endDay = new Date()
    
        //var week = new Date( Date.now()-7*86400000) 
        // console.log   (">>>>>>>>>>>>",week) 
        endDay.setHours(23, 59, 59, 999)
        let Tommorow1 = day.setDate(day.getDate() + 1)
        let Tommorow = new Date(Tommorow1)
    
        let lastTommorow = Tommorow.setHours(23, 59, 59, 999)
        const futureDay = 7 - day1;
        const futureTime = futureDay * 86400000;
        const time = day1 * 86400000;
        let startTime = today - time;
        let startDate = new Date(startTime)
        let endTime = today + futureTime;
        let endDate = new Date(endTime)
        var query = { status: { $ne: 'DELETE' },sender_UserType:"CUSTOMER" }
        if (req.body.dayCount == "Daily") {
    
            // query.$and = [{ createdAt: { $lte: endDay } }, { createdAt: { $gte: today } }]
            query.$and = [{ createdAt: { $lte: endDate } }, { createdAt: { $gte: startDate } }]
        }
    
        if (req.body.dayCount == "Weekly") {
            // query.$and = [{ createdAt: { $lte: endDate } }, { createdAt: { $gte: startDate } }]
            query.$and = [{ createdAt: { $lte: day } }, { createdAt: { $gte: monthDate } }]
        }
    
        if (req.body.dayCount == "Monthly") {
            //  query.$and = [{ createdAt: { $lte: day } }, { createdAt: { $gte: monthDate } }]
            query.$and = [{ createdAt: { $lte: day } }, { createdAt: { $gte: yearDate } }]
        }
        if (req.body.dayCount == "  ") {
            query.$and = [{ createdAt: { $lte: day } }, { createdAt: { $gte: yearDate } }]
        }
    
        transactionModel.find(query).exec((error1, data) => {
    
            var temp = []
    
            // console.log("@@@@@",error1,a)
            if (error1) {
                res.send({
                    responseCode: 500,
                    responseMessage: "Internal server error", error1
                })
            } else if (!data) {
                res.send({
                    responseCode: 404,
                    responseMessage: "No data found"
                })
            } else {
                var weekday = new Array(7);
                weekday[0] = "Sunday";
                weekday[1] = "Monday";
                weekday[2] = "Tuesday";
                weekday[3] = "Wednesday";
                weekday[4] = "Thursday";
                weekday[5] = "Friday";
                weekday[6] = "Saturday";
                var count = 0;
                var sunCount = 0
                var monCount = 0
                var tueCount = 0
                var wedCount = 0
                var thusCount = 0
                var friCount = 0
                var satCount = 0
    
    
                var month = new Array();
                month[0] = "January";
                month[1] = "February";
                month[2] = "March";
                month[3] = "April";
                month[4] = "May";
                month[5] = "June";
                month[6] = "July";
                month[7] = "August";
                month[8] = "September";
                month[9] = "October";
                month[10] = "November";
                month[11] = "December";
    
    
                var janCount = 0
                var febCount = 0
                var marCount = 0
                var aprCount = 0
                var mayCount = 0
                var junCount = 0
                var julyCount = 0
                var augCount = 0
                var sepCount = 0
                var octCount = 0
                var novCount = 0
                var decCount = 0
    
    
                data.forEach(function (entry) {
                    count = count + 1;
                    if (req.body.dayCount == "Weekly" || req.body.dayCount == "Daily") {
    
                        var dayFind = weekday[entry.createdAt.getDay()];
                        if (dayFind == "Sunday") {
                            sunCount = sunCount + 1;
                        } else if (dayFind == "Monday") {
                            monCount = monCount + 1;
                        } else if (dayFind == "Tueday") {
                            tueCount = tueCount + 1
                        } else if (dayFind == "Wednesday") {
                            wedCount = wedCount + 1
                        } else if (dayFind == "Thursday") {
                            thusCount = thusCount + 1
                        } else if (dayFind == "Friday") {
                            friCount = friCount + 1
                        } else if (dayFind == "Saturday") {
                            satCount = satCount + 1
                        }
                    }
                    else if (req.body.dayCount == "Monthly") {
                        var dayFind = month[entry.createdAt.getMonth()];
                        if (dayFind == "January") {
                            janCount = janCount + 1;
                        } else if (dayFind == "February") {
                            febCount = febCount + 1;
                        } else if (dayFind == "March") {
                            marCount = marCount + 1
                        } else if (dayFind == "April") {
                            aprCount = aprCount + 1
                        } else if (dayFind == "May") {
                            mayCount = mayCount + 1
                        } else if (dayFind == "June") {
                            junCount = junCount + 1
                        } else if (dayFind == "July") {
                            julyCount = julyCount + 1
                        } else if (dayFind == "August") {
                            augCount = augCount + 1
                        } else if (dayFind == "September") {
                            sepCount = sepCount + 1
                        } else if (dayFind == "October") {
                            octCount = octCount + 1
                        } else if (dayFind == "November") {
                            novCount = novCount + 1
                        } else if (dayFind == "December") {
                            decCount = decCount + 1
                        }
                    }
    
                })
                console.log('countcountcount', count)
                console.log('lengthhhh', data.length)
                console.log('sunCountsunCountsunCount', data.length)
                if (count == data.length) {
                    if (req.body.dayCount == "Weekly" || req.body.dayCount == "Daily") {
                        var finalData = [{ day: 'Sunday', count: sunCount }, { day: 'Monday', count: monCount }, { day: 'Tueday', count: tueCount }, { day: 'Wednesday', count: wedCount }, { day: 'Thursday', count: thusCount }, { day: 'Friday', count: friCount }, { day: 'Saturday', count: satCount }]
                    } else if (req.body.dayCount == "Monthly") {
                        var finalData = [{ day: 'January', count: janCount }, { day: 'February', count: marCount }, { day: 'March', count: wedCount }, { day: 'April', count: aprCount }, { day: 'May', count: mayCount }, { day: 'June', count: junCount }, { day: 'July', count: julyCount }, { day: 'August', count: augCount }, { day: 'September', count: sepCount }, { day: 'October', count: octCount }, { day: 'November', count: novCount }, { day: 'December', count: decCount }]
                    } else if (req.body.dayCount == "Yearly") {
    
                        var finalData = [{ day: 2020, count: data.length }]
                    }
                    res.send({
                        responseCode: 200,
                        responseMessage: "User graph list",
                        totalTransaction: finalData
                    })
                }
    
            }
        })
    },
    
    editSuppportMessageContent: (req, res) => {
        try {
            console.log("===========>",req)
            messageModel.findOne({ _id: req.body.messageId }, (err, result) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!result) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                }
                else {
                    messageModel.findOneAndUpdate({ _id: req.body.messageId }, { $set: req.body }, { new: true, runValidators: true }, (updateErr, updateResult) => {
                        if (updateErr) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, updateResult, SuccessMessage.UPDATE_SUCCESS);
                        }
                    })

                }           
            })

        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },

    addBillCategory: (req, res) => {

        if(!req.body.categoryImage && !req._id) {
            response(res, ErrorCode.SOMETHING_WRONG, [], "Category Image is required");
        }

        // if(!req.body.subCategoryImage && !req._id) {
        //     response(res, ErrorCode.SOMETHING_WRONG, [], "SubCategory Image is required");
        // }
        let image;
        var iscategoryImage = false;
        
        if(req.body.categoryImage.includes("base64")) {

            image = req.body.categoryImage;
            iscategoryImage = true;
        } else if(req.body.subCategoryImage.includes("base64"))
            image = req.body.subCategoryImage;

        if(image) 
        {
            commonFunction.uploadImage(image, (error, result) => {
                if (error) {
                     console.log("Error while Uploading Image", error);
                     response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                } else {
                     console.log("Image uploaded :)");

                     if(iscategoryImage)
                     req.body.categoryImage = result;
                     else req.body.subCategoryImage = result;

                     new billCategoryModel(req.body).save((err, success) => {
                        console.log("billCategory like ",err,success)
                        if (err) {
                            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, success, SuccessMessage.DATA_SAVED)
                        }
                    });
                }
            });
        } 
        else {
            new billCategoryModel(req.body).save((err, success) => {
                console.log("billCategory like ",err,success)
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR)
                }
                else {
                    response(res, SuccessCode.SUCCESS, success, SuccessMessage.DATA_SAVED)
                }
        });
    }
},

    getBillCategories: (req, res) => {
        try {
            billCategoryModel.find({ status: { $ne: "DELETE" } }, (error, data) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);

        }
    },

    getBillCategory: (req, res) => {
        try {
            billCategoryModel.find({ _id : req.body.id, status: { $ne: "DELETE" } }, (error, data) => {
                if (error) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (!data.length) {
                    response(res, ErrorCode.NOT_FOUND, [], "Category Not Found");
                }
                else {
                    response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);

        }
    },

    updateBillCategory: (req,res) => {
        billCategoryModel.findOne({ _id: req.body._id } , async (error, result) => {
            console.log("updateBillCategory", error, result)
            if (error) {
                response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
            }
            else if (!result) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            }
            else {

                obj = {}
                if (req.body.status) {
                    obj.status = req.body.status
                }
                if (req.body.categoryName) {
                    obj.categoryName = req.body.categoryName
                }
                if (req.body.subCategoryName) {
                    obj.subCategoryName = req.body.subCategoryName
                }
                if (req.body.billNumLength) {
                    obj.billNumLength = req.body.billNumLength
                }
                if (req.body.categoryImage) {
                    obj.categoryImage = await convertImage(req.body.categoryImage)
                }
                if (req.body.subCategoryImage) {
                    obj.subCategoryImage = await convertImage(req.body.subCategoryImage)
                }
                if (req.body.categoryId) {
                    obj.categoryId = req.body.categoryId
                }
                
                billCategoryModel.findOneAndUpdate({ _id: result._id }, { $set: obj }, { new: true }, (error, userData) => {
                    if (error) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    } else if (!userData) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.UPDATE_NOT);
                    } else {
                        response(res, SuccessCode.SUCCESS, userData, SuccessMessage.UPDATE_SUCCESS);
                    }
                })
            }
        })
  
}
}



function convertImage(profilePic) {
    return new Promise((resolve, reject) => {
        commonFunction.uploadImage(profilePic, (error, imageData) => {
            if (error) {
                resolve(error)
            }
            else {
                resolve(imageData)
            }
        })
    })
}
