const mongoose = require('mongoose');
const schema = mongoose.Schema;

let educationPaymentModel = new schema({
    courseName: {
        type:String
    },
    cousrseId: {
        type:String
    },
    selectedForm: {
        type:String
    },
    selectedFormId: {
        type:String
    },
    seatNumber: {
        type:String
    },
    studentName: {
        type:String
    },
    studentPhone: {
        type:String
    },
    studentEmail: {
        type:String
    },
    amount: {
        type:Number
    },
    rechargeType:{
        type:String,
        enum: ["MOBILE_WALLET", "CARD"],
        default:"MOBILE_WALLET"
    },
    billType:{
        type:String,
        enum: ["SUDANESE", "ARAB"],
        default: "SUDANESE"
    },
    transactionRefId: {
        type:String
    }
}, {timestamps : true});

var educationPayment = mongoose.model('educationPayment', educationPaymentModel);
module.exports= educationPayment;
