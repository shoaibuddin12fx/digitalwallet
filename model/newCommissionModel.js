const mongoose = require('mongoose');
const schema = mongoose.Schema;

const bcrypt = require("bcrypt-nodejs");

var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
var mongoosePaginate = require('mongoose-paginate');

let newCommissionModel = new schema({
    admin_To_Agent: {
        type: Number
    },
    agent_To_Admin: {
        type: Number
    },
    agent_To_Client: {
        type: Number
    },
    client_To_Agent: {
        type: Number
    },
    client_To_Client: {
        type: Number
    },
    transactionFee: {
        type: Number
    },
    monthlyFee: {
        type: Number
    },
    annuallyFee: {
        type: Number
    },
    depositFee: {
        type: Number
    },
    withdrawalFee: {
        type: Number
    },
    commisionFee: {
        type: Number
    },
    amount: {
        type: Number
    },
    amountType: {
        type: String
    },
    deposit_admin_commission: {
        type: Number
    },
    send_amount: {
        type: Number,
    },
    receive_amount: {
        type: Number,
    },
    deposit_agent_Commission: {
        type: Number,
    },
    sender_UserType: {
        type: String,
    },
    withdraw_admin_commission: {
        type: Number
    },
    withdraw_agent_commission: {
        type: Number
    },
    receiver_UserType: {
        type: String,
    },
    admin_commission: {
        type: String
    },
    agent_Commission:{
        type:String
    },  
    commission: {
        type: Number
    },
        status: {
        type: String,
    },
}, { timestamps: true })
newCommissionModel.plugin(mongoosePaginate);
newCommissionModel.plugin(mongooseAggregatePaginate);

var newCommission = mongoose.model('newCommission', newCommissionModel);
module.exports = newCommission

mongoose.model('newCommission', newCommissionModel).find((error, result) => {
    if (result.length == 0) {
        let obj = {
            'status': "ACTIVE",
            'admin_To_Agent': 1,
            'agent_To_Admin': 1,
            'agent_To_Client': 2,
            'client_To_Agent': 2,
            'client_To_Client':2,
            "billPayment_commision": 0
        };
        mongoose.model('newCommission', newCommissionModel).create(obj, (error1, success) => {
            if (error1)
                console.log("Error is" + error1)
            else
                console.log("saved succesfully.", success);
        })
    }
});


