const mongoose = require('mongoose');
const schema = mongoose.Schema;
const bcrypt = require("bcrypt-nodejs");

var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
var mongoosePaginate = require('mongoose-paginate');

let userBillModel = new schema({

    fullName: {
        type:String
    },
    accountNumber: {
        type: String
    },
    address: {
        type: String,
    },
    providerName: {
        type: String
    },
    providerId: {
        type: String
    },
    categoryId: {
        type:String
    },
    categoryName: {
        type:String
    },
    gender: {
        type: String,
        enum: ["MALE", "FEMALE", "OTHERS"],
        default:"MALE"
    },
    status: {
        type: String,
        enum: ["ACTIVE", "BLOCK", "DELETE"],
        default: "ACTIVE"
    },
}, { timestamps: true })
userBillModel.plugin(mongoosePaginate);
userBillModel.plugin(mongooseAggregatePaginate);
var userBill = mongoose.model('billAccount', userBillModel);
module.exports = userBill;