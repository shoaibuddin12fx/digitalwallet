const mongoose = require('mongoose');
const schema = mongoose.Schema;
const bcrypt = require("bcrypt-nodejs");

var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
var mongoosePaginate = require('mongoose-paginate');


let qrModel = new schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    mobileNumber: {
        type: Number
    },
    emailId: {
        type: String
    },
    city: {
        type: String
    },
    state: {
        type: String
    },
    userId: {
        type: schema.Types.ObjectId,
        ref: "user"
    },
    qrCode: {
        type: String
    },
    status: {
        type: String,
        enum: ["ACTIVE", "BLOCK", "DELETE"],
        default: "ACTIVE"
    }
}, { timestamps: true })


qrModel.plugin(mongoosePaginate);
qrModel.plugin(mongooseAggregatePaginate);

var qr = mongoose.model('qr', qrModel);
module.exports = qr   