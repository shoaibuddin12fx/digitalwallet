const mongoose = require('mongoose');
const schema = mongoose.Schema;

let billCategoryModel = new schema({
    categoryName: {
        type: String
    },
    categoryImage:{
        type:String
    },
    status: {
        type: String,
        enum: ["ACTIVE", "DEACTIVE", "DELETE"],
        default: "ACTIVE"
    },
    subCategoryId:{
        type: String
    },
    subCategoryName:{
        type: String
    },
    subCategoryImage:{
        type: String
    },
    billNumLength:{
        type: String
    },
    categoryId: {
        type:String
    }
},{timestamps:true})
var billCategory = mongoose.model('billCategories', billCategoryModel);
module.exports = billCategory;  