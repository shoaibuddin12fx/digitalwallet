const mongoose = require('mongoose');
const schema = mongoose.Schema;

let bankCardModel = new schema({

    NameOnCard: {
        type: String
    },
    cardNumber: {
        type: String
    },
    expiryDate:{
        type:Date
    },
    status: {
        type: String,
        enum: ["ACTIVE", "INACTIVE", "BLOCK"],
        default: "ACTIVE"
    },
    userId: {
        type:String
    },
    cardType:{
        type:String
    },
},{timestamps:true})

var bankCard = mongoose.model('BankCard', bankCardModel);
module.exports = bankCard;  