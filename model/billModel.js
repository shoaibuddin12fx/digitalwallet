const mongoose = require('mongoose');
const schema = mongoose.Schema;

let billModel = new schema({
    providerName: {
        type: String
    },
    providerId: {
        type: String
    },
    billAcNumber:{
        type:String
    },
    status: {
        type: String,
        enum: ["PAID", "UNPAID"],
        default: "UNPAID"
    },
    companyImage:{
        type: String
    },
    amountDueinUSD: {
        type:Number,
        default :"0"
    },
    amountDueinSDG: {
        type:Number,
        default :"0"
    },
    amountAfterDueUSD: {
        type:Number,
        default :"0"
    },
    amountAfterDueSDG: {
        type:Number,
        default :"0"
    },
    dueDate: {
        type:Date
    },
    customerName: {
        type:String
    },
    billMonth: {
        type:String
    },
    categoryId: {
        type:String
    },
    categoryName: {
        type:String
    },
    billCreatedOn:{
        type:Date
    },
    billStartDate:{
        type:Date
    },
    billEndDate:{
        type:Date
    },
    paidOn:{
        type:Date
    },
    paidBy: {
        type:String
    }
},{timestamps:true})
var bill = mongoose.model('Bill', billModel);
module.exports = bill;  