const mongoose = require('mongoose');
const schema = mongoose.Schema;

let merchantModel = new schema({
    merchantName: {
        type: String
    },
    contactNumber:{
        type:String
    },
    status: {
        type: String,
        enum: ["ACTIVE", "DEACTIVE", "DELETE"],
        default: "ACTIVE"
    },
    companyName:{
        type: String
    },
    companyImage:{
        type: String
    },
    homeAddress:{
        type: String
    },
    officeAddress:{
        type: String
    },
    officeContactNumber: {
        type:String
    },
    accountNumber: {
        type:String
    },
    categoryId: {
        type:String
    },
    currencyDealing: {
        type:String
    },
    nationality: {
        type:String
    },
    emailId: {
        type:String
    },
    categoryName: {
        type:String
    },
    countryCode:{
        type:String
    },
    balanceUSD: {
        type:Number,
        default:'0'
    },
    balanceSDG: {
        type:Number,
        default:'0'
    }

},{timestamps:true})
var merchant = mongoose.model('Merchants', merchantModel);
module.exports = merchant;