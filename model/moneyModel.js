const mongoose = require('mongoose');
const schema = mongoose.Schema;
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
var mongoosePaginate = require('mongoose-paginate');
let money = new schema({
    
    clientToClient_SEND: {
        type: Number
    },
    clientToAgent_WITHDRAW:{
        type:Number
    },
    clientToAdmin_EXCHANGE:{
        type:Number
    },
    AgentToClient_DEPOSITE:{
        type:Number
    },
    AgentToAdmin_EXCHANGE:{
        type:Number
    },
    AdminToAgent_SEND:{
        type:Number
    },
    AdminToClient_EXCHANGE:{
        type:Number
    },
    status:{
        type:String
    }
}, { timestamps: true })

money.plugin(mongoosePaginate);  
money.plugin(mongooseAggregatePaginate);

module.exports = mongoose.model('money', money)

mongoose.model('money', money).find((error, result) => {
    if (result.length == 0) {
        let obj = {
            'status':"ACTIVE",
            'clientToClient_SEND': 10,
            'clientToAgent_WITHDRAW': 10,
            'clientToAdmin_EXCHANGE': 10,
            'AgentToClient_DEPOSITE': 10,
            'AgentToAdmin_EXCHANGE': 10,
            'AdminToAgent_SEND': 10,
            'AdminToClient_EXCHANGE':10
   
        };
        mongoose.model('money', money).create(obj, (error, success) => {  
            if (error)
                console.log("Error is" + error)
            else
                console.log("saved succesfully.", success);
        })
    }
});